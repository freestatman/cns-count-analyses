#!/usr/bin/env Rscript

library(limma)

source("../lib/r/utils.r")

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 2){
    stop("usage: ./contrast-de.r RDATA OUTFILE")
} else{
    rdata.file <- args[1]
    outfile <- args[2]
}

load(rdata.file)

cm <- makeContrasts(contrasts=pair.diffs(colnames(design)),
                    levels=design)

c.fit <- treat(contrasts.fit(fit, contrasts=cm), lfc=1)
de <- decideTests(c.fit, method="global", adjust.method="BH",
                  p.value=0.01)

write.csv(de, file=outfile, row.names=FALSE, quote=FALSE)
