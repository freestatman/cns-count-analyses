#!/usr/bin/env Rscript

## Estimate whether gene is DE between any NCX area.
##
## estimate-perde.r does the same, but for brain regions (collapsing
## NCX areas).

library(limma)
library(rhdf5)

datafile <- "../data/expression/johnson2009functional.hdf5"
## genes x samples
expr <- t(as.matrix(h5read(datafile, "/values")))

## 32-bit integer warnings
suppressWarnings(di <- h5read(datafile, "/di") + 1)
suppressWarnings(db <- h5read(datafile, "/db") + 1)
suppressWarnings(dr <- h5read(datafile, "/dr") + 1)

ncx.idx  <- dr == 1
expr <- expr[,ncx.idx]
db <- factor(db[ncx.idx])
di <- factor(di[ncx.idx])

x <- data.frame(region=db, indiv=di)
design <- model.matrix(~ 0 + region, data=x)

dupcor <- duplicateCorrelation(expr,
                               design=design, ndups=1, block=x$indiv)
fit <- lmFit(expr, design, block=x$indiv, correlation=dupcor$consensus)

save(x, design, dupcor, fit, file="ncx-de.rdata")
