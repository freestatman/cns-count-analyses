#!/usr/bin/env python3

import re

import numpy as np
import pandas as pd

h5file = "../data/expression/kang2011spatio-p1.h5"
defile = "../limma-de/region-de-p1.csv"

de_dabg = pd.read_csv(defile)

regions = pd.Series(["ncx", "hip", "amy", "str", "md", "cbc"])

with pd.get_store(h5file, mode="r") as store:
    pass_dabg = store["pass_dabg"].values

de = np.zeros((len(pass_dabg), de_dabg.shape[1]), dtype=np.int)
de[pass_dabg] = de_dabg.values
de = pd.DataFrame(de, columns=de_dabg.columns)
de.index.name = "gene"

PR_RE = re.compile(r"pr([0-9]+)\.([0-9]+) - pr([0-9]+)\.([0-9])+$")


def period_regions(label):
    m = PR_RE.match(label)
    period = m.group(1)
    if period != m.group(3):
        raise ValueError("Label {} has contrasts between different periods")
    return int(period), int(m.group(2)), int(m.group(4))

de.columns = pd.MultiIndex.from_tuples(de.columns.map(period_regions),
                                       names=("period", "reg1", "reg2"))

de = de.stack(de.columns.names)
de.name = "de"
de = de.reset_index()
de = de[de["period"] > 2]
de["reg1"] = de["reg1"] - 1
de["reg2"] = de["reg2"] - 1
de["de_abs"] = de["de"].abs()

de_butone_any = {}
for regidx in range(6):
    de_butone = de[(de["reg1"] != regidx) & (de["reg2"] != regidx)]
    de_any = (de_butone.groupby(["gene", "period"])["de_abs"].sum() > 0)
    de_butone_any[regions[regidx]] = de_any.astype(np.int).unstack("period")
de_butone_any = pd.concat(de_butone_any, axis=1,
                          names=["dropped_region", "period"])

de_butone_any.to_csv("region-de-butone.dat", sep=" ",
                     index=False, header=False)

names = de_butone_any.columns.to_series().reset_index()
names = names[["dropped_region", "period"]]
names.to_csv("region-de-butone-names.txt", sep=" ", index=False)
