#!/usr/bin/env Rscript

## For each time period, estimate whether gene is DE between any of 11
## NCX areas.
##
## estimate-perde.r does the same, but for 6 brain regions (collapsing
## NCX areas).

library(limma)
library(rhdf5)

datafile <- "../data/expression/kang2011spatio-p1.hdf5"
## genes x samples
expr <- t(as.matrix(h5read(datafile, "/values")))

## 32-bit integer warnings
suppressWarnings(di <- h5read(datafile, "/di") + 1)
suppressWarnings(ds <- h5read(datafile, "/ds") + 1)
suppressWarnings(db <- h5read(datafile, "/db") + 1)
suppressWarnings(dr <- h5read(datafile, "/dr") + 1)

pass.dabg <- as.logical(h5read(datafile, "/pass_dabg"))
rin <- h5read(datafile, "/rin")

ncx.idx <- dr == 1
expr <- expr[,ncx.idx]
rin <- rin[ncx.idx]
ds <- ds[ncx.idx]
di <- di[ncx.idx]
db <- db[ncx.idx]

pr = paste(ds, ".", db, sep="")

x <- data.frame(pr=factor(pr), indiv=factor(di), rin=rin)
design <- model.matrix(~ 0 + pr + rin, data=x)

dupcor <- duplicateCorrelation(expr[pass.dabg,],
                               design=design, ndups=1, block=x$indiv)
fit <- lmFit(expr[pass.dabg,], design, block=x$indiv,
             correlation=dupcor$consensus)

save(x, design, dupcor, fit, file="ncx-pr-de-p1.rdata")
