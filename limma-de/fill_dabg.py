#!/usr/bin/env python3
"""Fill in DABG-filtered genes.

Usage: ./fill_dabg.py <de-file> <h5file> <outfile>
"""

from docopt import docopt
import numpy as np
import pandas as pd

args = docopt(__doc__)

de_dabg = pd.read_csv(args["<de-file>"])

with pd.get_store(args["<h5file>"], mode="r") as store:
    pass_dabg = store["pass_dabg"].values

de = np.zeros((len(pass_dabg), de_dabg.shape[1]), dtype=np.int)
de[pass_dabg] = de_dabg.values
de = pd.DataFrame(de, columns=de_dabg.columns)

de.to_csv(args["<outfile>"], index=False)
