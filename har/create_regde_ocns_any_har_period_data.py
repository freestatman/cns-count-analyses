#!/usr/bin/env python3

import sys

import numpy as np
import pandas as pd

import rdump

try:
    period = int(sys.argv[1])
except (ValueError, IndexError):
    sys.exit("Usage: {} PERIOD".format(sys.argv[0]))
period_idx = period - 1

period_idx = 3

de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")

cols = ["har", "ocns_nohp"]
ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts-har.csv")
ng = ng[cols]
ng["ocns_nohp"] = np.log1p(ng["ocns_nohp"])
ng["har"] = (ng["har"] > 0).astype(np.int)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values])

data = {}
data["y"] = de.values[:, period_idx]
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data,
                 "dp__regde_ocns_any_har_period{}.data.r".format(period))
