import mplutil


def plot_to_pdf(df, pdf_file, figsize=(9,7)):
    from matplotlib.backends.backend_pdf import PdfPages
    from matplotlib import gridspec
    import matplotlib.pyplot as plt

    with PdfPages(pdf_file) as pdf:
        total = df.shape[1]
        for idx, var in enumerate(df.columns):
            print("Plotting {} of {}...".format(idx + 1, total))
            fig = plt.figure(figsize=figsize)
            gs = gridspec.GridSpec(2, 2)

            tax = fig.add_subplot(gs[0, :])
            hax = fig.add_subplot(gs[1, 0])
            aax = fig.add_subplot(gs[1, 1])

            plot_trace(df[var].values, tax, alpha=0.6, color='k')
            plot_hist(df[var].values, hax, ec='k', fc='w')
            plot_acorr(df[var].values, aax, color='k')

            fig.suptitle(var)
            fig.tight_layout()

            pdf.savefig()
            plt.close()


def plot_trace(values, ax=None, **kwds):
    fig, ax = mplutil.get_figax(ax)

    ax.plot(values, **kwds)

    mplutil.remove_spines(ax)
    ax.set_ylabel('Value')
    ax.set_xlabel('Iteration')
    ax.locator_params('x', tight=True)
    ax.locator_params('y', tight=True, nbins=5)

    return fig


def plot_hist(values, ax=None, bins=30, **kwds):
    fig, ax = mplutil.get_figax(ax)

    ax.hist(values, bins=bins, **kwds)

    mplutil.remove_spines(ax, ['top', 'right', 'left'])
    ax.set_yticklabels([])
    ax.set_xlabel('Value')
    ax.locator_params('y', tight=True)
    ax.locator_params('x', tight=True, nbins=5)

    return fig


def plot_acorr(values, ax=None, maxlags=100, **kwds):
    import matplotlib.pyplot as plt

    fig, ax = mplutil.get_figax(ax)

    ## Max lags and detrend settings taken from pymc.plots.autocorrplot.
    maxlags = min(len(values) - 1, maxlags)
    ax.acorr(values, maxlags=maxlags,
             detrend=plt.mlab.detrend_mean, **kwds)

    mplutil.remove_spines(ax)
    ax.set_xlabel('Lag')
    ax.set_ylabel('Correlation')
    ax.locator_params('both', tight=True, nbins=6)

    return fig
