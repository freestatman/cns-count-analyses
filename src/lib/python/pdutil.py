

def filter_repeats(df, name, values):
    """Filter repeated rows from `df` based on `values`.

    Parameters
    ----------
    df : DataFrame
    name : string
        Specifies a column to consider as the identifier.
    values : list of strings
        Columns for numeric values in the data frame `df`.  Each
        column will be tried in order, until a column is found where
        the maximum value belongs to a single row.  If a single row is
        not found for any values, a ValueError is raised.

    Returns
    -------
    DataFrame
    """
    counts = df.groupby(name).size()
    repeats = df[df[name].isin(counts.index[counts > 1])]
    top_repeats = repeats.groupby(name).apply(_top, values)
    to_remove = repeats.index[~repeats.index.isin(top_repeats)]
    return df[~df.index.isin(to_remove)]


def _top(df, values):
    for col in values:
        is_max = df[col] == df[col].max()
        if is_max.sum() == 1:
            return df[is_max].index[0]
    raise ValueError("Top row could not be found for given values")
