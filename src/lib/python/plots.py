import numpy as np

import mplutil


def pointrange(x, y, ymin, ymax, ax=None, **kwds):
    """Plot points with error bars.

    Unlike `matplotlib.pyplot.errorbar`, the ranges are limited to the
    vertical direction, and `ymin` and `ymax` values represent absolute
    positions, not relative positions to subtract or add from `y'.

    Parameters
    ----------
    x : array-like
    y : array-like
    ymin, ymax : array-like
        Extend the range for point (x[i], y[i]) from ymin[i] to ymax[i].
    ax : AxesSubplot instance or None

    Other keyword arguments are passed to `errorbar`.

    Returns
    -------
    `matplotlib.figure.Figure` instance
    """
    fig, ax = mplutil.get_figax(ax)

    yerr = np.array([y - ymin, ymax - y])

    if 'c' not in kwds and 'color' not in kwds:
        kwds.setdefault('color', 'k')
    if 'ls' not in kwds and 'linestyle' not in kwds:
        kwds.setdefault('linestyle', 'None')
    kwds.setdefault('capsize', 0)
    kwds.setdefault('marker', 'o')

    ax.errorbar(x, y, yerr=yerr, **kwds)

    ax.set_xlim(np.min(x) - 0.5, np.max(x) + 0.5)

    return fig
