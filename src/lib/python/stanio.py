import pandas as pd


def read_samples(fname, filter_svars=False):
    """Read file with combined posterior samples for a model.

    Parameters
    ----------
    fname : str
        Name of file containing samples (typically the result of the
        Snakemake rule `stan_process_`). The first row should be a
        header row of the variable names. Each following row should be
        a sampling iteration. If name ends in '.gz', the file will be
        decompressed.
    filter_svars : bool
        Filter out columns corresponding to Stan variables ('lp__',
        'accept_stat__', ...).

    Returns
    -------
    Pandas data frame with sampled variables as columns and iterations
    as rows
    """
    compression = 'gzip' if fname.endswith('.gz') else None
    df = pd.read_csv(fname, compression=compression)
    df.rename_axis(lambda x: x.replace('.', '__', 1).replace('.', '_'),
                   axis=1, inplace=True)

    if filter_svars:
        to_drop = [c for c in df.columns if c.endswith('__')]
        df.drop(to_drop, axis=1, inplace=True)

    return df
