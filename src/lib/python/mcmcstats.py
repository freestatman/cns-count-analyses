

def interval_summary(samples, width=0.95):
    """Return interval summary for  `samples`.

    Parameters
    ----------
    samples : DataFrame
    width : float
      Size of interval.  Default is 95% interval.

    Returns
    -------
    DataFrame with interval and mean for each column of `samples`.
    """
    offset = (1 - width) / 2
    bounds = [offset, 1 - offset]
    labels = ["p" + str(round(b, 3)).replace("0.", "")
              for b in bounds]

    samp_sum = samples.quantile(bounds)
    samp_sum.index = labels
    samp_sum = samp_sum.T
    samp_sum["mean"] = samples.mean()

    return samp_sum
