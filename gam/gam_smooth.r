
gam_smooth_data  <-  function(model, n=100) {
    ## Modified from http://stackoverflow.com/questions/19735149/is-it-possible-to-plot-the-smooth-components-of-a-gam-fit-with-ggplot2.
    var_smooth <- function(i){
        smooth = model$smooth[[i]]
        data = model$model

        x = seq(min(data[smooth$term]),
                max(data[smooth$term]),
                length=n)
        range <- data.frame(x=x)
        names(range) <- c(smooth$term)

        mat = PredictMat(smooth, range)
        par = smooth$first.para:smooth$last.para

        y = mat %*% model$coefficients[par]

        se = sqrt(rowSums((
            mat %*% model$Vp[par, par, drop = FALSE]) * mat))

        return(data.frame(label=smooth$label,
                          x.var=smooth$term,
                          x.val=x,
                          value=y,
                          se=se))
    }

    var_edf <- function(i){
        smooth = model$smooth[[i]]
        par = smooth$first.para:smooth$last.para
        edf <- sum(model$edf[par])
        names(edf) <- smooth$term
        return(edf)
    }

    list(edf=sapply(1:length(model$smooth), var_edf),
         dat=do.call(rbind, lapply(1:length(model$smooth),
                                   var_smooth)))
}
