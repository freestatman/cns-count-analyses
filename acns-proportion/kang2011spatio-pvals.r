#!/usr/bin/env Rscript

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 1){
    stop("usage: ./kang2011spatio-pvals.r KIND")
} else{
    kind <- args[1]
}

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng <- ng[c("cns", "hacns", "cacns", "macns")]

ng <- data.frame((ng > 0) + 0)

de = read.table(paste("../limma-de/", kind, "-de-p1.dat", sep=""))
is.de = de[,6] == 1

n.cns <- sum(ng$cns)
n.cns.de <- sum(ng[is.de, "cns"])

cns.hyper  <- function(el){
    n.el <- sum(ng[,el])
    n.el.de <-sum(ng[is.de, el])
    phyper(n.el.de - 1, n.el, n.cns - n.el, n.cns.de, lower.tail=FALSE)
}

print(sapply(c("hacns", "cacns", "macns"), cns.hyper))
