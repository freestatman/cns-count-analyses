#!/usr/bin/env Rscript

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 1){
    stop("usage: ./johnson2009functional-pvals.r KIND")
} else{
    kind <- args[1]
}

ng <- read.csv("../data/nearest-gene/johnson2009functional-nearest-counts.csv")
ng <- ng[c("cns", "hacns", "cacns", "macns")]

ng <- data.frame((ng > 0) + 0)

de = scan(paste("../johnson2009functional/", kind, "-de.dat", sep=""),
          what=integer(), quiet=TRUE)
is.de = de == 1

n.cns <- sum(ng$cns)
n.cns.de <- sum(ng[is.de, "cns"])

cns.hyper  <- function(el){
    n.el <- sum(ng[,el])
    n.el.de <-sum(ng[is.de, el])
    phyper(n.el.de - 1, n.el, n.cns - n.el, n.cns.de, lower.tail=FALSE)
}

print(sapply(c("hacns", "cacns", "macns"), cns.hyper))
