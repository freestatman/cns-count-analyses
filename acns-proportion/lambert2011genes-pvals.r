#!/usr/bin/env Rscript

ng <- read.csv("../data/nearest-gene/lambert2011genes-nearest-counts.csv")
ng <- ng[c("cns", "hacns", "cacns", "macns")]

ng <- data.frame((ng > 0) + 0)

de = scan("../lambert2011genes/region-de.dat", what=integer(), quiet=TRUE)
is.de = de == 1

n.cns <- sum(ng$cns)
n.cns.de <- sum(ng[is.de, "cns"])

cns.hyper  <- function(el){
    n.el <- sum(ng[,el])
    n.el.de <-sum(ng[is.de, el])
    phyper(n.el.de - 1, n.el, n.cns - n.el, n.cns.de, lower.tail=FALSE)
}

print(sapply(c("hacns", "cacns", "macns"), cns.hyper))
