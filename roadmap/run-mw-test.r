#!/usr/bin/env Rscript

library(parallel)
library(data.table)

args <- commandArgs(trailingOnly = TRUE)
if (length(args) < 2)
    stop("Usage: run-mw-test.r <outfile> <combined-file> [combined-file ...]")

outfile <- args[1]
fnames <- args[2:length(args)]

comps <- list(
    c("cns", "hacns"),
    c("cns", "cacns"),
    c("cns", "macns"),
    c("cns", "rand0"),
    c("hacns", "rand0"),
    c("cacns", "rand0"),
    c("macns", "rand0"),
    c("LOF", "GOF"),
    c("LOF", "rand0"),
    c("GOF", "rand0"))

comp.names <- sapply(comps, function(x) paste(x[1], x[2], sep="-"))

process.file <- function(fname){
    print(fname)
    d <- fread(fname, sep="\t",
               col.names=c("name", "size", "covered",
                           "score_sum", "score_mean0", "score_mean"))
    d[,el.type := sapply(strsplit(d$name, ".", fixed=TRUE),
                         function (x) x[[1]])]
    d[,el.type := sapply(strsplit(d$el.type, "_", fixed=TRUE),
                         function (x) x[[1]])]

    pvals <- sapply(comps,
                    function(x)
                        wilcox.test(d[el.type==x[1], score_mean],
                                    d[el.type==x[2], score_mean],
                                    alternative = "greater")$p.value)

    data.frame(fname=basename(fname),
               comp=comp.names,
               pvals=pvals)
}

results <- mclapply(fnames, process.file, mc.cores=3)

save(results, fnames, file=outfile)
