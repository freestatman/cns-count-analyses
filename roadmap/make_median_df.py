#!/usr/bin/env python3

import pandas as pd

roadmap_eids = [
    "E065",  # Heart,Aorta
    "E066",  # Other,Liver
    "E067",  # Brain,Brain Angular Gyrus
    "E068",  # Brain,Brain Anterior Caudate
    "E069",  # Brain,Brain Cingulate Gyrus
    "E070",  # Brain,Brain Germinal Matrix
    "E071",  # Brain,Brain Hippocampus Middle
    "E072",  # Brain,Brain Inferior Temporal Lobe
    "E073",  # Brain,Brain_Dorsolateral_Prefrontal_Cortex
    "E074",  # Brain,Brain Substantia Nigra
    "E081",  # Brain,Fetal Brain Male
    "E082",  # Brain,Fetal Brain Female
    "E083",  # Heart,Fetal Heart
    "E086",  # Other,Fetal Kidney
    "E088",  # Other,Fetal Lung
    "E089",  # Muscle,Fetal Muscle Trunk
    "E090",  # Muscle,Fetal Muscle Leg
    "E095",  # Heart,Left Ventricle
    "E096",  # Other,Lung
    "E097",  # Other,Ovary
    "E098",  # Other,Pancreas
    "E104",  # Heart,Right Atrium
    "E105",  # Heart,Right Ventricle
    "E107",  # Muscle,Skeletal Muscle Male
    "E108",  # Muscle,Skeletal Muscle Female
    "E113",  # Other,Spleen
]

roadmap_marks = ["H3K4me1", "H3K4me3"]

colnames = ["name", "size", "covered",
            "score_sum", "score_mean0", "score_mean"]

dfs = []
for eid in roadmap_eids:
    print("{}...".format(eid))
    for mark in roadmap_marks:
        fname = "avg/{}-{}-element-avg-combined.tsv".format(eid, mark)
        df = pd.read_csv(fname, header=None, names=colnames, sep="\t")

        df["elem"] = df["name"].str.split(".").str[0]
        df["elem"] = df["elem"].str.split("_").str[0]

        elem_means = df.groupby("elem")["score_mean"].median().reset_index()
        elem_means["eid"] = eid
        elem_means["mark"] = mark
        dfs.append(elem_means)

pd.concat(dfs).to_csv("elements-h3k4me1-h3k4me3-medians.csv", index=False)
