#!/usr/bin/env Rscript

library(ggplot2)

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 2)
    stop("Usage: density_plots.r <sample> <mark>")

samp <- args[1]
mark <- args[2]

fname <- paste("avg/", samp, "-", mark, "-element-avg-combined.tsv", sep="")
out.fname <- paste("plots/", samp, "-", mark, "-plots.pdf", sep="")

dd <- read.table(fname, stringsAsFactors=FALSE, sep="\t")
colnames(dd) <- c("name", "size", "covered", "score_sum",
                  "score_mean0", "score_mean")

elem.type <- function(x){
    sapply(strsplit(x, "[._]"), function(xx) xx[1])
}

dd$elem <- elem.type(dd$name)
dd$elem <- factor(dd$elem)

########################################################################

cat("Plotting...\n")

pdf(out.fname, width=10, height=7)

p <- ggplot(dd) + geom_density(aes(score_mean, color=elem))
print(p)

p <- ggplot(dd) + geom_density(aes(score_mean)) + facet_wrap(~ elem)
print(p)

p <- ggplot(dd) + geom_density(aes(score_mean)) + facet_wrap(~ elem, scales="free")
print(p)

p <- ggplot(dd[dd$elem %in% c("cns", "rand0"),]) +
    geom_density(aes(score_mean, color=elem))
print(p)

p <- ggplot(dd[dd$elem %in% c("cns", "rand0"),]) +
    geom_density(aes(score_mean)) + facet_wrap(~ elem, ncol=1)
print(p)

p <- ggplot(dd[dd$elem %in% c("LOF", "GOF"),]) +
    geom_density(aes(score_mean, color=elem))
print(p)

p <- ggplot(dd[dd$elem %in% c("hacns", "cacns", "macns"),]) +
    geom_density(aes(score_mean, color=elem))
print(p)

cat("Done.\n")
dev.off()
