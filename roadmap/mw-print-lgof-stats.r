#!/usr/bin/env Rscript

library(data.table)

load("mw-test-results-process.rdata")

df <- df[comp %in% c("LOF-rand0", "GOF-rand0", "LOF-GOF")]

cat("\n* Total number of samples\n\n")
dcast(df[(mark == "H3K4me1") & (comp == "LOF-GOF"), .N, by=group],
      . ~ group, value.var="N")

for (mrk in c("H3K4me1", "H3K4me3")){
    cat("\n\n* ", mrk, "\n", sep="")

    cat("\n** Count\n\n")

    dc <- df[mark == mrk,
             .(raw=sum(pvals < 0.05),
               bonf=sum(pvals.bonf < 0.05)),
             by=.(group, comp)]

    print(dcast(melt(dc, id.vars=c("group", "comp"),
                     measure.vars=c("raw", "bonf"),
                     variable.name="ptype"),
                ptype + comp ~ group))

    cat("\n** Proportion\n\n")

    dp <- df[mark == mrk,
             .(raw=mean(pvals < 0.05),
               bonf=mean(pvals.bonf < 0.05)),
             by=.(group, comp)]

    print(dcast(melt(dp, id.vars=c("group", "comp"),
                     measure.vars=c("raw", "bonf"),
                     variable.name="ptype"),
                ptype + comp ~ group))
}
