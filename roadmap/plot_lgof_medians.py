#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mplutil

mplutil.style_use(["latex"])

sigmed = pd.read_csv("elements-h3k4me1-h3k4me3-medians.csv")

info = pd.read_csv("roadmap-samp-info.csv")
info = info.sort_values(["fetal", "brain"], ascending=[False, False])
info = info.set_index("sampid")

cats = info["sample_cat"].unique().tolist()
marks = sigmed["mark"].unique().tolist()

elems = ["LOF", "GOF", "rand0"]
sigmed = sigmed[sigmed["elem"].isin(elems)]

sigmed["cat"] = sigmed["eid"].map(lambda x: info.loc[x, "sample_cat"])

grouped = sigmed.groupby(["mark", "cat"])

fig, axs = plt.subplots(nrows=len(marks), ncols=len(cats),
                        sharex=True, sharey=True,
                        figsize=(mplutil.twocol_width * 0.75, 2.8))

def plot(ax, x, y):
    ax.plot(x, y, alpha=0.4, ls="-", color=colors.base, zorder=1)
    ax.plot(x, y, alpha=0.85,
            ls="none", color=colors.base, mec="none", ms=3,
            marker="o", zorder=2)

for rid, mark in enumerate(marks):
    for cid, cat in enumerate(cats):
        ax = axs[rid, cid]
        vals = grouped.get_group((mark, cat))

        for eid in vals["eid"].unique():
            dd = vals[vals["eid"] == eid].copy()
            dd["el_idx"] = dd["elem"].map(elems.index).values
            dd = dd.sort_values("el_idx")
            plot(ax, np.arange(len(dd)), dd["score_mean"].values)

        mplutil.remove_spines(ax)

axs[0, 0].set_xlim(-0.2, len(elems) - 0.8)
axs[0, 0].locator_params(axis="y", nbins=5)
axs[0, 0].set_ylim(-0.05)
axs[-1, 0].set_xticks(np.arange(len(elems)))

elnames = elems.copy()
elnames[-1] = "random"

fig.subplots_adjust(left=0.085, right=0.975, bottom=0.17, top=0.94,
                    wspace=0.1, hspace=0.15)

for ax in axs[-1, :]:
    ax.set_xticklabels(elnames, rotation="vertical")

for ax, cat in zip(axs[0, :], cats):
    ax.text(0.5, 1.05, cat.replace("_", ", "),
            ha="center", transform=ax.transAxes)

for ax, mark in zip(axs[:, -1], marks):
    ax.text(1.05, 0.55, mark,
            transform=ax.transAxes, rotation=-90,
            ha="center", va="center", fontsize="medium")

mplutil.shared_ylabel(axs[:, 0], "Median fold change signal intensity")
# mplutil.shared_xlabel(axs[-1, :], "Element set")

fig.savefig("roadmap-lgof-medians.pdf")
