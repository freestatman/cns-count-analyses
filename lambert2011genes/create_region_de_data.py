#!/usr/bin/env python3

import numpy as np
import pandas as pd

import rdump

de = pd.read_table("region-de.dat", header=None, sep=" ")
de = de.values.reshape(-1)

cols = ["ocns", "hacns", "cacns", "macns"]
ng = pd.read_csv("../data/nearest-gene/"
                 "lambert2011genes-nearest-counts.csv")
ng = ng[cols]
ng = np.log1p(ng)

x = np.hstack([np.ones((ng.shape[0], 1)), ng.values])

data = {}
data["y"] = de
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data, "dp__region_de.data.r")
