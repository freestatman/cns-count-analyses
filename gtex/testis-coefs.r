#!/usr/bin/env Rscript

library(parallel)

tissues <- c("Cerebellum", "Cerebral Ctx", "Heart", "Kidney", "Liver",
             "Lung", "Muscle", "Ovary", "Pancreas", "Spleen",
             "Testis")

de <- as.matrix(read.csv("tissue_nomerge-de-full-up.csv"))
testis.bm <- grepl(".11...", colnames(de), fixed=TRUE)
dd <- de[,testis.bm]

ng <- read.csv("../data/nearest-gene/gtex-nearest-counts.csv")
ng <- ng[c("ocns", "hacns", "cacns", "macns")]
ng <- log1p(ng)
ragg <- scan("../data/nearest-gene/gtex-randagg.dat", quiet=TRUE)
ng$ragg <- ragg

cnames <- colnames(dd)

fits <- lapply(1:dim(dd)[2],
               function (n)
                   fit <- glm(dd[,n] ~ ng$ocns +
                                  ng$hacns + ng$cacns + ng$macns + ragg,
                              family=binomial(link="probit")))

fit.sum <- function(i){
    bounds <- confint(fits[[i]])
    data.frame(mean=coef(fits[[i]]),
               lb=bounds[,1], ub=bounds[,2],
               param=rownames(bounds),
               name=cnames[i], row.names=NULL)
}

bounds <- mclapply(1:length(fits), fit.sum, mc.cores=4)

bounds.df <- do.call(rbind, bounds)
bounds.df$param <- gsub("(Intercept)", "intercept", bounds.df$param,
                        fixed=TRUE)
bounds.df$param <- gsub("ng$", "", bounds.df$param,
                        fixed=TRUE)

bounds.df$name <- as.character(bounds.df$name)

name.items <- strsplit(bounds.df$name, ".", fixed=TRUE)

bounds.df$period <- as.numeric(sub("pr", "",
                                   sapply(name.items, function(x) x[[1]]),
                                   fixed=TRUE))

reg2.idx <- as.numeric(sapply(name.items, function(x) x[[6]]))
bounds.df$reg2 <- tissues[reg2.idx]

bounds.df <- bounds.df[order(reg2.idx),]

write.csv(bounds.df,
          file="testis-coefs.csv", row.names=FALSE, quote=FALSE)
