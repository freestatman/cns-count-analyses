#!/usr/bin/env python3

from matplotlib import gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mplutil

mplutil.style_use(["latex"])

de = pd.read_csv("tissue_nomerge-coefs-up-names.csv")
tissues = de["rlabel1"].unique()

dem = de.set_index(["period", "region1", "region2"])
dem = dem["ocns"].unstack("region1")

fig = plt.figure(figsize=(mplutil.twocol_width * 0.84, 2))
gs = gridspec.GridSpec(1, 5, width_ratios=[17, 17, 17, 3.7, 1])

max_val = max(abs(de["ocns"].min()), de["ocns"].max())

vmin, vmax = -max_val, max_val

haxs = [fig.add_subplot(gs[i]) for i in range(3)]
ims = []

for period, ax in zip(dem.index.levels[0], haxs):
    pmat = dem.loc[period]
    vals = pmat.values
    im = ax.imshow(vals,
                   cmap=colors.cmap_div,
                   vmin=vmin, vmax=vmax,
                   interpolation="none", aspect="auto")
    ims.append(im)

    ax.tick_params(length=0)
    ax.text(0.5, 1., "Period {}".format(period + 12),
            ha="center", va="bottom", fontsize="small",
            transform=ax.transAxes)

    ax.set_yticks(np.arange(pmat.shape[0]))
    ax.set_xticks(np.arange(pmat.shape[1]))

    ax.set_xticklabels(tissues[pmat.columns.values],
                       fontsize="small", rotation=90)

haxs[0].set_yticklabels(tissues[pmat.index.values], fontsize="small")
for ax in haxs[1:]:
    ax.set_yticklabels([])

for ax in haxs:
    mplutil.style_heatmap(ax)

cax = fig.add_subplot(gs[-1])
mplutil.heat_colorbar(cax, ims[-1], "OCNS coefficient")

fig.subplots_adjust(left=0.112, right=0.965, bottom=0.317, top=0.94,
                    wspace=0.06, hspace=0.32)

fig.savefig("tissue_nomerge-heatmaps-coefs-up.pdf")
