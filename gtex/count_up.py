#!/usr/bin/env python3

import sys
import pandas as pd

try:
    _, infile, outfile = sys.argv
except ValueError:
    sys.exit("Usage: {} INFILE OUTFILE".format(sys.argv[0]))

de = pd.read_csv(infile)
de.sum(0).to_csv(outfile, index=True)
