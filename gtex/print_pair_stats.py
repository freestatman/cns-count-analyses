#!/usr/bin/env python3

import sys
import pandas as pd

try:
    _, coef_file = sys.argv
except ValueError:
    sys.exit("Usage: {} <coef name file>".format(sys.argv[0]))

de = pd.read_csv(coef_file)

print("\nOverall mean")
print(de["ocns"].values.mean())

print("\nMinimum pairwise comparisons")
print(de
      .groupby("period")
      .apply(lambda d: d.loc[d["ocns"].argmin()])
      [["period", "rlabel1", "rlabel2", "ocns"]]
      .to_string())

print("\nProportion of contrasts above 0")
print((de["ocns"] > 0).groupby(de["period"]).mean().to_string())


print("\nMaximum pairwise comparisons")
print(de
      .groupby("period")
      .apply(lambda d: d.loc[d["ocns"].argmax()])
      [["period", "rlabel1", "rlabel2", "ocns"]]
      .to_string())

if de["rlabel1"].isin(["CBC", "Cerebellum"]).any():
    print("\nCBC up vs others")
    print(de
          .groupby(de["rlabel1"].isin(["CBC", "Cerebellum"]))
          ["ocns"]
          .mean()
          .to_string())

if ("Cerebellum" in de["rlabel1"].values and
    "Cerebral Ctx" in de["rlabel1"].values):
    print("\nCortex/CBC up vs others")
    print(de
          .groupby(de["rlabel1"].isin(["Cerebral Ctx", "Cerebellum"]))
          ["ocns"]
          .mean()
          .to_string())

if "Brain" in de["rlabel1"].values:
    print("\nBrain vs others")
    print(de
          .groupby(de["rlabel1"] == "Brain")
          ["ocns"]
          .mean()
          .to_string())

print("\nMean per period-tissue")
print(de .groupby(["period", "rlabel1"])["ocns"].mean().to_string())

print("\nMax per period-tissue")
print(de .groupby(["period", "rlabel1"])["ocns"].max().to_string())
