#!/usr/bin/env Rscript

library(edgeR)
library(limma)
library(rhdf5)

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 2){
    stop("usage: ./fit-gtex-de.r NAME OUTFILE")
} else{
    name <- args[1]
    outfile <- args[2]
}

datafile <- paste("../data/expression/gtex/gtex-", name, ".hdf5",
                  sep="")
## genes x samples
expr <- t(as.matrix(h5read(datafile, "/values")))
pass.lowfilter <- as.logical(h5read(datafile, "/pass_lowfilter"))
expr.pass <- expr[pass.lowfilter,]

## 32-bit integer warnings
suppressWarnings(indiv <- h5read(datafile, "/indiv") + 1)
suppressWarnings(seq_batch <- h5read(datafile, "/seq_batch") + 1)
suppressWarnings(period <- h5read(datafile, "/period") + 1)
suppressWarnings(tissue <- h5read(datafile, "/tissue") + 1)

rin <- h5read(datafile, "/rin")

x <- data.frame(pr=factor(paste(period, tissue, sep=".")),
                indiv=factor(indiv),
                sbatch=factor(seq_batch),
                rin=rin)
design <- model.matrix(~ 0 + pr + rin + sbatch, data=x)

# https://stat.ethz.ch/pipermail/bioconductor/attachments/20130530/4dcc9475/attachment.pl
nf <- calcNormFactors(expr.pass)
v <- voom(expr.pass, design, lib.size=colSums(expr.pass) * nf)
dupcor <- duplicateCorrelation(v, design=design, block=x$indiv)
v <- voom(expr.pass, design, lib.size=colSums(expr.pass) * nf,
          block=x$indiv, correlation=dupcor$consensus)
fit <- lmFit(v, design, block=x$indiv, correlation=dupcor$consensus)

save(x, design, nf, dupcor, fit,
     file=outfile)
