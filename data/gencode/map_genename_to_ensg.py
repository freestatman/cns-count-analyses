#!/usr/bin/env python3
"""Create gene name to Ensembl gene ID map.

Usage: map_genename_to_ensg.py GTF OUTFILE

Arguments:
  GTF                Gencode GTF file
  OUTFILE            Name of json file to save map to

Options:
  -h, --help
"""

from collections import defaultdict
import json
import re

from docopt import docopt

from util import pattern_matches

args = docopt(__doc__)
gtf_file = args["GTF"]
outfile = args["OUTFILE"]

symbol_ensgs = defaultdict(set)
with open(gtf_file) as gtf_fh:
    lines = (line for line in gtf_fh)

    ensg_pat = re.compile(r'gene_id "(ENSG[R]{0,1}\d+)\.\d+";')
    name_pat = re.compile(r'gene_name "([-./\w]+)";')

    matches = pattern_matches([name_pat, ensg_pat], lines, groups=[1, 1])
    for name, ensg in matches:
        symbol_ensgs[name].add(ensg)

for k in symbol_ensgs:
    symbol_ensgs[k] = list(symbol_ensgs[k])

with open(outfile, "w") as outfh:
    json.dump(symbol_ensgs, outfh, indent=2)
