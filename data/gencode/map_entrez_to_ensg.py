#!/usr/bin/env python3
"""Create Entrez to ENSG map for protein-coding genes.
"""

import json
import numpy as np
import pandas as pd

d = pd.read_csv("gencode.v22.metadata.EntrezGene.gz",
                names=["enst", "entrez"], header=None,
                compression="gzip", sep="\t")

d["entrez"] = d["entrez"].astype(str)
d["enst"] = d["enst"].str.split(".").str.get(0)

with open("gencode-v19-enst-ensg.json") as jif:
    enst_to_ensg = json.load(jif)

d["ensg"] = d["enst"].map(lambda x: enst_to_ensg.get(x, np.nan))
gd = d[d["ensg"].notnull()]
gd = gd.sort(["entrez", "ensg", "enst"]).reset_index(drop=True)
entrez_ensg = gd.groupby("entrez")["ensg"].agg(lambda x: x.unique().tolist())

# In [12]: entrez_ensg.map(len).value_counts(sort=False)
# Out[12]:
# 1    18546
# 2      212
# 3       24
# 4        2
# 5        6
# 7        7
# dtype: int64

with open("gencode-v19-entrez-ensg.json", "w") as jof:
    json.dump(entrez_ensg.to_dict(), jof)
