#!/usr/bin/env python3

import json

with open("gencode.v19.annotation-transcript-pc-locus-coords.bed") as ifh:
    fields = (line.strip().split("\t") for line in ifh)
    lens = {f[3]: int(f[2]) - int(f[1]) for f in fields}

with open("gencode-v19-locus-lengths.json", "w") as jfh:
    json.dump(lens, jfh, indent=2)
