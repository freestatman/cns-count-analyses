#!/usr/bin/env python3
"""Extract ENSG from each GTF line.

Usage: get_ensgs.py [GTF...]
"""

import fileinput
import re

from docopt import docopt

_ = docopt(__doc__)

ensg_pat = re.compile(r'gene_id "(ENSG[R]{0,1}\d+)\.\d+";')

try:
    for line in fileinput.input():
        print(ensg_pat.search(line).group(1))
except BrokenPipeError:
    pass
