#!/usr/bin/env python3
"""Get bounds for coordinates for gene.

The start and end do not have to come from the same feature (e.g.,
they can be from different trascripts).

Usage: get_bounds.py [GTF...]
"""
import fileinput
import re


def store_bounds(fields):
    bounds = {}
    for chrm, start, stop, name, strand in fields:
        if name in bounds:
            if start < bounds[name]["start"]:
                bounds[name]["start"] = start
            if stop > bounds[name]["stop"]:
                bounds[name]["stop"] = stop
        else:
            bounds[name] = {"chrm": chrm,
                            "start": start,
                            "stop": stop,
                            "strand": strand}
    return bounds


def parse_lines(lines, field_idxs):
    ensg_pat = re.compile(r'gene_id "(ENSG[R]{0,1}\d+)\.\d+";')
    name_pat = re.compile(r'gene_name "([-./\w]+)";')

    for line in lines:
        chrm, start, stop, strand, info = split_limit(line, "\t",
                                                      field_idxs)
        gname = name_pat.search(info).group(1)
        ensg = ensg_pat.search(info).group(1)
        name = "|".join([gname, ensg])
        start, stop = int(start), int(stop)

        yield chrm, start, stop, name, strand


def split_limit(text, delim, idxs):
    """Split `text' by `delim` and limit to indices `idxs`."""
    fields = text.split(delim)
    return [fields[idx] for idx in idxs]

if __name__ == "__main__":
    from docopt import docopt
    _ = docopt(__doc__)

    lines = (ln for ln in fileinput.input())
    bounds = store_bounds(parse_lines(lines, [0, 3, 4, 6, 8]))

    try:
        for gene, info in bounds.items():
            bed_fields = [info["chrm"], str(info["start"]), str(info["stop"]),
                          gene, "", info["strand"]]
            print("\t".join(bed_fields))
    except BrokenPipeError:
        pass
