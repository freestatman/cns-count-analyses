import locus_coords as cut
import pytest


def test_locus_coords():
    spacings = [("chr1", 10, 20, "a", None),
                ("chr1", 30, 40, "b", 10),
                ("chr1", 80, 100, "c", 40),
                ("chr1", 90, 110, "d", 0),
                ("chr1", 130, 145, "e", 20),
                ("chr2", 10, 25, "f", None),
                ("chr2", 15, 40, "g", 0),
                ("chr2", 80, 110, "h", 40),
                ("chr2", 120, 130, "i", 10),
                ("chr3", 10, 25, "j", None),
                ("chr3", 50, 200, "k", 25),
                ("chr3", 300, 400, "l", 100),
                ("chr3", 350, 500, "m", 0)]

    result = list(cut.locus_coords(spacings))
    assert result == [("chr1", 0, 25, "a"),
                      ("chr1", 25, 60, "b"),
                      ("chr1", 120, None, "e"),
                      ("chr2", 60, 115, "h"),
                      ("chr2", 115, None, "i"),
                      ("chr3", 0, 37, "j"),
                      ("chr3", 38, 250, "k")]


def test_locus_coords_end_values():
    spacings = [("chr1", 10, 20, "a", None),
                ("chr1", 30, 40, "b", 10),
                ("chr1", 80, 100, "c", 40),
                ("chr1", 90, 110, "d", 0),
                ("chr1", 130, 145, "e", 20),
                ("chr2", 10, 25, "f", None),
                ("chr2", 15, 40, "g", 0),
                ("chr2", 80, 110, "h", 40),
                ("chr2", 120, 130, "i", 10),
                ("chr3", 10, 25, "j", None),
                ("chr3", 50, 200, "k", 25),
                ("chr3", 300, 400, "l", 100),
                ("chr3", 350, 500, "m", 0)]

    end_values = {"chr1": 200, "chr2": 250, "chr3": 750}

    result = list(cut.locus_coords(spacings, end_values=end_values))
    assert result == [("chr1", 0, 25, "a"),
                      ("chr1", 25, 60, "b"),
                      ("chr1", 120, 200, "e"),
                      ("chr2", 60, 115, "h"),
                      ("chr2", 115, 250, "i"),
                      ("chr3", 0, 37, "j"),
                      ("chr3", 38, 250, "k")]
