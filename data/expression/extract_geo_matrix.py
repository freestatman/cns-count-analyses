#!/usr/bin/env python3
"""Extract expression and sample information from series matrix file.

Usage: extract_geo_matrix.py [options] MATRIX

This will create two output files from NAME_series_matrix.txt.gz:
NAME-expression.txt and NAME-info.txt.

Arguments:
  MATRIX            File name ending in NAME_series_matrix.txt.gz

Options:
  -h, --help        Print this message.
  --fields=F        Comma-separed list of field names
                    [default: Sample_characteristics_ch1]
"""
import gzip
from docopt import docopt


def write_sample_info(matrix_fh, info_fh, fields):
    """Write sample info lines.

    Parameters
    ----------
    matrix_fh : file handle
        File handle for series matrix file
    info_fh : file handle
        File handle to write lines to
    fields : list
        List of fields to write, without the leading "!".
    """
    fields = ["!" + f for f in fields]
    for info_line in matrix_fh:
        for f in fields:
            if info_line.startswith(f):
                info_fh.write(info_line)
                break
        else:
            if info_line.startswith("!series_matrix_table_begin"):
                break


def write_expression_info(matrix_fh, expr_fh):
    """Write expression matrix.

    Parameters
    ----------
    matrix_fh : file handle
        File handle for series matrix file.  The next line of should
        be the first line of the matrix (i.e., rigt after
        "!series_matrix_table_begin").
    expr_fh : file handle
        File handle to write lines to
    """
    reached_end = False
    for data_line in matrix_fh:
        if data_line.startswith("!series_matrix_table_end"):
            reached_end = True
            break
        expr_fh.write(data_line)
    assert reached_end

if __name__ == "__main__":
    args = docopt(__doc__)
    fname = args["MATRIX"]
    fields = args["--fields"].split(",")

    with gzip.open(fname, mode="rt") as fh:
        sinfo_fname = fname.replace("_series_matrix.txt.gz",
                                    "-info.txt")
        print("Writing sample info from {} to {}".format(fname,
                                                         sinfo_fname))

        with open(sinfo_fname, "w") as ofh:
            write_sample_info(fh, ofh, fields)

        expr_fname = fname.replace("_series_matrix.txt.gz",
                                   "-expression.txt")
        print("Writing expression values from {} to {}".format(fname,
                                                               expr_fname))
        with open(expr_fname, "w") as ofh:
            write_expression_info(fh, ofh)
