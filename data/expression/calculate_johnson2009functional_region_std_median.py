#!/usr/bin/env python3
"""Take standardized median of region medians.
"""

import h5py
import pandas as pd

datafile = "johnson2009functional.hdf5"
with h5py.File(datafile, mode="r") as hf:
    dat = pd.DataFrame(hf["values"][:])
    dr = hf["dr"][:]

meds = dat.groupby(dr, axis=1).median().median(axis=1)
meds_std = (meds - meds.values.mean()) / meds.values.std()

meds_std.to_csv("johnson2009functional-region-std-median.dat", sep=" ",
                header=None, index=None)
