#!/usr/bin/env python3

import re

import numpy as np
import pandas as pd

with open("GSE13344-GPL5175-expression.txt") as fh:
    header = next(fh)
    samples = [s[1:-1] for s in header.strip().split("\t")][1:]

### Clean up sample info.

## Lines: region, individual, age, and PMI
with open("GSE13344-GPL5175-info.txt") as fh:
    sinfo = {k: line.strip().split("\t")[1:]
             for k, line in zip(["region", "indiv", "age", "pmi"], fh)}

indiv_re = re.compile(r'"[0-9]+ w\.g\., case #([0-9]+)"')
sinfo["indiv"] = [int(indiv_re.search(x).group(1)) for x in sinfo["indiv"]]

age_re = re.compile(r'"Age: ([0-9]+) weeks gestation"')
sinfo["age"] = [int(age_re.search(x).group(1)) for x in sinfo["age"]]

## Drop PMI because all values are "Post-Mortem Interval <1hr".
sinfo.pop("pmi")


def clean_region_line(ln):
    # 'name, [side], replicate"'
    name, *rest = ln[1:-1].split(",")
    if len(rest) == 2:
        side = rest[0].strip()
    else:
        side = None
    return name, side

region_sides = list(map(clean_region_line, sinfo["region"]))

region_abbrvs = {"cerebellum": "CBC",
                 "dorsolateral prefrontal neocortex": "DFC",
                 "hippocampus": "HIP",
                 "medial prefrontal neocortex": "MFC",
                 "mediodorsal thalamus": "MD",
                 "motor-somatosensory neocortex": "MSC",
                 "occipital visual neocortex": "V1C",
                 "orbital prefrontal neocortex": "OFC",
                 "parietal association neocortex": "IPC",
                 "striatum: putamen and caudate nucleus": "STR",
                 "temporal association cortex": "STC",
                 "temporal auditory neocortex": "A1C",
                 "ventrolateral prefrontal neocortex": "VFC"}

sinfo["region"] = [region_abbrvs[x[0]] for x in region_sides]

side_abbrvs = {"left": "L", "right": "R", None: np.nan}
sinfo["side"] = [side_abbrvs[x[1]] for x in region_sides]

si = pd.DataFrame(sinfo)
si.index = samples
si.index.name = "sample"

## Reset indiv to 0, 1...
si["indiv"] = np.unique(si.indiv.values, return_inverse=True)[1]

si.to_csv("johnson2009functional-sampleinfo.csv")
