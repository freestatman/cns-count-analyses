#!/usr/bin/env python3
"""Take standardized median of NCX areas medians.
"""

import h5py
import pandas as pd

datafile = "johnson2009functional.hdf5"
with h5py.File(datafile, mode="r") as hf:
    dat = pd.DataFrame(hf["values"][:])
    db = hf["db"][:]
    dr = hf["dr"][:]

ncx_bm = dr == 0
dat_ncx = dat.iloc[:, ncx_bm]
db_ncx = db[ncx_bm]

meds = dat_ncx.groupby(db_ncx, axis=1).median().median(axis=1)
meds_std = (meds - meds.values.mean()) / meds.values.std()

meds_std.to_csv("johnson2009functional-ncx-std-median.dat", sep=" ",
                header=None, index=None)
