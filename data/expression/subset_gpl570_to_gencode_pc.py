#!/usr/bin/env python3
"""Subset GPL570 table to genes in gencode.
"""

import json
import numpy as np
import pandas as pd

gi = pd.read_csv("gpl570-geneinfo.csv")

with open("../gencode/gencode-v19-entrez-ensg.json") as jif:
    entrez_ensgs = json.load(jif)
entrez_ensgs = {int(k): v for k, v in entrez_ensgs.items()}
entrezs = set(entrez_ensgs.keys())

in_gencode = gi["entrez"].isin(entrezs)
gig = gi[in_gencode].copy()

gig["ensg"] = gig["entrez"].map(lambda x: "|".join(entrez_ensgs[x]))
nuniq = gig["ensg"].str.count(r"\|") + 1

# In [6]: gig.nuniq.value_counts(sort=False)
# Out[7]:
# 1    38089
# 2      379
# 3       20
# 4        3
# 5        8
# 9        8
# Name: nuniq, dtype: int64

## Drop IDs with more than one ENSG.
gig = gig[nuniq == 1]

# In [93]: gig.groupby("arrayid")["arrayid"].size().value_counts()
# Out[93]:
# 1     37185
# 2       320
# 3        24
# 21        4
# 5         4
# 12        3
# 7         3
# 4         2
# 9         1
# 8         1
# 6         1
# dtype: int64

affycounts = gig.groupby("arrayid")["arrayid"].size()
gig = gig[gig["arrayid"].map(affycounts) == 1]

gig.to_csv("gpl570-geneinfo-gencode.csv", index=False)
