#!/usr/bin/env python3
"""Extract gene information GPL570-13270.txt.
"""

import pandas as pd


def parse_info_lines(lines):
    for line in lines:
        fields = line.split('\t')

        arrayid = fields[0]

        gene_name = fields[10].strip()

        entrez_ids = [i.strip() for i in fields[11].split('///')]
        entrez_ids = [int(i) for i in entrez_ids if i]

        for eid in entrez_ids:
            yield arrayid, gene_name, eid

if __name__ == '__main__':
    with open('GPL570-13270.txt') as ifh:
        lines = (ln for ln in ifh if not ln.startswith('#'))
        header = next(lines)
        assert header.split('\t')[11] == 'ENTREZ_GENE_ID'

        df = pd.DataFrame(parse_info_lines(lines),
                          columns=['arrayid', 'symbol', 'entrez'])

    df.to_csv('gpl570-geneinfo.csv', index=False)
