#!/usr/bin/env python3
"""Subset GTEx expression data.

* Limit to genes in kang2011spatio array data set.
* Select subset of tissues.
* Code categorical sample information columns.
* Match age bins to kang2011spatio periods.
"""

from collections import OrderedDict
import pandas as pd

sel_tissues = [("Adipose - Subcutaneous", False),
               ("Adipose - Visceral (Omentum)", False),
               ("Adrenal Gland", False),
               ("Artery - Aorta", False),
               ("Artery - Coronary", False),
               ("Artery - Tibial", False),
               ("Bladder", False),
               ("Brain - Amygdala", True),
               ("Brain - Anterior cingulate cortex (BA24)", True),
               ("Brain - Caudate (basal ganglia)", True),
               ("Brain - Cerebellar Hemisphere", True),
               ("Brain - Cerebellum", True),
               ("Brain - Cortex", True),
               ("Brain - Frontal Cortex (BA9)", True),
               ("Brain - Hippocampus", True),
               ("Brain - Hypothalamus", True),
               ("Brain - Nucleus accumbens (basal ganglia)", True),
               ("Brain - Putamen (basal ganglia)", True),
               ("Brain - Spinal cord (cervical c-1)", True),
               ("Brain - Substantia nigra", True),
               ("Breast - Mammary Tissue", False),
               ("Cells - EBV-transformed lymphocytes", False),
               ("Cells - Leukemia cell line (CML)", False),
               ("Cells - Transformed fibroblasts", False),
               ("Cervix - Ectocervix", False),
               ("Cervix - Endocervix", False),
               ("Colon - Sigmoid", False),
               ("Colon - Transverse", False),
               ("Esophagus - Gastroesophageal Junction", False),
               ("Esophagus - Mucosa", False),
               ("Esophagus - Muscularis", False),
               ("Fallopian Tube", False),
               ("Heart - Atrial Appendage", False),
               ("Heart - Left Ventricle", True),
               ("Kidney - Cortex", True),
               ("Liver", True),
               ("Lung", True),
               ("Minor Salivary Gland", False),
               ("Muscle - Skeletal", True),
               ("Nerve - Tibial", False),
               ("Ovary", True),
               ("Pancreas", True),
               ("Pituitary", False),
               ("Prostate", False),
               ("Skin - Not Sun Exposed (Suprapubic)", False),
               ("Skin - Sun Exposed (Lower leg)", False),
               ("Small Intestine - Terminal Ileum", False),
               ("Spleen", True),
               ("Stomach", False),
               ("Testis", True),
               ("Thyroid", False),
               ("Uterus", False),
               ("Vagina", False),
               ("Whole Blood", False),]

## Metadata

sattr = pd.read_csv("GTEx_Data_V6_Annotations_SampleAttributesDS.txt",
                    sep="\t", index_col=0)

sattr = sattr[sattr["SMTSD"].isin([t for t, use in sel_tissues if use])]

scols = OrderedDict([("SMNABTCHT", "iso_batch"),
                     ("SMGEBTCH", "seq_batch"),
                     ("SMRIN", "rin"),
                     # ("SMTS", "tissue_type"),
                     ("SMTSD", "tissue"),])

sattr = sattr.reindex(columns=list(scols.keys()))
sattr.rename(columns=scols, inplace=True)

sattr["indiv"] = sattr.index.str.split("-").str[:2].str.join("-")

subjs = pd.read_csv("GTEx_Data_V6_Annotations_SubjectPhenotypesDS.txt",
                    sep="\t", index_col=0)

# Match periods with adult periods from kang2011spatio.
periods = {"20-29": 0,
           "30-39": 0,
           "40-49": 1,
           "50-59": 1,
           "60-69": 2,
           "70-79": 2,}
sattr["period"] = sattr["indiv"].map(subjs["AGE"]).map(periods)

tissue_cat = sattr["tissue"].astype("category")
tissues = pd.Series(tissue_cat.cat.categories)
tissues.index.name, tissues.name = "code", "tissue"
sattr["tissue"] = tissue_cat.cat.codes

for col in ["indiv", "iso_batch", "seq_batch"]:
    sattr[col] = sattr[col].astype("category").cat.codes

## Expression

with pd.get_store("../kang2011spatio-p1.h5") as ks:
    kensgs = ks["geneinfo"]["ensg"]
kensgs.name = "ensg"

counts = pd.read_csv("GTEx_Analysis_v6_RNA-seq_RNA-SeQCv1.1.8_gene_reads.gct.gz",
                     header=2, index_col=0, sep="\t", compression="gzip")

genes = counts[["Description"]].copy().reset_index()
genes.rename(columns={"Description": "gname", "Name": "full_ensg"},
             inplace=True)
genes.index = genes["full_ensg"].str.split(".").str[0]
genes.index.name = "ensg"

genes = genes.reindex(index=kensgs.values)
genes.dropna(how="any", inplace=True)

counts = counts.reindex(index=genes["full_ensg"], columns=sattr.index)
counts.index = genes.index
counts.dropna(how="any", axis=1, inplace=True)
sattr = sattr.reindex(index=counts.columns)

sattr.index.name = "sampid"
sattr.reset_index(inplace=True)

sattr.index = ["s{}".format(i) for i in range(sattr.shape[0])]
sattr.index.name = "sample"
counts.columns = sattr.index

counts.to_csv("gtex-counts-subset.csv")
sattr.to_csv("gtex-sattr-subset.csv")
tissues.to_csv("gtex-tissues-subset.csv", header=True)
genes.to_csv("gtex-genes-subset.csv")
