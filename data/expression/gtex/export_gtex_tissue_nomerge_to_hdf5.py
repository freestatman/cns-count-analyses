#!/usr/bin/env python3

import numpy as np
import pandas as pd
import h5py

sel_tissues = [("Brain - Amygdala", False),
               ("Brain - Anterior cingulate cortex (BA24)", False),
               ("Brain - Caudate (basal ganglia)", False),
               ("Brain - Cerebellar Hemisphere", False),
               ("Brain - Cerebellum", True),
               ("Brain - Cortex", True),
               ("Brain - Frontal Cortex (BA9)", False),
               ("Brain - Hippocampus", False),
               ("Brain - Hypothalamus", False),
               ("Brain - Nucleus accumbens (basal ganglia)", False),
               ("Brain - Putamen (basal ganglia)", False),
               ("Brain - Spinal cord (cervical c-1)", False),
               ("Brain - Substantia nigra", False),
               ("Heart - Left Ventricle", True),
               ("Kidney - Cortex", True),
               ("Liver", True),
               ("Lung", True),
               ("Muscle - Skeletal", True),
               ("Ovary", True),
               ("Pancreas", True),
               ("Spleen", True),
               ("Testis", True),]

with pd.get_store("gtex.h5", mode="r") as store:
    ge = store["ge"]
    si = store["sampleinfo"]
    tissues = store["tissues"]

tissues = tissues[tissues.isin([r for r, use in sel_tissues if use])]
si = si[si["tissue"].isin(tissues.index)]

ge = ge.reindex(columns=si.index)

## Need to remove low counts (pg. 69, sec 15.3 of Limma manual).  Here
## it is suggested 5-10 counts in at least 3 libraries:
## https://support.bioconductor.org/p/66179/.
min_counts = 10
min_samples = 3
pass_lowfilter = (ge >= min_counts).sum(1) >= min_samples

# Log-counts per million (according to law2014voom).
cpm = np.log2((ge + 0.5) / (ge.sum(0) + 1) * 1e6)
cpm = cpm.groupby([si["period"], si["tissue"]], axis=1).median()
cpm = cpm.groupby(axis=1, level="period").median()
cpm = (cpm - cpm.values.mean()) / cpm.values.std()

with h5py.File("gtex-tissue_nomerge.hdf5", mode="w") as hf:
    hf.create_dataset("values", data=ge.values, compression="gzip")
    hf.create_dataset("pass_lowfilter", data=pass_lowfilter.values,
                      compression="gzip")
    hf.create_dataset("cpm_std", data=cpm.values, compression="gzip")

    hf.create_dataset("rin", data=si["rin"].values)
    hf.create_dataset("period", data=si["period"].values)

    for col in ["indiv", "tissue", "iso_batch", "seq_batch"]:
        hf.create_dataset(col,
                          data=si[col].astype("category").cat.codes.values)

tissues.to_csv("gtex-tissue_nomerge-tissues.txt", index=False)
