#!/usr/bin/env python3
"""Subset and prepare johnson2009functional data.

Subset to gencode v19 protein-coding genes.  If gene is assigned to
more than one transcript cluster ID, take the one with the most direct
probes.  Breaks ties with coverge score.
"""

import numpy as np
import pandas as pd

from pdutil import filter_repeats

ge = pd.read_csv("GSE13344-GPL5175-expression.txt", sep="\t", index_col=0)
si = pd.read_csv("johnson2009functional-sampleinfo.csv", index_col=0)
geneinfo = pd.read_csv("gpl5175-geneinfo-gencode.csv", index_col=0)

## Limit to gencode genes.
geg = ge[ge.index.isin(geneinfo.index)]
geg.index.name = "affyid"
geneinfo = geneinfo[geneinfo.index.isin(geg.index)]

geneinfo = filter_repeats(geneinfo, "gc_symbol", ["dir_probes", "coverage"])
geg = geg[geg.index.isin(geneinfo.index)]

assert (geneinfo.index == geg.index).all()

## Add integer labels for some sample features.

regions = ["OFC", "DFC", "VFC", "MFC", "MSC", "IPC", "A1C",
           "STC", "V1C", "HIP", "STR", "MD", "CBC"]
reg_ncx = {"OFC": 0, "DFC": 0, "VFC": 0, "MFC": 0, "MSC": 0,
           "IPC": 0, "A1C": 0, "STC": 0, "V1C": 0,
           "HIP": 1, "STR": 2, "MD" : 3, "CBC": 4}

si["db"] = si["region"].map(regions.index)
si["dr"] = si["region"].map(reg_ncx)
si["di"] = np.unique(si["indiv"].values, return_inverse=True)[1]

geg.to_csv("johnson2009functional-ge-subset.csv")
si.to_csv("johnson2009functional-sampleinfo-subset.csv")
geneinfo.to_csv("johnson2009functional-geneinfo-subset.csv")
