#!/usr/bin/env python3

import numpy as np
import pandas as pd

with pd.get_store("kang2011spatio-p1.h5", mode="r") as store:
    si = store["sampleinfo"]

print("\nNumber of samples: {}".format(si.shape[0]))

print("\nNumber of individuals: {}".format(si["indiv"].nunique()))

print("\nSamples per time period:")
print(si.groupby((si["ds"] + 1).values).size().to_string())

pcw_bm = si["age"].str.contains("PCW")
pcw_ages = si[pcw_bm]["age"].str.split(" ").str[0].astype(np.float)
print("\nMinimum age: {} PCW".format(pcw_ages.min()))

y_bm = si["age"].str.contains("Y")
y_ages = si[y_bm]["age"].str.split(" ").str[0].astype(np.int)
print("\nMaximum age: {} Y".format(y_ages.max()))
