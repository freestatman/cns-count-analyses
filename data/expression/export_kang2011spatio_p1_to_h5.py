#!/usr/bin/env python3

import pandas as pd

ge = pd.read_csv("kang2011spatio-p1-ge-subset.csv",
                 index_col=0)
dabg = pd.read_csv("kang2011spatio-p1-dabg-subset.csv",
                   index_col=0)
pass_dabg = pd.read_csv("kang2011spatio-p1-pass_dabg-subset.csv",
                        index_col=0, header=None, squeeze=True)

si = pd.read_csv("kang2011spatio-p1-sampleinfo-subset.csv",
                 index_col=0)
gi = pd.read_csv("kang2011spatio-p1-geneinfo-subset.csv",
                 index_col=0)

## np.nans will cause issue with mixed type saving to HDF5.
si["side"].fillna("", inplace=True)
gi["symbol"].fillna("", inplace=True)

h5name = "kang2011spatio-p1.h5"
with pd.get_store(h5name, mode="w", complevel=4) as store:
    store["ge"] = ge
    store["dabg"] = dabg
    store["pass_dabg"] = pass_dabg
    store["sampleinfo"] = si
    store["geneinfo"] = gi
