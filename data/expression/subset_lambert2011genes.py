#!/usr/bin/env python3
"""Subset to gencode v19 protein-coding genes.
"""

import pandas as pd
import json

ge = pd.read_csv("GSE21858-expression.txt", index_col=0, sep="\t")
si = pd.read_csv("lambert2011genes-sampleinfo.csv", index_col=0)

gi = pd.read_csv("gpl570-geneinfo-gencode.csv", index_col=0)

geg = ge[ge.index.isin(gi.index)]
geg.index.name = "affyid"

gi = gi.reindex(index=geg.index)

## np.nans will cause issue with mixed type saving to HDF5.
si.side.fillna("", inplace=True)

## Average genes with multiple probe sets.
geg = geg.groupby(gi["symbol"], sort=False).mean()

gi_avg = gi[~gi.duplicated(["symbol", "entrez", "ensg"])].copy()
n_ids = gi.groupby("symbol")["symbol"].size()
gi_avg["n_ids"] = gi_avg["symbol"].map(n_ids)

h5name = "lambert2011genes.h5"
with pd.get_store(h5name, mode="w", complevel=4) as store:
    store["ge"] = geg
    store["sampleinfo"] = si
    store["geneinfo"] = gi_avg
    store["geneinfo_all"] = gi
