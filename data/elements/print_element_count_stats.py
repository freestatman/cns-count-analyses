#!/usr/bin/env python3

import pandas as pd

elements = [("cns", "cns-pc8way-gt400-merged-hg19.bed"),
            ("ocns", "ocns-pc8way-gt400-merged-hg19.bed"),
            ("hacns", "hacns-coordinates-hg19.bed"),
            ("cacns", "cacns-coordinates-hg19.bed"),
            ("macns", "macns-coordinates-hg19.bed")]

counts = {}
for el, elfile in elements:
    with open(elfile) as fh:
        counts[el] = len(fh.readlines())
counts = pd.Series(counts)
counts.sort_values(ascending=False, inplace=True)

print("\n\nCounts")
print(counts.to_string())

print("\n\nFraction of total")
print((counts.drop(["ocns", "cns"]) / counts["cns"]).to_string())
