#!/usr/bin/env python3

import sys

import numpy as np
import pandas as pd

try:
    _, nearest_matrix, outfile = sys.argv
except (ValueError, IndexError):
    sys.exit("Usage: {} NEAREST OUTFILE".format(sys.argv[0]))

d = pd.read_csv(nearest_matrix).drop("ensg", axis=1)
np.log1p(d).median(1).to_csv(outfile, sep=" ", header=None, index=None)
