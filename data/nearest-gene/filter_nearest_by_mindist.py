#!/usr/bin/env python3
"""Remove elements that are not at least as far as distance.

USAGE: filter_nearest_by_mindist.py DIST
"""

import fileinput
import sys

try:
    min_dist = int(sys.argv[1])
except (IndexError, ValueError):
    sys.exit(__doc__)

for line in fileinput.input(files="-"):
    distance = int(line.split("\t")[10])
    if abs(distance) >= min_dist:
        sys.stdout.write(line)
