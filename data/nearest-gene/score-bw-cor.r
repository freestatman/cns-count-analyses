#!/usr/bin/env Rscript

ng <- read.csv("kang2011spatio-nearest-counts-scorebw.csv")
ng <- ng[,colnames(ng) != "ensg"]

print("150-300 vs 300-400")
print(cor(ng$ocns0, ng$ocns1, method="spearman"))

print("150-300 vs 400-500")
print(cor(ng$ocns0, ng$ocns2, method="spearman"))

print("150-300 vs 500-600")
print(cor(ng$ocns0, ng$ocns3, method="spearman"))
