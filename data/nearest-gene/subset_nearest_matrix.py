#!/usr/bin/env python3
"""Subset nearest feature matrix to genes from expression study.

Usage: subset_nearest_matrix.py MATRIX EXPR OUTFILE

Arguments:
  MATRIX
      Nearest feature matrix (genes x elements)

  EXPR
      A Pandas HDF5 file with a 'geneinfo' object that has an ENSG
      column

  OUTFILE
"""

import sys
import pandas as pd

try:
    _, mfile, efile, outfile = sys.argv
except ValueError:
    sys.exit(__doc__)

with pd.HDFStore(efile) as store:
    gi = store["geneinfo"]

d = pd.read_csv(mfile, index_col=0)
d.reindex(index=gi.ensg).to_csv(outfile)
