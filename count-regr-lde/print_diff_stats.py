#!/usr/bin/env python3

import sys

import numpy as np
import pandas as pd
from scipy.stats import norm

import mcmcstats
import stanio

try:
    _, sampfile = sys.argv
except ValueError:
    sys.exit("Usage: {} <sample-file>".format(sys.argv[0]))

samps = stanio.read_samples(sampfile, filter_svars=True)

print("\n\nCoefficients")
print(mcmcstats.interval_summary(samps))

n_pred = 10
n_pred_l = np.log1p(n_pred)

p0 = norm.cdf(samps["beta__1"].values)
p1 = norm.cdf(samps["beta__1"].values + n_pred_l * samps["beta__2"].values)

print("\n\nProbability at {} predictor units for beta__2".format(n_pred))
dint = mcmcstats.interval_summary(pd.Series(p1 - p0))
print(dint.to_string())
