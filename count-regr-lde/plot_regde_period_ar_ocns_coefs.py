#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

import colors
import mcmcstats
import mplutil
import periodax
from plots import pointrange
import stanio


mplutil.style_use(["latex"])

periods = np.arange(1, 16)
ocns_cols = ["p{}_beta__2".format(p) for p in periods]

dp_fname = "samples/dp__regde_ragg-all-chains-all-periods.j4.csv"
dp = stanio.read_samples(dp_fname, filter_svars=True)
dp_intervals = mcmcstats.interval_summary(dp[ocns_cols])

dpar_fname = "samples/dp__regde_ar_ragg-all-chains-all-periods.j4.csv"
dpar = stanio.read_samples(dpar_fname, filter_svars=True)
dpar_ocns_intervals = mcmcstats.interval_summary(dpar[ocns_cols[1:]])

fig, ax = plt.subplots(figsize=(mplutil.onecol_width, 2))

marker_size = 4.5

ocns_intervals = {"dp": dp_intervals,
                  "dpar": dpar_ocns_intervals}

ocns_xs = {"dp": periods - 0.25,
           "dpar": periods[1:] + 0.25}

dcolors = {"dp": colors.base,
           "dpar": colors.primary}

for dset in ["dp", "dpar"]:
    dset_intervals = ocns_intervals[dset]
    pointrange(ocns_xs[dset],
               dset_intervals["mean"],
               dset_intervals["p025"], dset_intervals["p975"],
               color=dcolors[dset],
               mec="none",
               markersize=marker_size, ax=ax)

periodax.period_vlines(ax, start=1)

mplutil.remove_spines(ax)
# ax.set_ylim(0)
ax.set_xlim(0.5, 15.6)
ax.tick_params(axis="x", length=0)
ax.locator_params(axis="y", nbins=6)

ax.set_xticks(periods)

ax.set_ylabel("OCNS coefficient")

ax.text(7.9, -0.06, "Adjusted for previous period",
        color=dcolors["dpar"],
        fontsize="small", ha="center", va="center",
        zorder=2)

ax.axhline(0, color=colors.light_gray, ls="--", zorder=1)

fig.subplots_adjust(left=0.13, right=0.995, bottom=0.24, top=0.97,
                    wspace=0.15, hspace=0.1)

periodax.label_groups(ax, start=1)

fig.savefig("ocns-ar-ocns-coefs.pdf")
