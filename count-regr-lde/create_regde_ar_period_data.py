#!/usr/bin/env python3

import sys

import numpy as np
import pandas as pd

import rdump

try:
    period = int(sys.argv[1])
except (ValueError, IndexError):
    sys.exit("Usage: {} PERIOD".format(sys.argv[0]))
period_idx = period - 1
ar_idx = period_idx - 1

if ar_idx < 0:
    sys.exit("No previous period to use as predictor")

de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")

cols = ["ocns", "hacns", "cacns", "macns"]
ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng = ng[cols]
ng = np.log1p(ng)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values,
               de.values[:, ar_idx, None]])

data = {}
data["y"] = de.values[:, period_idx]
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data,
                 "dp__regde_ar_period{}.data.r".format(period))
