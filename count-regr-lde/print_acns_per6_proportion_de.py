#!/usr/bin/env python3

import pandas as pd

de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")
de = de.iloc[:, 5]

cols = ["ocns", "hacns", "cacns", "macns"]
ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng = ng[cols]

print("Overall DE proportion")
print(de.mean())

for acns in cols[1:]:
    print("\n\nDE proportion for genes near a {}".format(acns))
    print(de.groupby(ng[acns] > 0).size().to_string())
    print("")
    print(de.groupby(ng[acns] > 0).mean().to_string())

    print("\n\nDE proportion for genes near N {}".format(acns))
    print(de.groupby(ng[acns]).size().to_string())
    print("")
    print(de.groupby(ng[acns]).mean().to_string())
