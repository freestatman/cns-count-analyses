#!/usr/bin/env python3

import pandas as pd

import mcmcstats
import stanio

dset_files = {
    ("reg", "k"): "samples/dp__regde_period6-all-chains.j4.csv",
    ("ncx", "k"): "samples/dp__ncxde_period6-all-chains.j4.csv",
    ("reg", "j"): "../johnson2009functional/samples/dp__region_de-all-chains.j4.csv",
    ("ncx", "j"): "../johnson2009functional/samples/dp__ncx_de-all-chains.j4.csv",
    ("reg", "n"): "samples/dp__regde_nofc_period6-all-chains.j4.csv",
    ("ncx", "n"): "samples/dp__ncxde_nofc_period6-all-chains.j4.csv",
    ("reg", "a"): "../anova-de/samples/dp__region_de_period6-all-chains.j4.csv",
    ("ncx", "a"): "../anova-de/samples/dp__ncx_de_period6-all-chains.j4.csv",
}

el_cols = ["beta__{}".format(i) for i in range(2, 6)]

dfs = []
for key, dfile in dset_files.items():
    trace = stanio.read_samples(dfile, filter_svars=True)[el_cols]

    df_wide = mcmcstats.interval_summary(trace)
    df_wide.index.name = "coef"
    df_wide.columns.name = "meas"

    sdf_long = df_wide.unstack().reset_index(name="value")
    sdf_long["de_kind"] = key[0]
    sdf_long["dset"] = key[1]
    dfs.append(sdf_long)

pd.concat(dfs).to_csv("dset-combined-per6-coef-summary.csv", index=None)
