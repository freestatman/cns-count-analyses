#!/usr/bin/env python3

import sys
import pandas as pd

try:
    _, period = sys.argv
except ValueError:
    sys.exit("usage: print_regde_proportions.py <period>")

try:
    period = int(period)
except ValueError:
    sys.exit("Period must be an integer")

period_idx = period - 1
de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")
de = de.iloc[:, period_idx]
de.name = "de"

ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv",
                 usecols=["ocns"])
d = pd.concat([ng, de], axis=1)

print("\n\n* Overall DE stats")
print("\nNumber of DE genes: {}".format(d["de"].sum()))
print("\nProportion DE: {}".format(d["de"].mean()))

for thresh in [1, 10, 50, 100]:
    print("\n\n* At or above {} OCNSs".format(thresh))
    above_thresh = d["ocns"] >= thresh
    dg = d.groupby(above_thresh)
    print("\nNumber of genes")
    print(dg["ocns"].size().to_string())
    print(above_thresh.mean())

    print("\nProportion of regional DE")
    print(dg["de"].mean().to_string())
