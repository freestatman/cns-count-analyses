#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import mplutil
import colors
from plots import pointrange

mplutil.style_use(["latex"])

coef_df = pd.read_csv("dset-combined-per6-coef-summary.csv")
dg = coef_df.groupby(["de_kind", "dset"])

fig, axs = plt.subplots(nrows=2, ncols=1,
                        figsize=(mplutil.onecol_width * 0.8, 3.2),
                        sharex=True, sharey=True)

coefs = ["beta__{}".format(i) for i in range(2, 6)]
coef_labels = ["OCNS", "HACNS", "CACNS", "MACNS"]


labels = {"k": "Kang",
          "j": "Johnson",
          "n": "No FC",
          "a": "ANOVA"}

dsets = ["k", "n", "a", "j"]
dcolors = [colors.base, colors.primary, colors.secondary, "#006400"]
offsets = np.linspace(-0.21, 0.21, len(dsets))
xvals = np.arange(0, len(coefs))

for dekind, ax in zip(["reg", "ncx"], axs):
    for didx, dset in enumerate(dsets):
        d = coef_df.loc[dg.groups[(dekind, dset)]]
        d = d.pivot("coef", "meas", "value")
        assert d.index.tolist() == coefs
        pointrange(xvals + offsets[didx], d["mean"].values,
                   d["p025"].values, d["p975"].values,
                   color=dcolors[didx], zorder=2,
                   mec="none", markersize=4, ax=ax)
    ax.set_xticks(xvals)
    ax.set_xticklabels(coef_labels)
    ax.locator_params(axis="y", nbins=6)
    mplutil.remove_spines(ax)
    ax.axhline(0, ls="--", color=colors.light_gray, zorder=1)
axs[0].set_xlim(-0.5, 3.5)
axs[0].set_ylim(-0.5, 0.4)

fig.subplots_adjust(left=0.2, right=0.99, bottom=0.05, top=0.97,
                    wspace=0.2, hspace=0.2)

axs[0].text(0.5, 0.99, "Regional", transform=axs[0].transAxes,
            fontsize="small", ha="center")
axs[1].text(0.5, 0.99, "Neocortical", transform=axs[1].transAxes,
            fontsize="small", ha="center")

mplutil.shared_ylabel(axs, "Coefficient value")

dset_pos = np.linspace(-0.45, -0.2, len(dsets))[::-1]
for didx, (dset, ypos) in enumerate(zip(dsets, dset_pos)):
    axs[0].text(-0.2, ypos, labels[dset], color=dcolors[didx], fontsize="small")

fig.savefig("dset-combined-per6-coefs.pdf")
