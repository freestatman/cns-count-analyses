#!/usr/bin/env python3
"""Like create_regde_period_data.py, but only use HACNS predictors.
"""

import sys

import numpy as np
import pandas as pd

import rdump

try:
    period = int(sys.argv[1])
except (ValueError, IndexError):
    sys.exit("Usage: {} PERIOD".format(sys.argv[0]))
period_idx = period - 1

de = pd.read_table("../limma-de/ncx-de-p1.dat", header=None, sep=" ")

ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv",
                 usecols=["hacns"])
ng = np.log1p(ng)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values])

data = {}
data["y"] = de.values[:, period_idx]
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data,
                 "dp__ncxde_hacns_period{}.data.r".format(period))
