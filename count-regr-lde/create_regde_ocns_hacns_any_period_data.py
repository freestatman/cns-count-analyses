#!/usr/bin/env python3

import sys

import numpy as np
import pandas as pd

import rdump

try:
    period = int(sys.argv[1])
except (ValueError, IndexError):
    sys.exit("Usage: {} PERIOD".format(sys.argv[0]))
period_idx = period - 1

de = pd.read_table("../limma-de/region-de-p1.dat", header=None, sep=" ")

cols = ["ocns", "hacns"]
ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng = ng[cols]
ng = (ng > 0).astype(np.int)

x = np.hstack([np.ones((ng.shape[0], 1)),
               ng.values])

data = {}
data["y"] = de.values[:, period_idx]
data["x"] = x
data["N"] = x.shape[0]
data["K"] = x.shape[1]

rdump.stan_rdump(data,
                 "dp__regde_ocns_hacns_any_period{}.data.r".format(period))
