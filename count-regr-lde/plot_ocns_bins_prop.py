#!/usr/bin/env python3

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

import colors
import mplutil

mplutil.style_use(["latex"])

de = pd.read_csv("../limma-de/region-de-p1.dat", sep=" ", header=None)
de.columns = ["p{}".format(i) for i in range(1, 16)]

ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv",
                 usecols=["ocns"])

transforms = ["ocns", "log_ocns"]

values = {"ocns": ng["ocns"],
          "log_ocns": np.log1p(ng["ocns"])}

nbins = 25
bins = {k: pd.cut(vs, bins=nbins) for k, vs in values.items()}

log_bsizes = {k: np.log1p(bins[k].value_counts(sort=False))
              for k, vs in bins.items()}

probs = {k: de.groupby(bs).sum() / de.groupby(bs).count()
         for k, bs in bins.items()}

fig, axs = plt.subplots(ncols=2, nrows=2,
                        figsize=(mplutil.twocol_width * 0.73, 3.5))

xs = np.arange(1, nbins + 1)
col = "p6"

marker_size = 3.5

descrips = {"ocns": "Number of OCNSs",
            "log_ocns": r"$\ln(1 + \text{Number of OCNSs})$"}

for transform, ax in zip(transforms, axs[1, :]):
    ax.plot(xs, probs[transform][col],
            ls="none", marker="o", ms=marker_size, mec="none",
            color=colors.base)
    mplutil.remove_spines(ax)
    ax.set_xlim(0.5, nbins + 1)

for ax in axs[1, :]:
    ax.set_ylim(-0.025, 1.025)
    ax.set_yticks(np.arange(0, 1.1, 0.2))

for transform, ax in zip(transforms, axs[0, :]):
    ax.plot(xs, log_bsizes[transform],
            ls="none", marker="o", ms=marker_size, mec="none",
            color=colors.base, zorder=2)
    mplutil.remove_spines(ax)
    ax.set_xlim(0.5, nbins + 1)
    ax.set_title(descrips[transform], fontsize="medium")
    ax.set_xticklabels([])

# min_size_y = max([ax.get_ylim()[0] for ax in axs[1, :]])
max_size_y = max([ax.get_ylim()[1] for ax in axs[0, :]])
for ax in axs[0, :]:
    ax.set_ylim(-0.25, max_size_y)

axs[1, 0].set_ylabel("Proportion of DEX genes")
axs[0, 0].set_ylabel(r"$\ln(1 + \text{Number of genes})$",
                     labelpad=8)

fig.subplots_adjust(left=0.09, right=0.99, bottom=0.1, top=0.94,
                    wspace=0.2, hspace=0.25)

mplutil.shared_xlabel(axs[-1, :], "Bin number")

fig.savefig("ocns-prop-vs-bins.pdf")
