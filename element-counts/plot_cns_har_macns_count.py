#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mplutil

mplutil.style_use(["latex"])

fig, axs = plt.subplots(ncols=2, figsize=(mplutil.twocol_width * 0.68, 2))

## MACNSs

ax_macns = axs[0]

ngc = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv",
                  usecols=["ocns", "macns"])
ngc["ocns_log"] = np.log1p(ngc["ocns"])
ngc_nz = ngc[ngc["macns"] != 0]

max_macns = ngc_nz["macns"].max()
macns_vals = ngc_nz["macns"].values.astype(float)

ax_macns.plot(ngc_nz["ocns_log"].values, macns_vals,
              color=colors.base, alpha=0.5,
              marker="o", ms=2.3, mec="none", ls="none")

ax_macns.set_ylim(0.5, max_macns + 0.6)
ax_macns.set_xlim(-0.2)

ax_macns.set(xlabel=r"$\ln(1 + \text{Number of OCNSs})$",
             ylabel="Number of MACNSs")

mplutil.remove_spines(ax_macns)

ax_macns.text(0.06, 0.9, "Genes nearest to MACNSs",
              color=colors.base, fontsize="small",
              transform=ax_macns.transAxes)

## HARs

ngc_har = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts-har.csv",
                      usecols=["har", "ocns_nohp"])
ngc_har["ocns_log"] = np.log1p(ngc_har["ocns_nohp"])
ngc_har_nz = ngc_har[ngc_har["har"] != 0]

ax_har = axs[1]

max_har = ngc_har_nz["har"].max()
har_vals = ngc_har_nz["har"].values.astype(float)

np.random.seed(1647034725)
har_vals += np.random.uniform(-0.25, 0.25, size=ngc_har_nz.shape[0])

for i in range(1, max_har + 2):
    ax_har.axhline(i + 0.5, color="0.8", lw=0.5, zorder=1, alpha=0.6)
    ax_har.tick_params(axis="y", length=0)

ax_har.plot(ngc_har_nz["ocns_log"].values, har_vals,
            color=colors.base, alpha=0.5,
            marker="o", ms=2.3, mec="none", ls="none")

ax_har.set_ylim(0.5, max_har + 0.6)
ax_har.set_xlim(-0.2)

ax_har.set(xlabel=r"$\ln(1 + \text{Number of OCNSs})$",
           ylabel="Number of HARs")

mplutil.remove_spines(ax_har)

ax_har.text(0.06, 0.9, "Genes nearest to HARs",
            color=colors.base, fontsize="small",
            transform=ax_har.transAxes)

fig.subplots_adjust(left=0.1, right=0.99, bottom=0.19, top=0.95,
                    wspace=0.25, hspace=0.2)

mplutil.subplot_label(ax_macns, r"\textbf{A}")
mplutil.subplot_label(ax_har, r"\textbf{B}")

fig.savefig("cns-har-macns-counts.pdf")
