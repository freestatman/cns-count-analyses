#!/usr/bin/env python3

import pandas as pd
pd.set_option("precision", 3)

ngc = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv",
                  usecols=["ocns", "hacns", "cacns", "macns"])

print("Summary")
print(ngc.describe().to_string())

print("\nProportion of genes near at least one element")
print((ngc > 0).mean().to_string())

print("\nProportion near more than one element "
      "of genes near at least one element")
print(ngc.apply(lambda d: (d > 1).sum() / (d > 0).sum(), axis=0).to_string())

print("\nMax neighbored")

print(ngc.max(0).to_string())
