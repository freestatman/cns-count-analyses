#!/usr/bin/env python3

import pandas as pd

ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")

print("Median OCNSs for genes nearest to a HACNS")
print(ng.loc[ng["hacns"] > 0, "ocns"].median())

print("Median OCNSs for genes nearest to a CACNS")
print(ng.loc[ng["cacns"] > 0, "ocns"].median())

print("Median OCNSs for genes nearest to a MACNS")
print(ng.loc[ng["macns"] > 0, "ocns"].median())
