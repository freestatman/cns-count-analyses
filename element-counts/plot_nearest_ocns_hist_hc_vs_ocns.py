#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mplutil

mplutil.style_use(["latex"])

ngc = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ngc["ocns"] = np.log1p(ngc["ocns"])

fig, axs = plt.subplots(ncols=3, nrows=1,
                        figsize=(mplutil.twocol_width, 1.95))

## OCNS Histogram

ax_hist = axs[0]

bins = np.arange(0, ngc["ocns"].max(), 0.4)

_, _, patches = ax_hist.hist(ngc["ocns"].values, bins=bins,
                             fc=colors.light_gray, ec=colors.base)
patches[0].set_facecolor("w")
ax_hist.set(xlabel=r"$\ln(1 + \text{Number of OCNSs})$",
            ylabel="Number of genes")

ax_hist.annotate("Not nearest\nto any OCNS",
                 xy=(0.5, 4000), xytext=(1.2, 4000),
                 fontsize="x-small",
                 va="center",
                 arrowprops=dict(headwidth=3,
                                 width=0.2,
                                 headlength=2.5,
                                 facecolor=colors.base,
                                 edgecolor=colors.base))

mplutil.remove_spines(ax_hist)

ax_hist.set_xlim(-0.2)

ax_hist.text(0.5, 0.95, "All genes",
             ha="center", va="center", fontsize="small",
             transform=ax_hist.transAxes)

ax_hist.locator_params(axis="y", nbins=5)

## HACNS

ngc_nz_hacns = ngc[ngc["hacns"] != 0]

ax_hacns = axs[1]

max_hacns = ngc_nz_hacns["hacns"].max()
hacns_vals = ngc_nz_hacns["hacns"].values.astype(float)

np.random.seed(1647034725)
hacns_vals += np.random.uniform(-0.25, 0.25, size=ngc_nz_hacns.shape[0])

for i in range(1, max_hacns + 2):
    ax_hacns.axhline(i + 0.5, color="0.8", lw=0.5, zorder=1, alpha=0.6)
ax_hacns.tick_params(axis="y", length=0)

ax_hacns.plot(ngc_nz_hacns["ocns"].values, hacns_vals,
              color=colors.base, alpha=0.5,
              marker="o", ms=2.3, mec="none", ls="none")

ax_hacns.set_ylim(0.5, max_hacns + 0.6)
ax_hacns.set_xlim(-0.2)

ax_hacns.set(xlabel=r"$\ln(1 + \text{Number of OCNSs})$",
             ylabel="Number of HACNSs")

mplutil.remove_spines(ax_hacns)

ax_hacns.text(0.06, 0.9, "Genes nearest to HACNSs",
              color=colors.base, fontsize="small",
              transform=ax_hacns.transAxes)

## CACNS

ngc_nz_cacns = ngc[ngc["cacns"] != 0]

ax_cacns = axs[2]

max_cacns = ngc_nz_cacns["cacns"].max()
cacns_vals = ngc_nz_cacns["cacns"].values.astype(float)

cacns_vals += np.random.uniform(-0.25, 0.25, size=ngc_nz_cacns.shape[0])

for i in range(1, max_cacns + 2):
    ax_cacns.axhline(i + 0.5, color="0.8", lw=0.5, zorder=1, alpha=0.6)
ax_cacns.tick_params(axis="y", length=0)

ax_cacns.plot(ngc_nz_cacns["ocns"].values, cacns_vals,
              color=colors.base, alpha=0.5,
              marker="o", ms=2.3, mec="none", ls="none")

assert max_cacns < max_hacns

ax_cacns.set_ylim(0.5, max_hacns + 0.6)
ax_cacns.set_xlim(-0.2)
ax_cacns.set_yticks(np.arange(1, 8))

ax_cacns.set(xlabel=r"$\ln(1 + \text{Number of OCNSs})$",
             ylabel="Number of CACNSs")

mplutil.remove_spines(ax_cacns)

ax_cacns.text(0.06, 0.9, "Genes nearest to CACNSs",
              color=colors.base, fontsize="small",
              transform=ax_cacns.transAxes)

fig.subplots_adjust(left=0.085, right=0.993, bottom=0.19, top=0.92,
                    wspace=0.3, hspace=0.2)

_, ypos = mplutil.subplot_label(ax_hist, r"\textbf{A}")
mplutil.subplot_label(ax_hacns, r"\textbf{B}", ypos=ypos)
mplutil.subplot_label(ax_cacns, r"\textbf{C}", ypos=ypos)

fig.savefig("ocns-hist-hc-vs-ocns.pdf")
