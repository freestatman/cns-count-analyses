#!/usr/bin/env python3
"""Like plot_regde_per6_dist_prob.py, but with locus-length predictor run.
"""

from matplotlib import gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import norm

import mplutil
import stanio
import colors

mplutil.style_use(["latex"])

fname = "samples/dp__regde_per6_dist-all-chains.j4.csv"
samps = stanio.read_samples(fname, filter_svars=True)

lfname = "samples/dp__regde_per6_dist_ragg-all-chains.j4.csv"
lsamps = stanio.read_samples(lfname, filter_svars=True)

ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts-dist.csv",
                 usecols=["ocns_intronic", "ocns_near", "ocns_far"])
ng = np.log1p(ng)

np.random.seed(2111221033)
nreals = 100
reals_idx = np.sort(np.random.choice(samps.index, nreals, replace=False))
reals = samps.loc[reals_idx].values
lreals = lsamps.loc[reals_idx].values

ocns_vals = np.linspace(0, ng.values.max())

preds = {}
preds["noadj"] = np.zeros((len(ocns_vals), reals.shape[1]))
preds["adj"] = np.zeros((len(ocns_vals), lreals.shape[1]))
preds["noadj"][:, 0] = 1.
preds["adj"][:, 0] = 1.

probs = {}
xvals = {}

for dset, svals in zip(["noadj", "adj"], [reals, lreals]):
    for idx, el in enumerate(ng.columns, 1):
        preds_el = preds[dset].copy()
        preds_el[:, idx] = ocns_vals

        other_zero = ng.drop(el, axis=1).sum(1) == 0
        ocns_bm = ocns_vals < ng[other_zero][el].max()

        xvals[(dset, el)] = ocns_vals[ocns_bm]
        linpred_el = np.inner(svals, preds_el[ocns_bm, :])
        probs[(dset, el)] = norm.cdf(linpred_el)

fig = plt.figure(figsize=(mplutil.twocol_width * 0.86, 4.75))
gs = gridspec.GridSpec(4, 3, height_ratios=[3, 4, 0.75, 4])

axs = np.empty((3, 3), dtype="O")
for cidx, el in enumerate(ng.columns):
    for ridx, gs_idx in [(0, 0), (1, 1), (2, 3)]:
        axs[ridx, cidx] = fig.add_subplot(gs[gs_idx, cidx])

alpha = 0.2
xmax = np.ceil(np.max([vs.max() for vs in xvals.values()]))

labels = ["Intronic",
          "Intergenic\n" r"$< 100$ kb",
          "Intergenic\n" r"$\geq 100$ kb"]

for el, label, ax in zip(ng.columns, labels, axs[1]):
    ax.plot(xvals[("noadj", el)], probs[("noadj", el)].T,
            color=colors.base,
            alpha=alpha)
    mplutil.remove_spines(ax)

    if el != "ocns_intronic":
        ax.set_yticklabels([])
    ax.set_ylim(0, 1)
    ax.set_xlim(0, xmax)

    ax.text(0.5, 0.97, label, transform=ax.transAxes,
            fontsize="small", va="top", ha="center")

bins = np.arange(0, xmax, 0.5)
ymax_hist = 15000

for el, label, ax in zip(ng.columns, labels, axs[0]):
    el_max = xvals[("noadj", el)].max()
    ## Truncate the last bin at the maximum value for this predictor.
    ## [x_1, x_2), [x_2, x_3) ... [x_{N-1}, x_max]
    valbins = bins[bins < el_max].tolist() + [el_max]

    ax.hist(ng[el].values, bins=valbins,
            ec=colors.base, fc=colors.lighter_gray)
    mplutil.remove_spines(ax, which=["top", "right", "bottom"])
    ax.locator_params(axis="y", nbins=5)

    ax.set_xlim(0, xmax)
    ax.set_ylim(0, ymax_hist)

    ax.set_xticklabels([])
    if el != "ocns_intronic":
        ax.set_yticklabels([])

    ax.text(0.5, 0.97, label, transform=ax.transAxes,
            fontsize="small", va="top", ha="center")

axs[1, 1].set_xlabel(r"$\ln(1 + \text{Number of OCNSs})$")
axs[1, 0].set_ylabel("Probabiltiy of regional\ndifferential expression")
axs[0, 0].set_ylabel("Number of genes")

axs[0, 0].yaxis.set_label_coords(-0.32, 0.5)
axs[1, 0].yaxis.set_label_coords(-0.22, 0.5)

for el, label, ax in zip(ng.columns, labels, axs[-1]):
    ax.plot(xvals[("adj", el)], probs[("adj", el)].T,
            color=colors.base,
            alpha=alpha)
    mplutil.remove_spines(ax)

    if el != "ocns_intronic":
        ax.set_yticklabels([])
    ax.set_ylim(0, 1)
    ax.set_xlim(0, xmax)

    ax.text(0.5, 0.97, label, transform=ax.transAxes,
            fontsize="small", va="top", ha="center")

axs[-1, 1].set_xlabel(r"$\ln(1 + \text{Number of OCNSs})$")
axs[-1, 0].set_ylabel("Probabiltiy of regional\ndifferential expression")

axs[-1, 0].yaxis.set_label_coords(-0.32, 0.5)
axs[-1, 0].yaxis.set_label_coords(-0.22, 0.5)

axs[-1, 1].set_title("Target size adjustment", fontsize="medium")

fig.subplots_adjust(left=0.12, right=0.99, bottom=0.08, top=0.96,
                    wspace=0.1, hspace=0.25)

mplutil.subplot_label(axs[0, 0], r"\textbf{A}")
mplutil.subplot_label(axs[-1, 0], r"\textbf{B}")

print("h/w: {}".format(axs[1, 0].bbox.height / axs[1, 0].bbox.width))
print("h/w: {}".format(axs[-1, 0].bbox.height / axs[-1, 0].bbox.width))
fig.savefig("regde-per6-dist-probs-ragg.pdf")
