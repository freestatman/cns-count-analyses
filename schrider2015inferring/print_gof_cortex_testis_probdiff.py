#!/usr/bin/env python3

import numpy as np
import pandas as pd
from scipy.stats import norm

import mcmcstats
import stanio

ss = stanio.read_samples("samples/dp__cortex_vs_testis-all-chains.j4.csv",
                         filter_svars=True)

p0 = norm.cdf(ss["beta__1"].values)
p1 = norm.cdf(ss["beta__1"].values + np.log1p(1) * ss["beta__3"].values)

print("\n\nProbability at 1 predictor unit")
dint = mcmcstats.interval_summary(pd.Series(p1 - p0))
print(dint.to_string())
