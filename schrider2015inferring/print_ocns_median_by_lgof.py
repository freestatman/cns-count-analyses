#!/usr/bin/env python3

import pandas as pd

ng = pd.read_csv("../data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv")
print("8-way phastCons")

print("Median OCNSs for genes nearest to a LOF")
print(ng.loc[ng["lof"] > 0, "ocns_nolgof"].median())

print("Median OCNSs for genes nearest to a GOF")
print(ng.loc[ng["gof"] > 0, "ocns_nolgof"].median())

print("\n100-way phastCons")
ngv = pd.read_csv("../data/nearest-gene/"
                  "kang2011spatio-nearest-counts-lgof-vert100.csv")

print("Median OCNSs for genes nearest to a LOF")
print(ngv.loc[ngv["lof"] > 0, "ocns_nolgof"].median())

print("Median OCNSs for genes nearest to a GOF")
print(ngv.loc[ngv["gof"] > 0, "ocns_nolgof"].median())
