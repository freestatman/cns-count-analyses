#!/usr/bin/env python3

import numpy as np
import pandas as pd

de = pd.read_csv("tissue_nomerge-coefs-up.csv")
de["pr1"], de["pr2"] = zip(*de["contrast"].str.split("-"))
de["period"] = de["pr1"].str.split(".").str[0].str.replace("pr", "")
de["period"] = de["period"].astype(np.int)

de["region1"] = de["pr1"].str.split(".").str[1].astype(np.int)
de["region2"] = de["pr2"].str.split(".").str[1].astype(np.int)

de["region1"] -= 1
de["region2"] -= 1

de = de.reindex(columns=["period", "region1", "region2",
                         "intercept", "lof", "gof",
                         "ocns_nolgof", "ragg"])
de = de.sort_values(["period", "region1", "region2"])

names = pd.Series(["Cerebellum",
                   "Cerebral Ctx",
                   "Heart",
                   "Kidney",
                   "Liver",
                   "Lung",
                   "Muscle",
                   "Ovary",
                   "Pancreas",
                   "Spleen",
                   "Testis"])

de["rlabel1"] = de["region1"].map(names)
de["rlabel2"] = de["region2"].map(names)

de.to_csv("tissue_nomerge_vert100-coefs-up-names.csv", index=False)
