#!/usr/bin/env Rscript

library(rhdf5)

de <- as.matrix(read.csv("../gtex/tissue_nomerge-de-full-up.csv"))

ragg <- scan("../data/nearest-gene/gtex-randagg.dat", quiet=TRUE)

cols <- c("lof", "gof", "ocns_nolgof")
ng <- read.csv("../data/nearest-gene/gtex-nearest-counts-lgof.csv")
ng <- ng[cols]
ng <- log1p(ng)
ng$ragg <- ragg

periods <- unlist(lapply(strsplit(colnames(de), ".", fixed=TRUE),
                         function (x) x[1]))
periods <- as.numeric(sub("pr", "", periods, fixed=TRUE))

fits <- lapply(1:dim(de)[2],
               function (n)
                   fit <- glm(de[,n] ~ ng$lof + ng$gof + ng$ocns_nolgof +
                                  ragg,
                              family=binomial(link="probit")))

coefs <- data.frame(t(sapply(fits, coef)))
colnames(coefs) <- c("intercept", "lof", "gof",
                     "ocns_nolgof", "ragg")
coefs$contrast <- gsub("...", "-", colnames(de), fixed=TRUE)

write.csv(coefs, file="tissue_nomerge-coefs-up.csv",
          row.names=FALSE, quote=FALSE)
