#!/usr/bin/env python3

from matplotlib import gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import colors
import mplutil

mplutil.style_use(["latex"])

fig = plt.figure(figsize=(mplutil.twocol_width * 0.84, 6))
gs = gridspec.GridSpec(4, 5, width_ratios=[17, 17, 17, 3.7, 0.75],
                       wspace=0.05, hspace=0.05)

heat_params = ["lof", "gof"]
de = pd.read_csv("tissue_nomerge_nocns-coefs-up-names.csv")
de_cns = pd.read_csv("tissue_nomerge-coefs-up-names.csv")

labels = ["ncLOF coefficient", "ncLOF coefficient, adjusted",
          "ncGOF coefficient", "ncGOF coefficient, adjusted"]
mats = [("lof", de), ("lof", de_cns),
        ("gof", de), ("gof", de_cns)]

haxs = np.empty((4, 3), dtype="O")
for r in range(4):
    for c in range(3):
        haxs[r, c] = fig.add_subplot(gs[r, c])

ims = []

for (param, mat), row in zip(mats, range(4)):
    tissues = mat["rlabel1"].unique()

    dem = mat.set_index(["period", "region1", "region2"])
    dem = dem[param].unstack("region1")

    max_val = max(abs(np.nanmin(dem.values)), np.nanmax(dem.values))
    vmin, vmax = -max_val, max_val

    for period, ax in zip(dem.index.levels[0], haxs[row]):
        pmat = dem.loc[period]
        vals = pmat.values
        im = ax.imshow(vals,
                       cmap=colors.cmap_div,
                       vmin=vmin, vmax=vmax,
                       interpolation="none", aspect="auto")
        if period == 1:
            ims.append(im)

        ax.tick_params(length=0)

        ax.set_yticks(np.arange(pmat.shape[0]))
        ax.set_xticks(np.arange(pmat.shape[1]))
        if period == 1:
            ax.set_yticklabels(tissues[pmat.index.values],
                               fontsize="small")

for ax, period in zip(haxs[0, :3], range(13, 16)):
    ax.text(0.5, 1., "Period {}".format(period),
            ha="center", va="bottom", fontsize="medium",
            transform=ax.transAxes)
for ax in haxs[-1, :3]:
    ax.set_xticklabels(tissues[pmat.columns.values],
                       fontsize="small", rotation=90)
for ax in haxs[:-1, :3].ravel():
    ax.set_xticklabels([])
for ax in haxs[:, 1:3].ravel():
    ax.set_yticklabels([])

for ax in haxs.ravel():
    mplutil.style_heatmap(ax)

for row, label in zip(range(4), labels):
    cax = fig.add_subplot(gs[row, -1])
    mplutil.heat_colorbar(cax, ims[row], label)

fig.subplots_adjust(left=0.113, right=0.96, bottom=0.107, top=0.975,
                    wspace=0.06, hspace=0.32)

fig.savefig("lgof-tissue_nomerge-heatmaps-coefs-up.pdf")
