shell.executable('bash')

configfile: "sm/config.json"

rule help:
    run:
        print("\nFigure targets:")
        print("\n".join(["    " + f for f in figures]))
        print("\nStat targets:")
        print("\n".join(["    " + s for s in stats]))

include: "sm/stan.smrules"


### Manuscript

figures = [
    "count-regr-lde/dset-combined-per6-coefs.pdf",
    "count-regr-lde/ocns-ar-ocns-coefs.pdf",
    "count-regr-lde/ocns-prop-vs-bins.pdf",
    "count-regr-lde/reg-ncxde-per6-prob-coefs.pdf",
    "element-counts/cns-har-macns-counts.pdf",
    "element-counts/nearest-distr.pdf",
    "element-counts/ocns-hist-hc-vs-ocns.pdf",
    "gam/gam-mexp-regde-log1p.pdf",
    "gam/gam-regde-nolog-acns.pdf",
    "gtex/testis-coefs.pdf",
    "gtex/tissue_nomerge-heat-box.pdf",
    "gtex/tissue_nomerge-heatmaps-coefs-up-acns.pdf",
    "gtex/tissue_nomerge-heatmaps-coefs-up.pdf",
    "gtex/tissue_nomerge-heatmaps-de-up.pdf",
    "lmlr/dp-lm-coef-comparison.pdf",
    "lmlr/lgof-coef-butone.pdf",
    "lmlr/lm-score-ragg-coefs.pdf",
    "lmlr/ocns-coef-butone.pdf",
    "lmlr/regde-per6-resid.pdf",
    "ocns-loc/regde-per6-dist-probs-ragg.pdf",
    "roadmap/roadmap-cns-acns-medians.pdf",
    "roadmap/roadmap-lgof-medians.pdf",
    "schrider2015inferring/lgof-coef-prob.pdf",
    "schrider2015inferring/lgof-tissue_nomerge-heatmaps-coefs-up.pdf",
    "schrider2015inferring/lgof-tissue_nomerge_vert100-heatmaps-coefs-up.pdf",
]

figures_tiffs = [f.replace(".pdf", ".tiff") for f in figures]

rule manu_figures:
    input: figures

rule manu_figures_tiffs:
    input: figures_tiffs

rule manu_figures_clean:
    params: files=figures
    shell: "rm -vf {params.files}"

stats = [
    "acns-proportion/johnson2009functional-ncx-acns-cns-ratio-stats.txt",
    "acns-proportion/johnson2009functional-ncx-pval-stats.txt",
    "acns-proportion/johnson2009functional-region-acns-cns-ratio-stats.txt",
    "acns-proportion/johnson2009functional-region-pval-stats.txt",
    "acns-proportion/kang2011spatio-ncx-pval-stats.txt",
    "acns-proportion/kang2011spatio-region-pval-stats.txt",
    "acns-proportion/lambert2011genes-acns-cns-ratio-stats.txt",
    "acns-proportion/lambert2011genes-pval-stats.txt",
    "acns-proportion/ncx-per6-acns-cns-ratio-stats.txt",
    "acns-proportion/region-per6-acns-cns-ratio-stats.txt",
    "count-regr-lde/dp__ncxde_hacns_period6-intervals.j4.txt",
    "count-regr-lde/dp__ncxde_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_acns_any_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_acns_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_any_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_hacns_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_medexp_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_nolog_acns_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_nolog_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_nostd_ragg_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_ocns_any_acns_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_ocns_nolog_acns_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_period6-intervals.j4.txt",
    "count-regr-lde/dp__regde_ragg-all-periods-sorted-intervals-stats.txt",
    "count-regr-lde/ncxde_period6-diff-stats.txt",
    "count-regr-lde/per6-regde-proportion-stats.txt",
    "count-regr-lde/regde-nostd-mean-ragg-diff-stats.txt",
    "count-regr-lde/regde_period6-diff-stats.txt",
    "data/elements/element-counts-stats.txt",
    "data/expression/kang2011spatio-p1-sampleinfo-stats.txt",
    "data/nearest-gene/cor-scorebw.dat",
    "element-counts/el-counts-cor-har-stats.txt",
    "element-counts/el-counts-cor-stats.txt",
    "element-counts/median-ocns-by-el-stats.txt",
    "element-counts/nearest-gene-counts-stats.txt",
    "go/gof-cat-stats.txt",
    "gtex/tissue_nomerge-coef-up-stats.txt",
    "har/dp__ncxde_har_period6-intervals.j4.txt",
    "har/dp__ncxde_ocns_har_period6-intervals.j4.txt",
    "har/dp__regde_har_period6-intervals.j4.txt",
    "har/dp__regde_ocns_har_period6-intervals.j4.txt",
    "limma-de/region-de-p1-de-period-count-stats.txt",
    "locus-length/dp__regde_loclen_period6-intervals.j4.txt",
    "ocns-loc/dp__regde_per6_dist-intervals.j4.txt",
    "ocns-loc/dp__regde_per6_dist_ragg-intervals.j4.txt",
    "ocns-loc/regde-per6-diff-stats.txt",
    "roadmap/mw-cns-stats.txt",
    "roadmap/mw-lgof-stats.txt",
    "schrider2015inferring/cortex-vs-testis-de-percent-stats.txt",
    "schrider2015inferring/gof-cortex-testis-probdiff-stats.txt",
    "schrider2015inferring/median-ocns-by-lgof-stats.txt",
    "schrider2015inferring/vert100-ocns-cor-stats.txt",
]

rule manu_stats:
    input: stats

rule manu_stats_clean:
    params: files=stats
    shell: "rm -vf {params.files}"

def tiff(name):
    def _(wildcards):
        fullname = name
        if wildcards.ext == "tiff":
            fullname = name.replace(".py", "__tiff.py")
        return fullname.format(**wildcards)
    return _

rule savefig_pdf_to_tiff_:
    input: "{name}/plot_{rest}.py"
    output: temp("{name}/plot_{rest}__tiff.py")
    run:
        with open(input[0]) as ifh:
            with open(output[0], "w") as ofh:
                ofh.write(re.sub(r"(\.savefig\(.*\.)pdf(['\"])",
                                 "\g<1>tiff\g<2>, dpi=300",
                                 ifh.read()))
        shell("chmod +x {output}")


### Data

#### Expression

rule expression_download_gl15175_gene_info_table:
    output: "data/expression/GPL5175-3188.txt"
    shell: "cd $(dirname {output}) && "
           "wget 'https://www.ncbi.nlm.nih.gov/geo/query/"
           "acc.cgi?mode=raw&is_datatable=true&acc=GPL5175"
           "&id=3188&db=GeoDb_blob89' -O $(basename {output})"

rule expression_extract_gene_info_gpl5175:
    input: "data/expression/extract_gpl5175_info.py",
           "data/expression/GPL5175-3188.txt"
    output: "data/expression/gpl5175-geneinfo.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_subset_gpl5175_geneinfo_to_gencode_pc:
    input: "data/expression/subset_gpl5175_to_gencode_pc.py",
           "data/gencode/gencode-v19-genename-ensg.json",
           "data/expression/gpl5175-geneinfo.csv"
    output: "data/expression/gpl5175-geneinfo-gencode.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

##### kang2011spatio

rule expression_download_kang2011spatio_gene_level_series_matrix:
    output: "data/expression/GSE25219-GPL5175_series_matrix.txt.gz"
    shell: "cd $(dirname {output}) && "
           "wget ftp://ftp.ncbi.nlm.nih.gov/geo/series/"
           "GSE25nnn/GSE25219/matrix/$(basename {output})"

rule expression_download_kang2011spatio_dabg:
    output: "data/expression/GSE25219_DABG_pvalue.csv.gz"
    shell: "cd $(dirname {output}) && "
           "wget ftp://ftp.ncbi.nlm.nih.gov/geo/series/"
           "GSE25nnn/GSE25219/suppl/$(basename {output})"

rule expression_extract_kang2011spatio_matrix:
    input: "data/expression/extract_geo_matrix.py",
           "data/expression/GSE25219-GPL5175_series_matrix.txt.gz"
    output: "data/expression/GSE25219-GPL5175-expression.txt",
            "data/expression/GSE25219-GPL5175-info.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) --fields=Sample_characteristics_ch1 "
           "$(basename {input[1]})"

rule expression_clean_kang2011spatio_sample_info:
    input: "data/expression/clean_kang2011spatio.py",
           "data/expression/GSE25219-GPL5175-info.txt",
           "data/expression/GSE25219-GPL5175-expression.txt",
    output: "data/expression/kang2011spatio-sampleinfo.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_subset_kang2011spatio_:
    input:
        "data/expression/subset_kang2011spatio_p{per}.py",
        "data/expression/GSE25219-GPL5175-expression.txt",
        "data/expression/kang2011spatio-sampleinfo.csv",
        "data/expression/gpl5175-geneinfo-gencode.csv",
        "data/expression/GSE25219_DABG_pvalue.csv.gz",
    output:
        "data/expression/kang2011spatio-p{per,[0-9]}-ge-subset.csv",
        "data/expression/kang2011spatio-p{per,[0-9]}-dabg-subset.csv",
        "data/expression/kang2011spatio-p{per,[0-9]}-pass_dabg-subset.csv",
        "data/expression/kang2011spatio-p{per,[0-9]}-sampleinfo-subset.csv",
        "data/expression/kang2011spatio-p{per,[0-9]}-geneinfo-subset.csv",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_kang2011spatio_export_to_h5_:
    input:
        "data/expression/export_kang2011spatio_p{per}_to_h5.py",
        "data/expression/kang2011spatio-p{per}-ge-subset.csv",
        "data/expression/kang2011spatio-p{per}-dabg-subset.csv",
        "data/expression/kang2011spatio-p{per}-pass_dabg-subset.csv",
        "data/expression/kang2011spatio-p{per}-sampleinfo-subset.csv",
        "data/expression/kang2011spatio-p{per}-geneinfo-subset.csv",
    output:
        "data/expression/kang2011spatio-p{per,[0-9]}.h5"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_kang2011spatio_export_to_hdf5_:
    input:
        "data/expression/export_kang2011spatio_p{per}_to_hdf5.py",
        "data/expression/kang2011spatio-p{per}.h5",
    output:
        "data/expression/kang2011spatio-p{per,[0-9]}.hdf5"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_kang2011spatio_print_sampleinfo_stats_:
    input:
        "data/expression/print_kang2011spatio_p{per}_sampleinfo.py",
        "data/expression/kang2011spatio-p{per}.h5"
    output: "data/expression/kang2011spatio-p{per,[0-9]}-sampleinfo-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule expression_kang2011spatio_std_median_expression_:
    input:
        "data/expression/"
        "calculate_kang2011spatio_p{per}_{name}_std_median.py",
        "data/expression/kang2011spatio-p{per}.hdf5"
    output:
        "data/expression/kang2011spatio-p{per,[0-9]}-{name}-std-median-per-stage.dat"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_kang2011spatio_genenames:
    input: "data/expression/export_genenames.py",
           "data/expression/kang2011spatio-p1.h5"
    output: "data/expression/kang2011spatio-gene-names.txt"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

##### johnson2009functional

rule expression_download_johnson2009functional_gene_level_series_matrix:
    output: "data/expression/GSE13344-GPL5175_series_matrix.txt.gz"
    shell: "cd $(dirname {output}) && "
           "wget ftp://ftp.ncbi.nlm.nih.gov/geo/series/GSE13nnn/"
           "GSE13344/matrix/$(basename {output})"

rule expression_extract_johnson2009functional_matrix:
    input: "data/expression/extract_geo_matrix.py",
           "data/expression/GSE13344-GPL5175_series_matrix.txt.gz"
    output: "data/expression/GSE13344-GPL5175-expression.txt",
            "data/expression/GSE13344-GPL5175-info.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) "
           "--fields=Sample_characteristics_ch1,Sample_title "
           "$(basename {input[1]})"

rule expression_clean_johnson2009functional_sample_info:
    input: "data/expression/clean_johnson2009functional.py",
           "data/expression/GSE13344-GPL5175-expression.txt",
           "data/expression/GSE13344-GPL5175-info.txt"
    output: "data/expression/johnson2009functional-sampleinfo.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_subset_johnson2009functional:
    input: "data/expression/subset_johnson2009functional.py",
           "data/expression/GSE13344-GPL5175-expression.txt",
           "data/expression/johnson2009functional-sampleinfo.csv",
           "data/expression/gpl5175-geneinfo-gencode.csv",
    output: "data/expression/johnson2009functional-ge-subset.csv",
            "data/expression/johnson2009functional-sampleinfo-subset.csv",
            "data/expression/johnson2009functional-geneinfo-subset.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_johnson2009functional_export_to_h5:
    input: "data/expression/export_johnson2009functional_to_h5.py",
           "data/expression/johnson2009functional-ge-subset.csv",
           "data/expression/johnson2009functional-sampleinfo-subset.csv",
           "data/expression/johnson2009functional-geneinfo-subset.csv"
    output: "data/expression/johnson2009functional.h5"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_johnson2009functional_export_to_hdf5:
    input: "data/expression/export_johnson2009functional_to_hdf5.py",
           "data/expression/johnson2009functional.h5",
    output: "data/expression/johnson2009functional.hdf5"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_johnson2009functional_median_expression_:
    input: "data/expression/"
           "calculate_johnson2009functional_{kind}_std_median.py",
           "data/expression/johnson2009functional.hdf5"
    output: "data/expression/johnson2009functional-{kind}-std-median.dat"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

##### lambert2011genes

rule expression_download_gpl570_gene_info_table:
    output: "data/expression/GPL570-13270.txt"
    shell: 'cd $(dirname {output}) && '
           "wget 'http://www.ncbi.nlm.nih.gov/geo/query/"
           "acc.cgi?mode=raw&is_datatable=true&acc=GPL570"
           "&id=13270&db=GeoDb_blob82' -O $(basename {output})"

rule expression_extract_gene_info_gpl570:
    input: "data/expression/extract_gpl570_info.py",
           "data/expression/GPL570-13270.txt"
    output: "data/expression/gpl570-geneinfo.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_subset_gpl570_geneinfo_to_gencode_pc:
    input: "data/expression/subset_gpl570_to_gencode_pc.py",
           "data/gencode/gencode-v19-entrez-ensg.json",
           "data/expression/gpl570-geneinfo.csv"
    output: "data/expression/gpl570-geneinfo-gencode.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_download_lambert2011genes_gene_level_series_matrix:
    output: "data/expression/GSE21858_series_matrix.txt.gz"
    shell: "cd $(dirname {output}) && "
           "wget ftp://ftp.ncbi.nlm.nih.gov/geo/series/GSE21nnn/"
           "GSE21858/matrix/$(basename {output})"

rule expression_extract_lambert2011genes_matrix:
    input: "data/expression/extract_geo_matrix.py",
           "data/expression/GSE21858_series_matrix.txt.gz"
    output: "data/expression/GSE21858-expression.txt",
            "data/expression/GSE21858-info.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) "
           "--fields=Sample_characteristics_ch1 "
           "$(basename {input[1]})"

rule expression_clean_lambert2011genes_sample_info:
    input: "data/expression/clean_lambert2011genes.py",
           "data/expression/GSE21858-expression.txt",
           "data/expression/GSE21858-info.txt"
    output: "data/expression/lambert2011genes-sampleinfo.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_subset_lambert2011genes:
    input: "data/expression/subset_lambert2011genes.py",
           "data/expression/GSE21858-expression.txt",
           "data/expression/lambert2011genes-sampleinfo.csv",
           "data/expression/gpl570-geneinfo-gencode.csv",
    output: "data/expression/lambert2011genes.h5"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_lambert2011genes_export_to_hdf5:
    input: "data/expression/export_lambert2011genes_to_hdf5.py",
           "data/expression/lambert2011genes.h5",
    output: "data/expression/lambert2011genes.hdf5"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_lambert2011genes_median_expression:
    input: "data/expression/calculate_lambert2011genes_std_median.py",
           "data/expression/lambert2011genes.hdf5"
    output: "data/expression/lambert2011genes-std-median.dat"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule expression_lambert2011genes_median_expression_:
    input: "data/expression/"
           "calculate_lambert2011genes_region_std_median.py",
           "data/expression/lambert2011genes.hdf5"
    output: "data/expression/lambert2011genes-region-std-median.dat"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

##### GTEx

rule expression_gtex_subset:
    input: "data/expression/gtex/subset_gtex.py",
           "data/expression/gtex/GTEx_Data_V6_Annotations_SampleAttributesDS.txt",
           "data/expression/gtex/GTEx_Data_V6_Annotations_SubjectPhenotypesDS.txt",
           "data/expression/gtex/"
           "GTEx_Analysis_v6_RNA-seq_RNA-SeQCv1.1.8_gene_reads.gct.gz"
    output: "data/expression/gtex/gtex-counts-subset.csv",
            "data/expression/gtex/gtex-sattr-subset.csv",
            "data/expression/gtex/gtex-tissues-subset.csv",
            "data/expression/gtex/gtex-genes-subset.csv"
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule expression_gtex_export_to_h5:
    input: "data/expression/gtex/export_gtex_to_h5.py",
           "data/expression/gtex/gtex-counts-subset.csv",
           "data/expression/gtex/gtex-sattr-subset.csv",
           "data/expression/gtex/gtex-tissues-subset.csv",
           "data/expression/gtex/gtex-genes-subset.csv"
    output: "data/expression/gtex/gtex.h5"
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule expression_gtex_h5_link:
    input: "data/expression/gtex/gtex.h5"
    output: "data/expression/gtex.h5"
    shell: "ln -rsf {input} {output}"

rule expression_gtex_export_to_hdf5_:
    input: "data/expression/gtex/export_gtex_{name}_to_hdf5.py",
           "data/expression/gtex/gtex.h5"
    output: "data/expression/gtex/gtex-{name}.hdf5",
            "data/expression/gtex/gtex-{name}-tissues.txt"
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule expression_gtex_genenames:
    input: "data/expression/export_gtex_genenames.py",
           "data/expression/gtex.h5"
    output: "data/expression/gtex-gene-names.txt"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

#### Gencode

rule gencode_gtf:
    output: "data/gencode/gencode.v19.annotation.gtf.gz"
    shell: "cd $(dirname {output}) && "
           "wget ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/"
           "release_19/$(basename {output})"

rule gencode_gene_pc_gtf:
    input: "data/gencode/gencode.v19.annotation.gtf.gz"
    output: "data/gencode/gencode.v19.annotation-gene-pc.gtf"
    shell: "zcat {input} | awk '/\tgene\t/' | "
           "awk '/; gene_type \"protein_coding\"; /' | "
           "awk '!/; gene_status \"PUTATIVE\"; /' | "
           "awk '/; transcript_type \"protein_coding\"; /' "
           "> {output}"

rule gencode_transcript_pc_gtf:
    input: "data/gencode/gencode.v19.annotation.gtf.gz"
    output: "data/gencode/gencode.v19.annotation-transcript-pc.gtf"
    shell: "zcat {input} | awk '/\ttranscript\t/' | "
           "awk '/; gene_type \"protein_coding\"; /' |"
           "awk '!/; gene_status \"PUTATIVE\"; /' | "
           "awk '/; transcript_type \"protein_coding\"; /' "
           "> {output}"

rule gencode_transcript_pc_longest_bed:
    input: "data/gencode/get_bounds.py",
           "data/gencode/gencode.v19.annotation-transcript-pc.gtf"
    output: "data/gencode/gencode.v19.annotation-transcript-pc-longest.bed"
    shell: "./{input[0]} {input[1]} | bedtools sort > {output}"

rule gencode_genename_ensg_map:
    input: "data/gencode/map_genename_to_ensg.py",
           "data/gencode/gencode.v19.annotation-transcript-pc.gtf"
    output: "data/gencode/gencode-v19-genename-ensg.json"
    shell: "./{input[0]} {input[1]} {output}"

rule gencode_enst_ensg_map:
    input: "data/gencode/map_enst_to_ensg.py",
           "data/gencode/gencode.v19.annotation-transcript-pc.gtf"
    output: "data/gencode/gencode-v19-enst-ensg.json"
    shell: "./{input[0]} {input[1]} {output}"

rule gencode_entrez_ensg_map:
    input: 'data/gencode/map_entrez_to_ensg.py',
           'data/gencode/gencode.v22.metadata.EntrezGene.gz',
           'data/gencode/gencode-v19-enst-ensg.json'
    output: 'data/gencode/gencode-v19-entrez-ensg.json'
    shell: 'cd $(dirname {input[0]}) && ./$(basename {input[0]})'

rule gencode_ensts:
    input: 'data/gencode/get_ensts.py',
           gtf='data/gencode/gencode.v19.annotation-transcript-pc.gtf'
    output: 'data/gencode/ensts.txt'
    shell: './{input[0]} < {input.gtf} > {output}'

rule gencode_ensgs:
    input: "data/gencode/get_ensgs.py",
           gtf="data/gencode/gencode.v19.annotation-gene-pc.gtf"
    output: "data/gencode/ensgs.txt"
    shell: "{input[0]} {input.gtf} | sort > {output}"

rule gencode_entrez:
    output: 'gencode.v22.metadata.EntrezGene.gz'
    shell: 'wget ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/'
           'release_22/{output}'

rule gencode_transcript_pc_longest_spacing:
    input: "data/gencode/gencode.v19.annotation-transcript-pc-longest.bed"
    output: "data/gencode/gencode.v19.annotation-transcript-pc-longest.bed.spacing"
    shell: "bedtools spacing -i {input} > {output}"

rule gencode_transcript_pc_locus_coords:
    input: "data/gencode/locus_coords.py",
           "data/gencode/gencode.v19.annotation-transcript-pc-longest.bed.spacing",
           "data/gencode/hg19.chrom.sizes"
    output: "data/gencode/gencode.v19.annotation-transcript-pc-locus-coords.bed"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule gencode_transcript_pc_locus_lengths:
    input: "data/gencode/locus_lengths.py",
           "data/gencode/gencode.v19.annotation-transcript-pc-locus-coords.bed"
    output: "data/gencode/gencode-v19-locus-lengths.json"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gencode_nonoverlapping_genes:
    input: "data/gencode/nonoverlapping_genes.py",
           "data/gencode/gencode.v19.annotation-transcript-pc-longest.bed.spacing",
    output: "data/gencode/nonoverlapping-genes.txt"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gencode_hg19_chrm_lens:
    output: "data/gencode/hg19.chrom.sizes"
    shell: "cd $(dirname {output}) && "
           "wget https://genome.ucsc.edu/goldenpath/help/$(basename {output})"

#### LiftOver

rule liftover_lift_chain_hg19_:
    output: "data/liftover/{build0}ToHg19.over.chain.gz"
    shell: "cd $(dirname {output}) && "
           "wget http://hgdownload.cse.ucsc.edu/goldenPath/"
           "{wildcards.build0}/liftOver/$(basename {output})"

rule hg17_hg19_bed_:
    input: "{name}-hg17.bed", "data/liftover/hg17ToHg19.over.chain.gz"
    output: "{name}-hg19.bed"
    shell: "liftOver {input[0]} {input[1]} {output} {output}.unmapped"

rule hg18_hg19_bed_:
    input: "{name}-hg18.bed", "data/liftover/hg18ToHg19.over.chain.gz"
    output: "{name}-hg19.bed"
    shell: "liftOver {input[0]} {input[1]} {output} {output}.unmapped"

#### Elements

rule elements_prabhakar2006accelerated_suppzip:
    output: "data/elements/Prabhakar.TableS1.zip"
    shell: "cd $(dirname {output}) && "
           "wget http://www.sciencemag.org/content/suppl/"
           "2006/11/03/314.5800.786.DC1/$(basename {output})"

rule elements_prabhakar2006accelerated_supp:
    input: "data/elements/Prabhakar.TableS1.zip"
    output: "data/elements/prabhakar2006accelerated-supplement.xls"
    shell: "unzip {input} && "
           "mv 1130738TableS1.xls {output} && "
           "touch {output}"

rule elements_extract_prabhakar2006accelerated_info:
    input: "data/elements/extract_acns_info.py",
           "data/elements/prabhakar2006accelerated-supplement.xls"
    output: "data/elements/hacns-info.csv",
            "data/elements/cacns-info.csv",
            "data/elements/macns-info.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule elements_prabhakar2006accelerated_hg17_bed:
    input: "data/elements/acns_info_to_bed.py",
           "data/elements/hacns-info.csv",
           "data/elements/cacns-info.csv",
           "data/elements/macns-info.csv"
    output: "data/elements/hacns-coordinates-hg17.bed",
            "data/elements/cacns-coordinates-hg17.bed",
            "data/elements/macns-coordinates-hg17.bed"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule elements_phastcons_8way:
    output: "data/elements/phastConsElements8way.txt.gz"
    shell: "cd $(dirname {output}) && "
           "wget http://hgdownload-test.cse.ucsc.edu/goldenPath/"
           "hg17/phastCons/mzPt1Mm5Rn3Cf1Gg2Fr1Dr1/$(basename {output})"

rule elements_phastcons_8way_label_hg_:
    input: "data/elements/pc8way-gt{thresh}.bed"
    output: "data/elements/pc8way-gt{thresh,[0-9]+}-hg17.bed"
    shell: "ln -rsf {input} {output}"

rule elements_phastcons_8way_label_bw_hg_:
    input: "data/elements/pc8way-bw{t1}-{t2}.bed"
    output: "data/elements/pc8way-bw{t1,[0-9]+}-{t2,[0-9]+}-hg17.bed"
    shell: "ln -rsf {input} {output}"

rule elements_phastcons_100way_label_hg_:
    input: "data/elements/pc100way{subset}-gt{thresh}.bed"
    output: "data/elements/pc100way{subset,[A-z]*}-gt{thresh,[0-9]+}-hg19.bed"
    shell: "ln -rsf {input} {output}"

rule phastcons_gt_thresh_:
    input: "data/elements/phastConsElements{xway}.txt.gz"
    output: "data/elements/pc{xway}-gt{thresh,[0-9]+}.bed"
    shell: "zcat {input} | "
           "awk 'BEGIN{{OFS=\"\t\";}} {{score=int($6);}} "
           "score >= {wildcards.thresh} {{print $2, $3, $4;}}' > {output}"

rule phastcons_gt_between_thresh_:
    input: "data/elements/phastConsElements{xway}.txt.gz"
    output: "data/elements/pc{xway}-bw{t1,[0-9]+}-{t2,[0-9]+}.bed"
    shell: "zcat {input} | "
           "awk 'BEGIN{{OFS=\"\t\";}} {{score=int($6);}} "
           "score >= {wildcards.t1} && score < {wildcards.t2} "
           "{{print $2, $3, $4;}}' > {output}"

rule elements_phastcons_merge_:
    input: "data/elements/pc{xway}-gt{thresh}-hg19.bed"
    output: "data/elements/pc{xway}-gt{thresh,[0-9]+}-merged-hg19.bed"
    shell: 'grep -v "random\|hap\|Un\|chrM" {input} | '
           'sort -k1,1V -k2g > {output}.tmp && '
           'bedtools merge -i {output}.tmp > {output} && '
           'rm {output}.tmp'

rule elements_phastcons_bw_merge_:
    input: "data/elements/pc{xway}-bw{t1}-{t2}-hg19.bed"
    output: "data/elements/pc{xway}-bw{t1,[0-9]+}-{t2,[0-9]+}-merged-hg19.bed"
    shell: 'grep -v "random\|hap\|Un\|chrM" {input} | '
           'sort -k1,1V -k2g > {output}.tmp && '
           'bedtools merge -i {output}.tmp > {output} && '
           'rm {output}.tmp'

rule elements_intersect_cns_:
    input: "data/elements/{s}acns-coordinates-hg19.bed",
           "data/elements/cns-pc8way-gt400-merged-hg19.bed",
    output: "data/elements/{s}acns-hg19.bed"
    shell: "bedtools intersect -a {input[0]} -b {input[1]} | "
           "bedtools sort > {output}"

rule elements_filter_out_exon_:
    input: "data/elements/{s}-coordinates-hg19.bed",
           "data/elements/cns-data-hg19/exon-coordinates-hg19.bed"
    output: "data/elements/{s}-exonfiltered-coordinates-hg19.bed"
    shell: "bedtools intersect -v -a {input[0]} -b {input[1]} > {output}"

rule elements_ucsc_hg19_download_:
    output: "data/elements/{path}.txt.gz"
    shell: "cd $(dirname {output}) && "
           "wget http://hgdownload.cse.ucsc.edu/goldenPath/"
           "hg19/database/$(basename {output})"

rule elements_knowngene_hg19_bed:
    input: "data/elements/cns-data-hg19/knownGene.txt.gz",
    output: "data/elements/cns-data-hg19/exon-coordinates-hg19.bed"
    shell: "zcat {input} | "
           "awk 'BEGIN{{OFS=\"\t\";}} "
           "{{n=int($8); split($9,s,/,/);split($10,e,/,/); "
           "for(i=1;i<=n;++i) {{print $2, s[i], e[i];}} }}' | "
           "grep -v \"random\|hap\|Un\|chrM\" | "
           "sort -k1,1V -k2g > {output}.tmp && "
           "bedtools merge -i {output}.tmp > {output} && "
           "rm {output}.tmp"

rule elements_spliced_est_hg19_bed:
    input: "data/elements/cns-data-hg19/intronEst.txt.gz",
    output: "data/elements/cns-data-hg19/spliced-est-coordinates-hg19.bed"
    shell: "zcat {input} | "
           "awk 'BEGIN{{OFS=\"\t\";}} "
           "{{n=int($19); split($20,b,/,/); split($22,s,/,/); "
           "for(i=1;i<=n;++i) {{print $15, s[i], s[i] + b[i];}} }}' | "
           "grep -v \"random\|hap\|Un\|chrM\" | "
           "sort -k1,1V -k2g  > {output}.tmp && "
           "bedtools merge -i {output}.tmp > {output} && "
           "rm {output}.tmp"

rule elements_retroposed_hg19_bed:
    input: "data/elements/cns-data-hg19/ucscRetroAli5.txt.gz",
    output: "data/elements/cns-data-hg19/retroposed-coordinates-hg19.bed"
    shell: "zcat {input} | cut -f15,17,18 | "
           "grep -v \"random\|hap\|Un\|chrM\" | "
           "sort -k1,1V -k2g > {output}.tmp && "
           "bedtools merge -i {output}.tmp > {output} && "
           "rm {output}.tmp"

rule elements_chainself_link_hg19_bed:
    input: "data/elements/cns-data-hg19/chainSelfLink.txt.gz",
    output: "data/elements/cns-data-hg19/chainself-link-coordinates-hg19.bed"
    shell: "zcat {input} | cut -f2-4 | "
           "grep -v \"random\|hap\|Un\|chrM\" | "
           "sort -k1,1V -k2g > {output}.tmp && "
           "bedtools merge -i {output}.tmp > {output} && "
           "rm {output}.tmp"

rule elements_combined_cns_filter_dataset_bed:
    input: "data/elements/cns-data-hg19/exon-coordinates-hg19.bed",
           "data/elements/cns-data-hg19/spliced-est-coordinates-hg19.bed",
           "data/elements/cns-data-hg19/retroposed-coordinates-hg19.bed",
           "data/elements/cns-data-hg19/chainself-link-coordinates-hg19.bed"
    output: "data/elements/cns-data-hg19/filter-features-coordinates-hg19.bed"
    shell: "cat {input} | "
           "sort -k1,1V -k2g > {output}.tmp && "
           "bedtools merge -i {output}.tmp > {output} && "
           "rm {output}.tmp"

rule elements_cns_filtered_:
    input: "data/elements/{name}-hg19.bed",
           "data/elements/cns-data-hg19/filter-features-coordinates-hg19.bed"
    output: "data/elements/cns-{name}-hg19.bed"
    shell: "bedtools intersect -v -a {input[0]} -b {input[1]} | "
           "bedtools sort | "
           "awk 'BEGIN {{OFS=\"\\t\"}} {{print $0,\"cns.\"NR}}' > {output}"

rule elements_combine_acns:
    input: "data/elements/hacns-hg19.bed",
           "data/elements/cacns-hg19.bed",
           "data/elements/macns-hg19.bed"
    output: "data/elements/acns-hg19.bed"
    shell: "cat {input} > {output}"

rule elements_cns_remove_acns_:
    input: "data/elements/cns-{name}-hg19.bed",
           "data/elements/acns-hg19.bed",
    output: "data/elements/ocns-{name}-hg19.bed",
    shell: "bedtools intersect -v -a {input[0]} -b {input[1]} > {output}"

rule elements_print_element_count_stats:
    input: "data/elements/print_element_count_stats.py",
           "data/elements/cns-pc8way-gt400-merged-hg19.bed",
           "data/elements/hacns-coordinates-hg19.bed",
           "data/elements/cacns-coordinates-hg19.bed",
           "data/elements/macns-coordinates-hg19.bed"
    output: "data/elements/element-counts-stats.txt"
    shell: "cd $(dirname {input[0]}) "
           "&& ./$(basename {input[0]}) > $(basename {output})"

rule element_counts_print_ocns_cor:
    input: "element-counts/print-counts-cor.r",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "element-counts/el-counts-cor-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule element_counts_print_ocns_cor_har:
    input: "element-counts/print-counts-cor-har.r",
           "data/nearest-gene/kang2011spatio-nearest-counts-har.csv"
    output: "element-counts/el-counts-cor-har-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

##### Random noncoding elements

rule elements_random_:
    input: "data/gencode/hg19.chrom.sizes"
    output: "data/elements/random-elems{idx,[0-9]+}-hg19.bed"
    run:
        seeds = [1242180204, 3150805968, 2214330840, 1537285587,
                 1181808143, 216262133, 3052523518, 221059682,
                 721217568, 3701222452, 2233111114, 2377261019,
                 3635684832, 3498713939, 3569405681,]
        seed = seeds[int(wildcards.idx)]
        # Value for -n flag chosen so that
        # cns-random-elems{idx}-merged-hg19.bed output has a similar
        # number of # elements as cns-pc8way-gt400-merged-hg19.bed.
        shell("bedtools random -seed {} -l 200 -n 213000 "
              "-g {{input[0]}} > {{output}}".format(seed))

rule elements_random_merge_:
    input: "data/elements/random-elems{idx}-hg19.bed"
    output: "data/elements/random-elems{idx,[0-9]+}-merged-hg19.bed"
    shell: 'grep -v "random\|hap\|Un\|chrM" {input} | '
           'sort -k1,1V -k2g > {output}.tmp && '
           'bedtools merge -i {output}.tmp > {output} && '
           'rm {output}.tmp'

rule elements_random_agg_predictor_:
    input:
        "data/nearest-gene/make_random_element_aggregate.py",
        "data/nearest-gene/{name}-nearest-counts-random-elems.csv"
    output: "data/nearest-gene/{name}-randagg.dat"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) "
           "$(basename {input[1]}) $(basename {output})"

##### HARs

rule elements_har_download_hg18_:
    output: "data/elements/2x{let,[PH]}ARs.bed"
    shell: "cd $(dirname {output}) && "
           "wget http://www.broadinstitute.org/"
           "ftp/pub/assemblies/mammals/29mammals/$(basename {output})"

def elements_har_bed_to_orig_(wildcards):
    fname = "data/elements/2x{}ARs.bed"
    return fname.format(wildcards.kind.upper())

rule elements_har_sort_label_hg_18_:
    input: elements_har_bed_to_orig_
    output: "data/elements/{kind,[ph]}ar-hg18.bed"
    shell: "cut -f1-4 {input} > {output}.tmp && "
           "bedtools sort -i {output}.tmp > {output} && "
           "rm {output}.tmp"

rule elements_har_filter_out_exon_:
    input: "data/elements/{kind}ar-hg19.bed",
           "data/elements/cns-data-hg19/exon-coordinates-hg19.bed"
    output: "data/elements/{kind,[ph]}ar-exonfiltered-hg19.bed"
    shell: "bedtools intersect -v -a {input[0]} -b {input[1]} > {output}"

rule elements_har_har_par_hg19:
    input: "data/elements/har-exonfiltered-hg19.bed",
           "data/elements/par-exonfiltered-hg19.bed"
    output: temp("data/elements/har-par-exonfiltered-hg19.bed")
    shell: "cat {input} > {output}.tmp && "
           "bedtools sort -i {output}.tmp > {output} && "
           "rm {output}.tmp"

rule elements_har_ocns_no_har_par_hg19:
    input: "data/elements/ocns-pc8way-gt400-merged-hg19.bed",
           "data/elements/har-par-exonfiltered-hg19.bed"
    output: "data/elements/ocns-no-har-par-hg19.bed"
    shell: "bedtools intersect -v -a {input[0]} -b {input[1]} > {output}"

##### schrider2015inferring

rule elements_schrider2015inferring_download_:
    output: "data/elements/schrider2015inferring/{kind,[GL]}OF_Candidates.bed"
    shell: "cd $(dirname {output[0]}) && "
           "wget https://raw.githubusercontent.com/kern-lab/popCons/"
           "793a32682af437cfb41894f6ac71a335cd830d79/beds/"
           "$(basename {output[0]})"

def elements_schrider2015inferring_bed_to_orig_(wildcards):
    fname = "data/elements/schrider2015inferring/{}OF_Candidates.bed"
    return fname.format(wildcards.kind.upper())

rule elements_schrider2015inferring_filter_:
    input: elements_schrider2015inferring_bed_to_orig_
    output: "data/elements/{kind,[gl]}of-hg19.bed"
    shell: "sed '1d' {input} | cut -f1-4 > {output}"

rule elements_schrider2015inferring_filter_out_exon_:
    input: "data/elements/{kind}of-hg19.bed",
           "data/elements/cns-data-hg19/exon-coordinates-hg19.bed"
    output: "data/elements/{kind,[gl]}of-exonfiltered-hg19.bed"
    shell: "bedtools intersect -v -a {input[0]} -b {input[1]} > {output}"

rule elements_schrider2015inferring_lgof_hg19:
    input: "data/elements/lof-hg19.bed",
           "data/elements/gof-hg19.bed"
    output: "data/elements/lgof-hg19.bed"
    shell: "cat {input} > {output}"

rule elements_schrider2015inferring_ocns_no_lgof_hg19:
    input: "data/elements/ocns-pc8way-gt400-merged-hg19.bed",
           "data/elements/lgof-hg19.bed",
    output: "data/elements/ocns-nolgof-hg19.bed"
    shell: "bedtools intersect -v -a {input[0]} -b {input[1]} > {output}"

rule elements_schrider2015inferring_ocns_no_lgof_vert100_hg19:
    input: "data/elements/ocns-pc100way-gt400-merged-hg19.bed",
           "data/elements/lgof-hg19.bed",
    output: "data/elements/ocns-nolgof-vert100-hg19.bed"
    shell: "bedtools intersect -v -a {input[0]} -b {input[1]} > {output}"

#### Nearest gene metrics

rule nearest_genes_any_:
    input: "{name}-nearest-genes.txt"
    output: "{name}-nearest-any.txt"
    shell: 'cut -f8 {input} | cut -d"|" -f2 | sort | uniq | '
           'sed "s/$/ 1/" > {output}'

rule nearest_genes_counts_:
    input: "{name}-nearest-genes.txt"
    output: "{name}-nearest-counts.txt"
    shell: "cut -f8 {input} | cut -d'|' -f2 | sort | uniq -c | "
           "awk '{{print $2,$1}}' > {output}"

rule nearest_gene_nearest_genes_:
    input: "data/elements/{name}-hg19.bed",
           "data/gencode/gencode.v19.annotation-transcript-pc-longest.bed"
    output: "data/nearest-gene/{name}-nearest-genes.txt"
    shell: "bedtools closest -D ref -a {input[0]} -b {input[1]} > {output}"

rule nearest_gene_mindist_:
    input: "data/nearest-gene/filter_nearest_by_mindist.py",
           "data/nearest-gene/{name}-nearest-genes.txt"
    output: "data/nearest-gene/{name}-nearest-genes-mindist{dist,[0-9]+}.txt"
    shell: "cat {input[1]} | ./{input[0]} {wildcards.dist} > {output}"

rule nearest_gene_within_:
    input: "data/nearest-gene/filter_nearest_within.py",
           "data/nearest-gene/{name}-nearest-genes.txt"
    output: "data/nearest-gene/"
            "{name}-nearest-genes-within{min,[0-9]+}-{max,[0-9]+}.txt"
    shell: "cat {input[1]} | "
           "./{input[0]} {wildcards.min} {wildcards.max} > {output}"

rule nearest_gene_name_counts_tag_:
    input: "{name}-nearest-genes-{tag}.txt"
    output: "{name}-nearest-counts-{tag}.txt"
    shell: "cut -f8 {input} | cut -d'|' -f2 | sort | uniq -c | "
           "awk '{{print $2,$1}}' > {output}"

def nearest_gene_cfiles(wildcards):
    clist_file = "data/nearest-gene/{}-clist".format(wildcards.feature)
    with open(clist_file) as cf:
        files = [ln.split()[1].strip() for ln in cf]
    return [os.path.join("data/nearest-gene/", f) for f in files]

rule nearest_gene_nearest_matrix_:
    input:
        "data/nearest-gene/make_nearest_matrix.py",
        clist="data/nearest-gene/{feature}-clist",
        ensgs="data/gencode/ensgs.txt",
        cfiles=nearest_gene_cfiles
    output: "data/nearest-gene/{feature}.csv"
    shell: "./{input[0]} {input.clist} {input.ensgs} {output}"

def nearest_gene_h5file_(wildcards):
    if wildcards.dset.startswith("kang2011spatio"):
        name = "kang2011spatio-p1"
    else:
        name = wildcards.dset
    return "data/expression/{}.h5".format(name)

rule nearest_gene_subset_nearest_matrix_:
    input: "data/nearest-gene/subset_nearest_matrix.py",
           matrix="data/nearest-gene/nearest-{name}.csv",
           expr=nearest_gene_h5file_
    output: "data/nearest-gene/{dset}-nearest-{name}.csv"
    shell: "./{input[0]} {input.matrix} {input.expr} {output}"

rule nearest_gene_ocns_split_cor:
    input: "data/nearest-gene/score-bw-cor.r",
           "data/nearest-gene/kang2011spatio-nearest-counts-scorebw.csv"
    output: "data/nearest-gene/cor-scorebw.dat"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"


### Binary {H,C,M}ACNS/CNS proportion (limma)

rule acns_proportion_print_per6_de_ratio_:
    input: "acns-proportion/print_de_per6_acns_cns_ratio.py",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "limma-de/{name}-de-p1.dat"
    output: "acns-proportion/{name}-per6-acns-cns-ratio-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.name} > $(basename {output})"

rule acns_proportion_print_johnson2009functional_ratio_:
    input:
        "acns-proportion/print_johnson2009functional_acns_cns_ratio.py",
        "data/nearest-gene/johnson2009functional-nearest-counts.csv",
        "johnson2009functional/{name}-de.dat"
    output:
        "acns-proportion/johnson2009functional-{name}-acns-cns-ratio-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.name} > $(basename {output})"

rule acns_proportion_print_lambert2011genes_ratio_:
    input: "acns-proportion/print_lambert2011genes_acns_cns_ratio.py",
           "data/nearest-gene/lambert2011genes-nearest-counts.csv",
           "lambert2011genes/region-de.dat"
    output: "acns-proportion/lambert2011genes-acns-cns-ratio-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule acns_proportion_johnson2009functional_pvals_:
    input: "acns-proportion/johnson2009functional-pvals.r",
           "data/nearest-gene/johnson2009functional-nearest-counts.csv",
           "johnson2009functional/{name}-de.dat",
    output: "acns-proportion/johnson2009functional-{name}-pval-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.name} "
           "> $(basename {output[0]})"

rule acns_proportion_kang2011spatio_pvals_:
    input: "acns-proportion/kang2011spatio-pvals.r",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "limma-de/{name}-de-p1.dat",
    output: "acns-proportion/kang2011spatio-{name}-pval-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.name} "
           "> $(basename {output[0]})"

rule acns_proportion_lambert2011genes_pvals_:
    input: "acns-proportion/lambert2011genes-pvals.r",
           "data/nearest-gene/lambert2011genes-nearest-counts.csv",
           "lambert2011genes/region-de.dat",
    output: "acns-proportion/lambert2011genes-pval-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output[0]})"


### Count distributions

rule element_counts_plot_ocns_hist_hc_vs_ocns_:
    input: tiff("element-counts/plot_nearest_ocns_hist_hc_vs_ocns.py"),
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "element-counts/ocns-hist-hc-vs-ocns.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule element_counts_plot_cns_har_macns_counts_:
    input: tiff("element-counts/plot_cns_har_macns_count.py"),
           "data/nearest-gene/kang2011spatio-nearest-counts-har.csv"
    output: "element-counts/cns-har-macns-counts.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule element_counts_print_nearest_count_stats:
    input: "element-counts/print_nearest_count_stats.py",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "element-counts/nearest-gene-counts-stats.txt"
    shell: "cd $(dirname {input[0]}) "
           "&& ./$(basename {input[0]}) > $(basename {output})"

rule element_counts_plot_nearest_gene_distr_:
    input: tiff("element-counts/plot_nearest_distr.py"),
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output: "element-counts/nearest-distr.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule element_counts_print_median_ocns_by_acns:
    input: "element-counts/print_ocns_median_by_acns.py",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "element-counts/median-ocns-by-el-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"


### DE with limma

rule limma_de_fit_pair_de_:
    input: "limma-de/fit-{name}-de-p{per}.r",
           "data/expression/kang2011spatio-p{per}.hdf5",
    output: "limma-de/{name}-de-p{per,[0-9]}.rdata"
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule limma_de_contrasts_pr_de_:
    input: "limma-de/contrast-pr-de.r",
           "limma-de/{name}-pr-de-p{per}.rdata",
           "lib/r/utils.r"
    output: "limma-de/{name,[^-]+}-de-p{per}.csv"
    shell: "cd $(dirname {input[0]}) "
           "&& time ./$(basename {input[0]}) "
           "$(basename {input[1]}) $(basename {output})"

rule limma_de_contrasts_pr_nofc_de_:
    input: "limma-de/contrast-pr-nofc-de.r",
           "limma-de/{name}-pr-de-p{per}.rdata",
           "lib/r/utils.r"
    output: "limma-de/{name,[^-]+}-nofc-de-p{per,[0-9]}.csv"
    shell: "cd $(dirname {input[0]}) "
           "&& time ./$(basename {input[0]}) "
           "$(basename {input[1]}) $(basename {output})"

rule limma_de_any_de_:
    input: "limma-de/any_de.py",
           "limma-de/{kind}-de-p{per}.csv",
           "data/expression/kang2011spatio-p{per}.h5"
    output: "limma-de/{kind}-de-p{per,[0-9]}.dat"
    shell: "./{input[0]} {input[1]} {input[2]} {output}"

rule limma_de_any_de_butone:
    input: "limma-de/any_de_butone.py",
           "limma-de/region-de-p1.csv",
           "data/expression/kang2011spatio-p1.h5",
    output: "limma-de/region-de-butone.dat",
            "limma-de/region-de-butone-names.txt"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule limma_de_print_de_count_:
    input: "limma-de/de_period_counts.py",
           "{name}.dat"
    output: "{name}-de-period-count-stats.txt"
    shell: "./{input[0]} {input[1]} > {output}"

rule limma_de_fill_dabg_:
    input: "limma-de/fill_dabg.py",
           "limma-de/{kind}-de-p{per}.csv",
           "data/expression/kang2011spatio-p{per}.h5"
    output: "limma-de/{kind}-de-p{per,[0-9]}-full.csv"
    shell: "./{input[0]} {input[1]} {input[2]} {output}"


### Count regression

rule count_regr_lde_print_regde_de_proportions_:
    input: "count-regr-lde/print_regde_proportions.py",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "limma-de/region-de-p1.dat"
    output: "count-regr-lde/per{period,[0-9]+}-regde-proportion-stats.txt"
    shell: "cd $(dirname {input[0]}) "
           "&& ./$(basename {input[0]}) {wildcards.period} "
           "> $(basename {output})"

rule count_regr_lde_print_regde_per6_print_acns_proportions:
    input: "count-regr-lde/print_acns_per6_proportion_de.py",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output: "count-regr-lde/regde-per6-acns-proportion-de-stats.txt"
    shell: "cd $(dirname {input[0]}) "
           "&& ./$(basename {input[0]}) > $(basename {output})"

rule count_regr_lde_dp_regde_period_data_:
    input: "count-regr-lde/create_regde{kind}_period_data.py",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output:
        "count-regr-lde/dp__regde{kind,[_A-z]*}_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_dp_nofc_regde_period_data_:
    input: "count-regr-lde/create_nofc_regde_period_data.py",
           "limma-de/region-nofc-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output:
        "count-regr-lde/dp__regde_nofc_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_dp_medexp_regde_period_data_:
    input: "count-regr-lde/create_medexp_regde_period_data.py",
           "limma-de/region-de-p1.dat",
           "data/expression/kang2011spatio-p1-region-std-median-per-stage.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output:
        "count-regr-lde/dp__regde_medexp_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_dp_regde_ragg_period_data_:
    input:
        "count-regr-lde/create_regde{kind}_period_ragg_data.py",
        "limma-de/region-de-p1.dat",
        "data/nearest-gene/kang2011spatio-nearest-counts.csv",
        "data/nearest-gene/kang2011spatio-randagg.dat"
    output:
        "count-regr-lde/dp__regde{kind,[_A-z]*}_ragg_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_dp_ncxde_period_data_:
    input: "count-regr-lde/create_ncxde_period_data.py",
           "limma-de/ncx-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output: "count-regr-lde/dp__ncxde_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_dp_ncxde_hacns_period_data_:
    input: "count-regr-lde/create_ncxde_hacns_period_data.py",
           "limma-de/ncx-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output: "count-regr-lde/dp__ncxde_hacns_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_dp_ncxde_ocns_hacns_any_period_data_:
    input: "count-regr-lde/create_ncxde_ocns_hacns_any_period_data.py",
           "limma-de/ncx-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output: "count-regr-lde/dp__ncxde_ocns_hacns_any_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_dp_ncxde_any_acns_period_data_:
    input: "count-regr-lde/create_ncxde_ocns_any_acns_period_data.py",
           "limma-de/ncx-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output: "count-regr-lde/dp__ncxde_ocns_any_acns_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_dp_nofc_ncxde_period_data_:
    input: "count-regr-lde/create_nofc_ncxde_period_data.py",
           "limma-de/ncx-nofc-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output: "count-regr-lde/dp__ncxde_nofc_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule count_regr_lde_regde_period_stan_run_:
    input: "models/{model}_",
           "count-regr-lde/{model}__{kind}_period{period}.data.r"
    output: "count-regr-lde/samples/"
            "{model}__{kind}_period{period,[0-9]+}-chain-{job}-of-4.csv"
    run:
        seeds = [538833765, 2971834801, 2533106360, 410634888,
                 3359583103, 759144701, 2090257469, 2448920206,
                 2920084016, 3364004282, 4176549166, 2048924999,
                 365883058, 3198564618, 3666096418]
        ## Deal with nolog initialization issues.  For others, just
        ## use the default.
        init = 0 if "nolog" in wildcards.kind else 2
        shell("{} sample num_warmup=1000 num_samples=1000 "
              "init={} "
              "data file={} "
              "output file={} "
              "id={} "
              "random seed={}".format(input[0],
                                      init,
                                      input[1],
                                      output[0],
                                      wildcards.job,
                                      seeds[int(wildcards.period) - 3]))

rule count_regr_lde_print_sorted_ocns_ragg_intervals_:
    input: "count-regr-lde/dp__regde_ragg-all-chains-all-periods-intervals.j4.txt"
    output: "count-regr-lde/dp__regde_ragg-all-periods-sorted-intervals-stats.txt"
    shell: "sed 1d {input} | grep beta__2 | "
           "sort -k2g,2 > {output}"

rule count_regr_lde_plot_regde_vs_bin_:
    input: tiff("count-regr-lde/plot_ocns_bins_{meas}.py"),
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "count-regr-lde/ocns-{meas}-vs-bins.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule count_regr_lde_per6_combine_dset_coef_summary:
    input: "count-regr-lde/combine_per6_dset_coef_summary.py",
           "count-regr-lde/samples/dp__regde_period6-all-chains.j4.csv",
           "count-regr-lde/samples/dp__ncxde_period6-all-chains.j4.csv",
           "count-regr-lde/samples/dp__regde_nofc_period6-all-chains.j4.csv",
           "count-regr-lde/samples/dp__ncxde_nofc_period6-all-chains.j4.csv",
           "johnson2009functional/samples/dp__region_de-all-chains.j4.csv",
           "johnson2009functional/samples/dp__ncx_de-all-chains.j4.csv",
           "anova-de/samples/dp__region_de_period6-all-chains.j4.csv",
           "anova-de/samples/dp__ncx_de_period6-all-chains.j4.csv"
    output: "count-regr-lde/dset-combined-per6-coef-summary.csv"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule count_regr_lde_plot_per6_combine_dset_coef_:
    input: tiff("count-regr-lde/plot_coef_dset_compare_per6.py"),
           "count-regr-lde/dset-combined-per6-coef-summary.csv"
    output: "count-regr-lde/dset-combined-per6-coefs.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule count_regr_lde_print_diff_stats_:
    input: "count-regr-lde/print_diff_stats.py",
           "count-regr-lde/samples/dp__{name}-all-chains.j4.csv",
    output: "count-regr-lde/{name}-diff-stats.txt"
    shell: "./{input[0]} {input[1]} > {output}"

rule count_regr_lde_print_diff_stats:
    input:
        "count-regr-lde/print_regde_nostd_diff_stats.py",
        "count-regr-lde/samples/dp__regde_nostd_ragg_period6-all-chains.j4.csv",
        "data/nearest-gene/kang2011spatio-randagg.dat"
    output: "count-regr-lde/regde-nostd-mean-ragg-diff-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule count_regr_lde_plot_reg_ncxde_prob_coefs_:
    input: tiff("count-regr-lde/plot_reg_ncxde_ocns_prob_coefs_per6.py"),
           "count-regr-lde/samples/dp__regde_period6-all-chains.j4.csv",
           "count-regr-lde/samples/dp__ncxde_period6-all-chains.j4.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "count-regr-lde/reg-ncxde-per6-prob-coefs.{ext}",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule count_regr_lde_plot_period_ar_ocns_coefs:
    input:
        tiff("count-regr-lde/plot_regde_period_ar_ocns_coefs.py"),
        "count-regr-lde/samples/dp__regde_ragg-all-chains-all-periods.j4.csv",
        "count-regr-lde/samples/dp__regde_ar_ragg-all-chains-all-periods.j4.csv",
    output: "count-regr-lde/ocns-ar-ocns-coefs.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"


### Locus length predictor

rule locus_length_export_df:
    input: "locus-length/export_loclen_df.py",
           "data/expression/kang2011spatio-p1.h5",
           "data/gencode/gencode-v19-locus-lengths.json"
    output: "locus-length/kang2011spatio-loclens.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule locus_length_dp_regde_period_data_:
    input: "locus-length/create_regde_loclen_period_data.py",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "data/expression/kang2011spatio-p1.h5",
           "data/gencode/gencode-v19-locus-lengths.json",
    output: "locus-length/dp__regde_loclen_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule locus_length_regde_dp_period_stan_run_:
    input: "models/dp_",
           "locus-length/dp__regde_loclen_period{period}.data.r"
    output: "locus-length/samples/"
            "dp__regde_loclen_period{period,[0-9]+}-chain-{job}-of-4.csv"
    run:
        seeds = [3846301754, 2861335125, 3064128360, 339414417,
                 3443064694, 2417768666, 1609555361, 3139530801, 2322324367,
                 2957061841, 2722671833, 2737616346, 308923186]
        shell("models/dp_ sample num_warmup=500 num_samples=1000 "
              "data file={} "
              "output file={} "
              "id={} "
              "random seed={}".format(input[1],
                                      output[0],
                                      wildcards.job,
                                      seeds[int(wildcards.period) - 3]))



### OCNS distance

rule ocns_loc_dp_regde_per6_dist_data:
    input: "ocns-loc/create_regde_per6_dist_data.py",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-dist.csv",
    output: "ocns-loc/dp__regde_per6_dist.data.r"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule ocns_loc_dp_regde_per6_dist_ragg_data:
    input: "ocns-loc/create_regde_per6_dist_ragg_data.py",
           "limma-de/region-de-p1.dat",
           "data/expression/kang2011spatio-p1.h5",
           "data/nearest-gene/kang2011spatio-nearest-counts-dist.csv",
           "data/nearest-gene/kang2011spatio-randagg.dat"
    output: "ocns-loc/dp__regde_per6_dist_ragg.data.r"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule ocns_loc_dp_per6_dist_stan_run_:
    input: "models/dp_",
           "ocns-loc/dp__regde_per6_dist.data.r"
    output: "ocns-loc/samples/dp__regde_per6_dist-chain-{job}-of-4.csv"
    shell: "models/dp_ sample num_warmup=500 num_samples=1000 "
           "data file={input[1]} "
           "output file={output} "
           "id={wildcards.job} "
           "random seed=4291654148"

rule ocns_loc_dp_per6_dist_ragg_stan_run_:
    input: "models/dp_",
           "ocns-loc/dp__regde_per6_dist_ragg.data.r"
    output: "ocns-loc/samples/dp__regde_per6_dist_ragg-chain-{job}-of-4.csv"
    shell: "models/dp_ sample num_warmup=500 num_samples=1000 "
           "data file={input[1]} "
           "output file={output} "
           "id={wildcards.job} "
           "random seed=4291654148"

rule ocns_loc_plot_regde_per6_dist_probs_ragg_:
    input: tiff("ocns-loc/plot_regde_per6_dist_prob_ragg.py"),
           "ocns-loc/samples/dp__regde_per6_dist-all-chains.j4.csv",
           "ocns-loc/samples/dp__regde_per6_dist_ragg-all-chains.j4.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-dist.csv"
    output: "ocns-loc/regde-per6-dist-probs-ragg.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule ocns_loc_print_regde_per6_diff_stats:
    input: "ocns-loc/print_regde_per6_pt_diff.py",
           "ocns-loc/samples/dp__regde_per6_dist-all-chains.j4.csv",
           "ocns-loc/samples/dp__regde_per6_dist_ragg-all-chains.j4.csv",
    output: "ocns-loc/regde-per6-diff-stats.txt"
    shell: "cd $(dirname {input[0]}) "
           "&& ./$(basename {input[0]}) > $(basename {output})"


### Count regression, anova

rule anova_de_:
    input: "anova-de/estimate-per6-{kind}-de.r",
           "data/expression/kang2011spatio-p1.hdf5"
    output: "anova-de/{kind}-de.dat"
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule anova_de_create_per6_data_:
    input:
        "anova-de/create_{kind}_de_period6_data.py",
        "anova-de/{kind}-de.dat",
    output: "anova-de/dp__{kind}_de_period6.data.r"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule anova_de_regde_dp_per6_stan_run_:
    input:
        "models/dp_",
        "anova-de/dp__{kind}_de_period6.data.r"
    output:
        "anova-de/samples/dp__{kind}_de_period6-chain-{job,[0-9]+}-of-{jt}.csv"
    run:
        seeds = {"region": 1183786201, "ncx": 4087614201}
        shell("models/dp_ sample num_warmup=1000 num_samples=1000 "
              "data file={} "
              "output file={} "
              "id={} "
              "random seed={}".format(input[1],
                                      output[0],
                                      wildcards.job,
                                      seeds[wildcards.kind]))


### johnson2009functional

rule johnson2009functional_fit_de_:
    input: "johnson2009functional/fit-{kind}-de.r",
           "data/expression/johnson2009functional.hdf5"
    output: "johnson2009functional/{kind}-de.rdata"
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule johnson2009functional_constrast_de_:
    input: "johnson2009functional/contrast-de.r",
           "johnson2009functional/{name}-de.rdata",
           "lib/r/utils.r"
    output: "johnson2009functional/{name,[^-]+}-de.csv"
    shell: "cd $(dirname {input[0]}) "
           "&& time ./$(basename {input[0]}) "
           "$(basename {input[1]}) $(basename {output})"

rule johnson2009functional_limma_any_de_:
    input: "johnson2009functional/any_de.py",
           "johnson2009functional/{kind}-de.csv",
    output: "johnson2009functional/{kind}-de.dat"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) $(basename {input[1]}) "
           "$(basename {output})"

rule johnson2009functional_dp_data:
    input:
        "johnson2009functional/create_{kind}_de_data.py",
        "johnson2009functional/{kind}-de.dat",
        "data/nearest-gene/johnson2009functional-nearest-counts.csv",
    output:
        "johnson2009functional/dp__{kind,[a-z]+}_de.data.r"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule johnson2009functional_dp_stan_run_:
    input: "models/dp_",
           "johnson2009functional/dp__{kind}_de.data.r"
    output:
        "johnson2009functional/samples/"
        "dp__{kind,[a-z]+}_de-chain-{job,[0-9]+}-of-4.csv"
    run:
        seeds = {"region": 12745632, "ncx": 579514966}
        shell("models/dp_ sample num_warmup=1000 num_samples=1000 "
              "data file={} "
              "output file={} "
              "id={} "
              "random seed={}".format(input[1],
                                      output[0],
                                      wildcards.job,
                                      seeds[wildcards.kind]))

### lambert2011genes

rule lambert2011genes_fit_de_:
    input: "lambert2011genes/fit-region-de.r",
           "data/expression/lambert2011genes.hdf5"
    output: "lambert2011genes/region-de.rdata"
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule lambert2011genes_constrast_de_:
    input: "lambert2011genes/contrast-de.r",
           "lambert2011genes/region-de.rdata",
           "lib/r/utils.r"
    output: "lambert2011genes/region-de.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lambert2011genes_limma_any_de_:
    input: "lambert2011genes/{name}-de.csv",
    output: "lambert2011genes/{name}-de.dat"
    shell: "sed 1d {input} | tr -d - > {output}"

rule lambert2011genes_dp_data:
    input:
        "lambert2011genes/create_region_de_data.py",
        "lambert2011genes/region-de.dat",
        "data/nearest-gene/lambert2011genes-nearest-counts.csv",
    output:
        "lambert2011genes/dp__region_de.data.r"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lambert2011genes_dp_stan_run_:
    input: "models/dp_",
           "lambert2011genes/dp__region_de.data.r"
    output:
        "lambert2011genes/samples/dp__region_de-chain-{job,[0-9]+}-of-4.csv"
    run:
        shell("models/dp_ sample num_warmup=1000 num_samples=1000 "
              "data file={} "
              "output file={} "
              "id={} "
              "random seed={}".format(input[1],
                                      output[0],
                                      wildcards.job,
                                      3682011672))


### HAR

rule har_dp_regde_period_data_:
    input: "har/create_regde{kind}_period_data.py",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-har.csv"
    output: "har/dp__regde{kind,[_A-z]*}_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule har_dp_ncxde_period_data_:
    input: "har/create_ncxde{kind}_period_data.py",
           "limma-de/ncx-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-har.csv"
    output: "har/dp__ncxde{kind,[_A-z]*}_period{period,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.period}"

rule har_regde_dp_period_stan_run_:
    input: "models/dp_",
           "har/dp__{kind}_period{period}.data.r"
    output: "har/samples/"
            "dp__{kind}_period{period,[0-9]+}-chain-{job}-of-4.csv"
    run:
        seeds = [3435816542, 2546745775, 3088671871, 2286718513,
                 1411341804, 1782993957, 134495800, 219449152,
                 2784676954, 3737674947, 3955414306, 1503875291,
                 897010355, 1160264189, 2922121138]
        shell("models/dp_ sample num_warmup=500 num_samples=1000 "
              "data file={} "
              "output file={} "
              "id={} "
              "random seed={}".format(input[1],
                                      output[0],
                                      wildcards.job,
                                      seeds[int(wildcards.period) - 3]))


### GTEx

rule gtex_fit_de_:
    input: "gtex/fit-gtex-de.r",
           "data/expression/gtex/gtex-{name}.hdf5",
    output: "gtex/{name,[^-]+}-de.rdata",
    shell: "cd $(dirname {input[0]}) "
           "&& time ./$(basename {input[0]}) "
           "{wildcards.name} $(basename {output})"

rule gtex_contrast_de_:
    input: "gtex/contrast-pr-de.r",
           "gtex/{name}-de.rdata",
           "lib/r/utils.r"
    output: "gtex/{name,[^-]+}-de.csv"
    shell: "cd $(dirname {input[0]}) "
           "&& time ./$(basename {input[0]}) "
           "$(basename {input[1]}) $(basename {output})"

rule gtex_fill_low_:
    input: "gtex/fill_low.py",
           "data/expression/gtex/gtex-{name}.hdf5",
           "gtex/{name}-de.csv",
    output: "gtex/{name,[^-]+}-de-full.csv"
    shell: "./{input[0]} {input[1]} {input[2]} {output}"

rule gtex_up_count_:
    input: "gtex/count_up.py",
           "gtex/{name}-de-full-up.csv",
    output: "gtex/{name}-de-up-count.csv"
    shell: "{input[0]} {input[1]} {output}"

rule gtex_lrup_coefs_:
    input: "gtex/lrup-coefs.r",
           "gtex/{name}-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts.csv",
           "data/nearest-gene/gtex-randagg.dat",
    output: "gtex/{name,[^-]+}-coefs-up.csv",
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.name} $(basename {output})"

rule gtex_testis_coefs:
    input: "gtex/testis-coefs.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts.csv",
           "data/nearest-gene/gtex-randagg.dat",
    output: "gtex/testis-coefs.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gtex_plot_testis_coefs:
    input: "gtex/plot_testis_coefs.py",
           "gtex/testis-coefs.csv"
    output: "gtex/testis-coefs.pdf"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gtex_reformat_pair_coefs_up_:
    input: "gtex/reformat_{name}_coefs_up.py",
           "gtex/{name}-coefs-up.csv"
    output: "gtex/{name}-coefs-up-names.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gtex_reformat_pair_de_up_:
    input: "gtex/reformat_{name}_de_up.py",
           "gtex/{name}-de-up-count.csv"
    output: "gtex/{name}-de-up-count-names.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gtex_pair_coef_up_heatmaps_:
    input: tiff("gtex/plot_{name}_heat_coefs_up.py"),
           "gtex/{name}-coefs-up-names.csv"
    output: "gtex/{name}-heatmaps-coefs-up.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gtex_pair_coef_up_tissue_nomerge_acns:
    input: "gtex/plot_tissue_nomerge_heat_coefs_up_acns.py",
           "gtex/tissue_nomerge-coefs-up-names.csv"
    output: "gtex/tissue_nomerge-heatmaps-coefs-up-acns.pdf"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gtex_pair_de_up_heatmaps_:
    input: tiff("gtex/plot_{name}_heat_de_up.py"),
           "gtex/{name}-de-up-count-names.csv"
    output: "gtex/{name}-heatmaps-de-up.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gtex_print_pair_coef_up_stats_:
    input: "gtex/print_pair_stats.py",
           "gtex/{name}-coefs-up-names.csv"
    output: "gtex/{name}-coef-up-stats.txt"
    shell: "{input[0]} {input[1]} > {output}"

rule gtex_plot_tissue_nomerge_heat_box_:
    input: tiff("gtex/plot_tissue_nomerge_heat_box.py"),
           "gtex/tissue_nomerge-coefs-up-names.csv"
    output: "gtex/tissue_nomerge-heat-box.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"


### roadmap

roadmap_eids = [
    # "E063",  # Adipose,Adipose Nuclei
    "E065",  # Heart,Aorta
    "E066",  # Other,Liver
    "E067",  # Brain,Brain Angular Gyrus
    "E068",  # Brain,Brain Anterior Caudate
    "E069",  # Brain,Brain Cingulate Gyrus
    "E070",  # Brain,Brain Germinal Matrix
    "E071",  # Brain,Brain Hippocampus Middle
    "E072",  # Brain,Brain Inferior Temporal Lobe
    "E073",  # Brain,Brain_Dorsolateral_Prefrontal_Cortex
    "E074",  # Brain,Brain Substantia Nigra
    # "E075",  # Digestive,Colonic Mucosa
    # "E076",  # Sm. Muscle,Colon Smooth Muscle
    # "E077",  # Digestive,Duodenum Mucosa
    # "E078",  # Sm. Muscle,Duodenum Smooth Muscle
    # "E079",  # Digestive,Esophagus
    # "E080",  # Other,Fetal Adrenal Gland
    "E081",  # Brain,Fetal Brain Male
    "E082",  # Brain,Fetal Brain Female
    "E083",  # Heart,Fetal Heart
    # "E084",  # Digestive,Fetal Intestine Large
    # "E085",  # Digestive,Fetal Intestine Small
    "E086",  # Other,Fetal Kidney
    # "E087",  # Other,Pancreatic Islets
    "E088",  # Other,Fetal Lung
    "E089",  # Muscle,Fetal Muscle Trunk
    "E090",  # Muscle,Fetal Muscle Leg
    # "E091",  # Other,Placenta
    # "E092",  # Digestive,Fetal Stomach
    # "E093",  # Thymus,Fetal Thymus
    # "E094",  # Digestive,Gastric
    "E095",  # Heart,Left Ventricle
    "E096",  # Other,Lung
    "E097",  # Other,Ovary
    "E098",  # Other,Pancreas
    # "E099",  # Other,Placenta Amnion
    # "E100",  # Muscle,Psoas Muscle
    # "E101",  # Digestive,Rectal Mucosa Donor 29
    # "E102",  # Digestive,Rectal Mucosa Donor 31
    # "E103",  # Sm. Muscle,Rectal Smooth Muscle
    "E104",  # Heart,Right Atrium
    "E105",  # Heart,Right Ventricle
    # "E106",  # Digestive,Sigmoid Colon
    "E107",  # Muscle,Skeletal Muscle Male
    "E108",  # Muscle,Skeletal Muscle Female
    # "E109",  # Digestive,Small Intestine
    # "E110",  # Digestive,Stomach Mucosa
    # "E111",  # Sm. Muscle,Stomach Smooth Muscle
    # "E112",  # Thymus,Thymus
    "E113",  # Other,Spleen
]

roadmap_marks = ["H3K4me1", "H3K4me3"]

rule roadmap_download_tissue_mark_subset_:
    output: "roadmap/bigwigs/{name}.fc.signal.bigwig"
    shell: "cd $(dirname {output[0]}) && "
           "wget http://egg2.wustl.edu/roadmap/data/byFileType/"
           "signal/consolidated/macs2signal/foldChange/"
           "$(basename {output})"

rule roadmap_download_tissue_mark_subset_files:
    input:
        expand("roadmap/bigwigs/{eid}-{mark}.fc.signal.bigwig",
               eid=roadmap_eids, mark=roadmap_marks)

rule elements_rename_random_bed_:
    input: "data/elements/cns-random-elems{rn}-merged-hg19.bed"
    output: "data/elements/rand{rn,[0-9]+}-hg19.bed"
    shell: "sed 's/cns/rand{wildcards.rn}/' {input} > {output}"

rule roadmap_bed_link_:
    input: "data/elements/{name}-hg19.bed"
    output: "roadmap/beds/{name}.bed"
    shell: "ln -rsf {input} {output}"

rule roadmap_bigwig_avg_over_bed_:
    input:
        bigwig="roadmap/bigwigs/{bwsamp}-{bwmark}.fc.signal.bigwig",
        bed="roadmap/beds/{bedname}.bed"
    output: "roadmap/avg/{bedname}-{bwsamp}-{bwmark}-avg.tsv"
    shell: "bigWigAverageOverBed {input.bigwig} {input.bed} {output}"

rule roadmap_concat_:
    input:
        "roadmap/avg/ocns-pc8way-gt400-merged-{samp}-{mark}-avg.tsv",
        "roadmap/avg/rand0-{samp}-{mark}-avg.tsv",
        "roadmap/avg/rand1-{samp}-{mark}-avg.tsv",
        "roadmap/avg/hacns-{samp}-{mark}-avg.tsv",
        "roadmap/avg/cacns-{samp}-{mark}-avg.tsv",
        "roadmap/avg/macns-{samp}-{mark}-avg.tsv",
        "roadmap/avg/gof-exonfiltered-{samp}-{mark}-avg.tsv",
        "roadmap/avg/lof-exonfiltered-{samp}-{mark}-avg.tsv",
    output:
        "roadmap/avg/{samp}-{mark}-element-avg-combined.tsv",
    shell: "cat {input} > {output}"

rule roadmap_combined_files:
    input:
        expand("roadmap/avg/{samp}-{mark}-element-avg-combined.tsv",
               samp=roadmap_eids,
               mark=roadmap_marks)

rule roadmap_make_median_df:
    input:
        "roadmap/make_median_df.py",
        expand("roadmap/avg/{samp}-{mark}-element-avg-combined.tsv",
               samp=roadmap_eids,
               mark=roadmap_marks)
    output: "roadmap/elements-h3k4me1-h3k4me3-medians.csv"
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule roadmap_mw_test:
    input:
        "roadmap/run-mw-test.r",
        cfs=expand("roadmap/avg/{samp}-{mark}-element-avg-combined.tsv",
                   samp=roadmap_eids,
                   mark=roadmap_marks)
    output: "roadmap/mw-test-results.rdata"
    shell: "time ./{input[0]} {output[0]} {input.cfs}"

rule roadmap_mw_test_process:
    input: "roadmap/mw-process.r",
           "roadmap/mw-test-results.rdata"
    output: "roadmap/mw-test-results-process.rdata"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule roadmap_mw_print_stats_:
    input: "roadmap/mw-print-{name}-stats.r",
           "roadmap/mw-test-results-process.rdata"
    output: "roadmap/mw-{name}-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output[0]})"

rule roadmap_density_plot_:
    input: "roadmap/density_plots.r",
           "roadmap/avg/{samp}-{mark}-element-avg-combined.tsv"
    output: "roadmap/plots/{samp}-{mark}-plots.pdf"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.samp} {wildcards.mark}"

rule roadmap_density_plots:
    input:
        expand("roadmap/plots/{samp}-{mark}-plots.pdf",
               samp=roadmap_eids,
               mark=roadmap_marks)

rule roadmap_plot_cns_acns_median:
    input: "roadmap/plot_cns_acns_medians.py",
           "roadmap/elements-h3k4me1-h3k4me3-medians.csv"
    output: "roadmap/roadmap-cns-acns-medians.pdf"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule roadmap_plot_lgof_median:
    input: "roadmap/plot_lgof_medians.py",
           "roadmap/elements-h3k4me1-h3k4me3-medians.csv"
    output: "roadmap/roadmap-lgof-medians.pdf"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"


### schrider2015inferring

rule schrider2015inferring_dp_regde_period_data_:
    input: "schrider2015inferring/create_{name}_period_data.py",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv"
    output: "schrider2015inferring/dp__{name,[_a-z]+}_period{per,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.per}"

rule schrider2015inferring_dp_cortex_vs_other_data_:
    input: "schrider2015inferring/create_cortex_vs_other_ragg_data.py",
           "gtex/tissue_nomerge-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts-lgof.csv",
           "data/nearest-gene/gtex-randagg.dat"
    output: "schrider2015inferring/dp__cortex_vs_{other,[a-z]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.other}"

rule schrider2015inferring_dp_regde_period_ragg_data_:
    input:
        "schrider2015inferring/create_regde{kind}_period_ragg_data.py",
        "limma-de/region-de-p1.dat",
        "data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv",
        "data/nearest-gene/kang2011spatio-randagg.dat",
    output:
        "schrider2015inferring/dp__regde{kind,[_A-z]*}_ragg_period{per,[0-9]+}.data.r"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.per}"

rule schrider2015inferring_regde_dp_period_stan_run_:
    input: "models/dp_",
           "schrider2015inferring/dp__{kind}_period{period}.data.r"
    output: "schrider2015inferring/samples/"
            "dp__{kind}_period{period,[0-9]+}-chain-{job}-of-4.csv"
    run:
        seeds = [1506420389, 387534604, 3210525864, 315922955,
                 2965255179, 997415542, 563406020, 2199925299,
                 2890589917, 675143093, 1658231374, 2447276414,
                 2996351059, 3925505180, 4241152714]
        shell("models/dp_ sample num_warmup=500 num_samples=1000 "
              "data file={} "
              "output file={} "
              "id={} "
              "random seed={}".format(input[1],
                                      output[0],
                                      wildcards.job,
                                      seeds[int(wildcards.period) - 1]))

rule schrider2015inferring_dp_cortex_vs_other_stan_run_:
    input: "models/dp_",
           "schrider2015inferring/dp__cortex_vs_{other}.data.r"
    output: "schrider2015inferring/samples/"
            "dp__cortex_vs_{other,[a-z]+}-chain-{job}-of-4.csv"
    run:
        seeds = {"cerebellum": 1293202362,
                 "heart": 2673693139,
                 "kidney": 1413656395,
                 "liver": 2052259419,
                 "lung": 615083473,
                 "muscle": 2802587160,
                 "ovary": 3545605093,
                 "pancreas": 196084813,
                 "spleen": 4208899577,
                 "testis": 3138578402}
        shell("models/dp_ sample num_warmup=500 num_samples=1000 "
              "data file={} "
              "output file={} "
              "id={} "
              "random seed={}".format(input[1],
                                      output[0],
                                      wildcards.job,
                                      seeds[wildcards.other]))

rule schrider2015inferring_gtex_lrup_coefs_:
    input: "schrider2015inferring/lrup-coefs{mod}.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts-lgof.csv",
           "data/nearest-gene/gtex-randagg.dat"
    output: "schrider2015inferring/tissue_nomerge{mod,[_a-z]*}-coefs-up.csv",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule schrider2015inferring_gtex_vert100_lrup_coefs:
    input: "schrider2015inferring/lrup-vert100-coefs.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts-lgof-vert100.csv",
           "data/nearest-gene/gtex-randagg.dat"
    output: "schrider2015inferring/tissue_nomerge_vert100-coefs-up.csv",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule schrider2015inferring_reformat_pair_coefs_up_:
    input:
        "schrider2015inferring/reformat_{name}_coefs_up.py",
        "schrider2015inferring/{name}-coefs-up.csv"
    output:
        "schrider2015inferring/{name}-coefs-up-names.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule schrider2015inferring_coef_up_heatmaps:
    input:
        tiff("schrider2015inferring/plot_tissue_nomerge_heat_coefs_up.py"),
        "schrider2015inferring/tissue_nomerge_nocns-coefs-up-names.csv",
        "schrider2015inferring/tissue_nomerge-coefs-up-names.csv",
    output: "schrider2015inferring/lgof-tissue_nomerge-heatmaps-coefs-up.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule schrider2015inferring_vert100_coef_up_heatmaps:
    input:
        tiff("schrider2015inferring/plot_tissue_nomerge_vert100_heat_coefs_up.py"),
        "schrider2015inferring/tissue_nomerge_vert100-coefs-up-names.csv"
    output:
        "schrider2015inferring/lgof-tissue_nomerge_vert100-heatmaps-coefs-up.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule schrider2015inferring_combine_period_samples_:
    input:
        expand("schrider2015inferring/samples/dp__regde_nocns_period{per}"
               "-all-chains-marked.j{{jobs}}.csv",
               per=range(1, 16))
    output: "schrider2015inferring/samples/dp__regde_nocns"
            "-all-chains-all-periods.j{jobs,[0-9]+}.csv"
    shell: "paste -d, {input} > {output}"

rule schrider2015inferring_mark_cortex_vs_column_:
    input:
        "{path}__cortex_vs_{other}-all-chains.j{jobs}.csv"
    output:
        "{path}__cortex_vs_{other,[a-z]+}-all-chains-marked.j{jobs,[0-9]+}.csv"
    run:
        prefix = wildcards.other + "_"
        with open(input[0]) as ifh:
            with open(output[0], "w") as ofh:
                header = ",".join([prefix + c for c in next(ifh).split(",")])
                ofh.write(header)
                for line in ifh:
                    ofh.write(line)

rule schrider2015inferring_combine_cortex_vs_other_samples_:
    input:
        expand("schrider2015inferring/samples/dp__cortex_vs_{other}"
               "-all-chains-marked.j{{jobs}}.csv",
               other=["cerebellum", "heart", "kidney", "liver", "lung",
                      "muscle", "ovary", "pancreas", "spleen", "testis"])
    output: "schrider2015inferring/samples/dp__cortex_vs"
            "-all-chains-all-other.j{jobs,[0-9]+}.csv"
    shell: "paste -d, {input} > {output}"

rule schrider2015inferring_plot:
    input:
        tiff("schrider2015inferring/plot_lgof_coef_prob.py"),
        "schrider2015inferring/samples/dp__regde_ragg-all-chains-all-periods.j4.csv",
        "schrider2015inferring/samples/dp__regde_nocns-all-chains-all-periods.j4.csv",
        "schrider2015inferring/tissue_nomerge-coefs-up-names.csv",
        "schrider2015inferring/tissue_nomerge_nocns-coefs-up-names.csv",
        "schrider2015inferring/samples/dp__cortex_vs-all-chains-all-other.j4.csv"
    output: "schrider2015inferring/lgof-coef-prob.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule schrider2015inferring_print_cortex_vs_testis_percent_de:
    input:
        "schrider2015inferring/print_cortex_vs_testis_percent_de.py",
        "gtex/tissue_nomerge-de-full-up.csv",
        "data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv",
    output: "schrider2015inferring/cortex-vs-testis-de-percent-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule schrider2015inferring_print_ocns_median_by_lgof:
    input:
        "schrider2015inferring/print_ocns_median_by_lgof.py",
        "data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv",
        "data/nearest-gene/kang2011spatio-nearest-counts-lgof-vert100.csv",
    output: "schrider2015inferring/median-ocns-by-lgof-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule schrider2015inferring_print_vert100_ocns:
    input:
        "schrider2015inferring/print-vert100-ocns-cor.r",
        "data/nearest-gene/gtex-nearest-counts-lgof.csv",
        "data/nearest-gene/kang2011spatio-nearest-counts-lgof-vert100.csv",
    output: "schrider2015inferring/vert100-ocns-cor-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"

rule schrider2015inferring_print_cortex_testis_probdiff:
    input:
        "schrider2015inferring/print_gof_cortex_testis_probdiff.py",
        "schrider2015inferring/samples/dp__cortex_vs_testis-all-chains.j4.csv",
    output: "schrider2015inferring/gof-cortex-testis-probdiff-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) > $(basename {output})"


### Gene Ontology

rule go_download_human_gene_associations:
    output: "go/gene_association.goa_human.gz",
            "go/gene_association.goa_human.gz.md5"
    shell: "pushd $(dirname {output[0]}) && "
           "wget http://geneontology.org/gene-associations/"
           "$(basename {output[0]}) && popd &&"
           "echo Downloaded on $(date +%m/%d/%y) > {output[1]} && "
           "md5sum {output[0]} >> {output[1]}"

rule go_make_topgo_mapping_:
    input: "go/make_topgo_mapping.py",
           "go/gene_association.goa_human.gz"
    output: "go/go-human-genename-mapping.txt"
    shell: "{input[0]} {input[1]} > {output}"

rule go_run_lgof_cortex_:
    input: "go/lgof-cortex-up-go.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "go/go-human-genename-mapping.txt",
           "data/nearest-gene/gtex-nearest-counts-lgof.csv",
           "data/expression/gtex-gene-names.txt"
    output:
        "go/{elem,(lof|gof)}-cortex-up-go-{kind,(bp|mf|cc)}"
        "-period{per,(13|14|15)}.csv"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) "
           "{wildcards.elem} {wildcards.kind} {wildcards.per}"


rule go_run_ocns_cortex_:
    input: "go/ocns-cortex-up-go.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "go/go-human-genename-mapping.txt",
           "data/nearest-gene/gtex-nearest-counts-lgof.csv",
    output:
        "go/ocns-cortex-up-go-{kind,(bp|mf|cc)}"
        "-period{per,(13|14|15)}-thresh{thresh,[0-9]+}.csv"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) "
           "{wildcards.kind} {wildcards.per} {wildcards.thresh}"

rule go_run_ragg_cortex_:
    input: "go/ragg-cortex-up-go.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "go/go-human-genename-mapping.txt",
           "data/nearest-gene/gtex-randagg.dat"
    output:
        "go/ragg-cortex-up-go-{kind,(bp|mf|cc)}"
        "-period{per,(13|14|15)}-thresh{thresh,[0-9]+}.csv"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) "
           "{wildcards.kind} {wildcards.per} {wildcards.thresh}"

rule go_lgof_cortex_print_cat_stats_:
    input:
        "go/print_lgof_go_cats.py",
        "go/{elem}-cortex-up-go-bp-period13.csv",
        "go/{elem}-cortex-up-go-bp-period14.csv",
        "go/{elem}-cortex-up-go-bp-period15.csv",
        "go/ocns-cortex-up-go-bp-period13-thresh10.csv",
        "go/ocns-cortex-up-go-bp-period14-thresh10.csv",
        "go/ocns-cortex-up-go-bp-period15-thresh10.csv",
        "go/ragg-cortex-up-go-bp-period13-thresh10.csv",
        "go/ragg-cortex-up-go-bp-period14-thresh10.csv",
        "go/ragg-cortex-up-go-bp-period15-thresh10.csv",
    output: "go/{elem,(gof|lof)}-cat-stats.txt"
    shell: "cd $(dirname {input[0]}) && "
           "./$(basename {input[0]}) {wildcards.elem} > $(basename {output})"


### glm probit

rule lmlr_regde_resid_bins:
    input: "lmlr/lm-regde-per6-binned-resid.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "lmlr/regde-per6-resid-vs-estim.dat",
            "lmlr/regde-per6-resid-vs-ocns.dat"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_plot_lm_comparison_:
    input:
        tiff("lmlr/plot_regde_comparison.py"),
        "lmlr/lm-regde-ragg.csv",
        "count-regr-lde/samples/dp__regde_ragg-all-chains-all-periods.j4.csv"
    output: "lmlr/dp-lm-coef-comparison.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_plot_score_ragg_:
    input: tiff("lmlr/plot_regde_score_ragg.py"),
           "lmlr/lm-regde-score-ragg.csv",
           "lmlr/lm-regde-score.csv"
    output: "lmlr/lm-score-ragg-coefs.{ext}"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_plot_regde_resid_bins_:
    input: tiff("lmlr/plot_regde_per6_binned_resid.py"),
           "lmlr/regde-per6-resid-vs-estim.dat",
           "lmlr/regde-per6-resid-vs-ocns.dat"
    output: "lmlr/regde-per6-resid.{ext}",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_plot_coefs_:
    input: "lmlr/plot-lm-de.r",
           "{path}/lm-{name}.csv"
    output: "{path}/lm-coef-plots/lm-{name}.pdf"
    shell: "./{input[0]} {input[1]} {output}"

rule lmlr_regde_coef:
    input: "lmlr/lm-regde.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "lmlr/lm-regde.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_nocns_coef:
    input: "lmlr/lm-regde-nocns.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "lmlr/lm-regde-nocns.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_nofc_coef:
    input: "lmlr/lm-regde-nofc.r",
           "limma-de/region-nofc-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "lmlr/lm-regde-nofc.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_score_coef:
    input: "lmlr/lm-regde-score.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-score.csv"
    output: "lmlr/lm-regde-score.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_score_coef_loclen:
    input: "lmlr/lm-regde-score-loclen.r",
           "locus-length/kang2011spatio-loclens.csv",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-score.csv"
    output: "lmlr/lm-regde-score-loclen.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_score_coef_ragg:
    input: "lmlr/lm-regde-score-ragg.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-randagg.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-score.csv"
    output: "lmlr/lm-regde-score-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_lgof_coef_:
    input: "lmlr/lm-regde-lgof{set}.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv"
    output: "lmlr/lm-regde-lgof{set,[-A-z]*}.csv",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_lgof_ragg_coef_:
    input: "lmlr/lm-regde-lgof-ragg.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv"
    output: "lmlr/lm-regde-lgof-ragg.csv",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_ncxde_lgof_ragg_coef_:
    input: "lmlr/lm-ncxde-lgof-ragg.r",
           "limma-de/ncx-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv"
    output: "lmlr/lm-ncxde-lgof-ragg.csv",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_lgof_vert100_coef:
    input: "lmlr/lm-regde-lgof-vert100.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-lgof-vert100.csv"
    output: "lmlr/lm-regde-lgof-vert100.csv",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_lgof_vert100_ragg_coef:
    input: "lmlr/lm-regde-lgof-vert100-ragg.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts-lgof-vert100.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv"
    output: "lmlr/lm-regde-lgof-vert100-ragg.csv",
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_ncxde_coef:
    input: "lmlr/lm-ncxde.r",
           "limma-de/ncx-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "lmlr/lm-ncxde.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_ncxde_nofc_coef:
    input: "lmlr/lm-ncxde-nofc.r",
           "limma-de/ncx-nofc-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "lmlr/lm-ncxde-nofc.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_ncxde_ragg_coef:
    input: "lmlr/lm-ncxde-ragg.r",
           "limma-de/ncx-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv"
    output: "lmlr/lm-ncxde-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_ar_coef:
    input: "lmlr/lm-regde-ar.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv"
    output: "lmlr/lm-regde-ar.csv",
            "lmlr/lm-regde-ar-nolastde.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_johnson2009functional_regde_coef:
    input: "lmlr/lm-johnson2009functional-regde.r",
           "johnson2009functional/region-de.dat",
           "data/nearest-gene/johnson2009functional-nearest-counts.csv"
    output: "lmlr/lm-johnson2009functional-regde.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_johnson2009functional_ncxde_coef:
    input: "lmlr/lm-johnson2009functional-ncxde.r",
           "johnson2009functional/ncx-de.dat",
           "data/nearest-gene/johnson2009functional-nearest-counts.csv"
    output: "lmlr/lm-johnson2009functional-ncxde.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_lambert2011genes_regde_coef:
    input: "lmlr/lm-lambert2011genes-regde.r",
           "lambert2011genes/region-de.dat",
           "data/nearest-gene/lambert2011genes-nearest-counts.csv"
    output: "lmlr/lm-lambert2011genes-regde.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_randagg_coef:
    input: "lmlr/lm-regde-ragg.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv"
    output: "lmlr/lm-regde-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_ragg_butone_coef:
    input: "lmlr/lm-regde-ragg-butone.r",
           "limma-de/region-de-butone.dat",
           "limma-de/region-de-butone-names.txt",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv",
    output: "lmlr/lm-regde-ragg-butone.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_ragg_lgof_butone_coef:
    input: "lmlr/lm-regde-ragg-lgof-butone.r",
           "limma-de/region-de-butone.dat",
           "limma-de/region-de-butone-names.txt",
           "data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv",
    output: "lmlr/lm-regde-ragg-lgof-butone.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_plot_butone:
    input: "lmlr/plot_butone.py",
           "lmlr/lm-regde-ragg-butone.csv",
           "lmlr/lm-regde-ragg.csv",
    output: "lmlr/ocns-coef-butone.pdf"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_plot_lgof_butone:
    input: "lmlr/plot_lgof_butone.py",
           "lmlr/lm-regde-ragg-lgof-butone.csv",
           "lmlr/lm-regde-lgof-ragg.csv"
    output: "lmlr/lgof-coef-butone.pdf"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_nocns_randagg_coef:
    input: "lmlr/lm-regde-nocns-ragg.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv"
    output: "lmlr/lm-regde-nocns-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_regde_ragg_med_coef:
    input: "lmlr/lm-regde-ragg-med.r",
           "limma-de/region-de-p1.dat",
           "data/expression/kang2011spatio-p1-region-std-median-per-stage.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv"
    output: "lmlr/lm-regde-ragg-med.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_ar_ragg_coef:
    input: "lmlr/lm-regde-ar-ragg.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv"
    output: "lmlr/lm-regde-ar-ragg.csv",
            "lmlr/lm-regde-ar-nolastde-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_gtex_cortex_testes_ragg:
    input: "lmlr/lm-gtex-cortex-testes-ragg.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts.csv",
           "data/nearest-gene/gtex-nearest-counts-random-elems.csv",
    output: "lmlr/lm-gtex-cortex-testes-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_gtex_cortex_testes_lgof_ragg:
    input: "lmlr/lm-gtex-cortex-testes-lgof-ragg.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts.csv",
           "data/nearest-gene/gtex-nearest-counts-random-elems.csv",
    output: "lmlr/lm-gtex-cortex-testes-lgof-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_gtex_cortex_testes_lgof_any_ragg:
    input: "lmlr/lm-gtex-cortex-testes-lgof-any-ragg.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts.csv",
           "data/nearest-gene/gtex-nearest-counts-random-elems.csv",
    output: "lmlr/lm-gtex-cortex-testes-lgof-any-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule lmlr_gtex_liver_testes_ragg:
    input: "lmlr/lm-gtex-liver-testes-ragg.r",
           "gtex/tissue_nomerge-de-full-up.csv",
           "data/nearest-gene/gtex-nearest-counts.csv",
           "data/nearest-gene/gtex-nearest-counts-random-elems.csv",
    output: "lmlr/lm-gtex-liver-testes-ragg.csv"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"


### GAM

rule gam_regde_run_:
    input: "gam/gam-regde-{name}.r",
           "gam/gam_smooth.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
    output: "gam/gam-regde-{name}-smooth-data.csv",
            "gam/gam-regde-{name}-smooth-edf.csv",
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule gam_mexp_regde_run_:
    input: "gam/gam-mexp-regde-{name}.r",
           "gam/gam_smooth.r",
           "limma-de/region-de-p1.dat",
           "data/nearest-gene/kang2011spatio-nearest-counts.csv",
           "data/expression/kang2011spatio-p1-region-std-median-per-stage.dat",
    output: "gam/gam-mexp-regde-{name}-smooth-data.csv",
            "gam/gam-mexp-regde-{name}-smooth-edf.csv",
    shell: "cd $(dirname {input[0]}) && time ./$(basename {input[0]})"

rule gam_plot_gam_regde_nolog_acns:
    input: "gam/plot_gam_regde_nolog_acns.py",
           "gam/gam-regde-nolog-acns-smooth-data.csv",
    output: "gam/gam-regde-nolog-acns.pdf"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"

rule gam_plot_mexp_gam_regde_log1p:
    input: "gam/plot_mexp_gam_regde_log1p.py",
           "gam/gam-mexp-regde-log1p-smooth-data.csv",
    output: "gam/gam-mexp-regde-log1p.pdf"
    shell: "cd $(dirname {input[0]}) && ./$(basename {input[0]})"


### General rules

rule limma_de_pair_coefs_split_up_:
    input: "bin/de_split_up.py",
           "{path}-full.csv",
    output: "{path}-full-up.csv"
    shell: "{input[0]} {input[1]} {output}"

rule stan_samples_mark_period_column_:
    input: "{path}_period{per}-all-chains.j{jobs}.csv"
    output: "{path}_period{per,[0-9]+}-all-chains-marked.j{jobs,[0-9]+}.csv"
    run:
        prefix = "p" + wildcards.per + "_"
        with open(input[0]) as ifh:
            with open(output[0], "w") as ofh:
                header = ",".join([prefix + c for c in next(ifh).split(",")])
                ofh.write(header)
                for line in ifh:
                    ofh.write(line)

rule stan_intervals_combined_periods_:
    input: "{path}/samples/{run}-all-chains-all-periods"
           ".j{jobs}.csv"
    output: "{path}/{run,[_A-z0-9]+}-all-chains-all-periods"
            "-intervals.j{jobs,[0-9]+}.txt"
    shell: "stan-intervals {input} > {output}"

rule regde_combine_period_samples_:
    input:
        expand("{{path}}_regde_period{per}"
               "-all-chains-marked.j{{jobs}}.csv",
               per=range(1, 16))
    output: "{path}_regde-all-chains-all-periods.j{jobs,[0-9]+}.csv"
    shell: "paste -d, {input} > {output}"

rule regde_ragg_combine_period_samples_:
    input:
        expand("{{path}}_regde_ragg_period{per}"
               "-all-chains-marked.j{{jobs}}.csv",
               per=range(1, 16))
    output: "{path}_regde_ragg-all-chains-all-periods.j{jobs,[0-9]+}.csv"
    shell: "paste -d, {input} > {output}"

rule regde_ar_ragg_combine_period_samples_:
    input:
        expand("{{path}}_regde_ar_ragg_period{per}"
               "-all-chains-marked.j{{jobs}}.csv",
               per=range(2, 16))
    output: "{path}_regde_ar_ragg-all-chains-all-periods.j{jobs,[0-9]+}.csv"
    shell: "paste -d, {input} > {output}"


### Other

rule md5sum_:
    input: "{name}"
    output: "{name}.md5"
    shell: "md5sum {input} > {output}"

rule md5sum_wgets:
    input:
        "data/elements/2xHARs.bed.md5",
        "data/elements/2xPARs.bed.md5",
        "data/elements/Prabhakar.TableS1.zip.md5",
        "data/elements/cns-data-hg19/chainSelfLink.txt.gz.md5",
        "data/elements/cns-data-hg19/intronEst.txt.gz.md5",
        "data/elements/cns-data-hg19/knownGene.txt.gz.md5",
        "data/elements/cns-data-hg19/ucscRetroAli5.txt.gz.md5",
        "data/elements/phastConsElements100way.txt.gz.md5",
        "data/elements/phastConsElements8way.txt.gz.md5",
        "data/elements/schrider2015inferring/GOF_Candidates.bed.md5",
        "data/elements/schrider2015inferring/LOF_Candidates.bed.md5",
        "data/expression/GPL5175-3188.txt.md5",
        "data/expression/GSE13344-GPL5175_series_matrix.txt.gz.md5",
        "data/expression/GSE25219-GPL5175_series_matrix.txt.gz.md5",
        "data/expression/GSE25219_DABG_pvalue.csv.gz.md5",
        "data/gencode/gencode.v19.annotation.gtf.gz.md5",
        "data/gencode/hg19.chrom.sizes.md5",
        "data/liftover/hg17ToHg19.over.chain.gz.md5",
        "data/liftover/hg18ToHg19.over.chain.gz.md5",
        "roadmap/bigwigs/E065-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E065-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E066-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E066-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E067-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E067-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E068-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E068-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E069-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E069-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E070-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E070-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E071-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E071-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E072-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E072-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E073-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E073-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E074-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E074-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E081-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E081-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E082-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E082-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E083-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E083-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E086-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E086-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E088-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E088-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E089-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E089-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E090-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E090-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E095-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E095-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E096-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E096-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E097-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E097-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E098-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E098-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E104-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E104-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E105-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E105-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E107-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E107-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E108-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E108-H3K4me3.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E113-H3K4me1.fc.signal.bigwig.md5",
        "roadmap/bigwigs/E113-H3K4me3.fc.signal.bigwig.md5",
