#!/usr/bin/env Rscript

## For period 6, estimate whether gene is DE between any of 11 NCX
## areas.

library(rhdf5)

datafile <- "../data/expression/kang2011spatio-p1.hdf5"

## 32-bit integer warnings
suppressWarnings(ds <- h5read(datafile, "/ds") + 1)
per6.bm <- ds == 6
suppressWarnings(db <- h5read(datafile, "/db")[per6.bm] + 1)
suppressWarnings(dr <- h5read(datafile, "/dr")[per6.bm] + 1)
ncx.bm <- dr == 1

pass.dabg <- as.logical(h5read(datafile, "/pass_dabg"))
rin <- h5read(datafile, "/rin")[per6.bm]

## genes x samples
expr <- t(as.matrix(h5read(datafile, "/values")))[pass.dabg, per6.bm]

expr <- expr[,ncx.bm]
db <- db[ncx.bm]
db <- factor(db)
rin <- rin[ncx.bm]

de.anova  <- function(gene.expr){
    da <- anova(lm(gene.expr ~ db + rin))
    pval <- da$Pr[1]
    reg.means <- tapply(gene.expr, db, mean)
    max.fold <-  max(reg.means, na.rm=TRUE) - min(reg.means, na.rm=TRUE)
    return(c(pval, max.fold, max(gene.expr)))
}

de.results <- as.data.frame(t(apply(expr, 1, de.anova)))
names(de.results) <- c("pval", "max.fold", "max.val")

de.results$pval <- p.adjust(de.results$pval, method="fdr")

is.de <- function(vect, pval.cutoff=0.01, fold.cutoff=1, max.cutoff=6) {
    return(vect[1] < pval.cutoff &
           vect[2] > fold.cutoff &
           vect[3] > max.cutoff)
}

de.status <- apply(de.results, 1, is.de)
de.status  <- de.status + 0

de.full <- as.vector(matrix(0, length(pass.dabg)))
de.full[pass.dabg] <- de.status

write.table(de.full, file="ncx-de.dat",
            row.names=FALSE, col.names=FALSE, quote=FALSE)
