#!/usr/bin/env python3

import matplotlib.pyplot as plt
import pandas as pd

import mplutil
import colors

mplutil.style_use(["latex"])

transforms = ["none", "sqrt", "log1p"]

labels = ["Number of CNSs",
          r"$\sqrt{\text{Number of CNSs}}$",
          r"$\ln(1 + \text{Number of CNSs})$"]

## Against estimated value

df_e = pd.read_csv("regde-per6-resid-vs-estim.dat", sep=" ")

fsize = (mplutil.twocol_width * 0.89, 3.2)

se_args = {"alpha": 0.5, "zorder": 1, "color": colors.base}
pt_args = {"marker": "o", "markersize": 2.5, "ls": "none",
           "alpha": 0.8, "mec": "none", "color": colors.base,
           "zorder": 2}

fig, axs = plt.subplots(ncols=3, nrows=2,
                        # sharey=True, sharex=True,
                        figsize=fsize)

df_e_grp = df_e.groupby("transform")

e_ymin, e_ymax = -0.4, 0.4

for t, ax in zip(transforms, axs[0]):
    tdf = df_e.loc[df_e_grp.groups[t]]
    xvals = tdf["xbar"].values

    assert tdf["ybar"].max() < e_ymax
    assert tdf["ybar"].min() > e_ymin

    ax.plot(xvals, tdf["ybar"].values, **pt_args)
    ax.plot(xvals, tdf["X2se"].values, **se_args)
    ax.plot(xvals, - tdf["X2se"].values, **se_args)
    mplutil.remove_spines(ax)

for ax in axs[0]:
    ax.set_xlim(0, 1)
    ax.set_ylim(e_ymin, e_ymax)
    ax.locator_params(axis="y", nbins=6)

for lab, ax in zip(labels, axs[0]):
    ax.text(0.5, 0.99, lab, transform=ax.transAxes,
            ha="center", fontsize="small")

axs[0, 0].set_xlim(0, 1)
axs[0, 0].set_ylabel("Average residual")
axs[0, 1].set_xlabel("Expected value")

## Against OCNS

df_c = pd.read_csv("regde-per6-resid-vs-ocns.dat", sep=" ")

df_c_grp = df_c.groupby("transform")

o_ymin, o_ymax = -0.4, 0.4

for t, ax in zip(transforms, axs[1]):
    tdf = df_c.loc[df_c_grp.groups[t]]

    assert tdf["ybar"].max() < o_ymax
    assert tdf["ybar"].min() > o_ymin

    xvals = tdf["xbar"].values
    ax.plot(xvals, tdf["ybar"].values, **pt_args)
    ax.plot(xvals, tdf["X2se"].values, **se_args)
    ax.plot(xvals, - tdf["X2se"].values, **se_args)
    mplutil.remove_spines(ax)
    ax.set_xlim(0 - xvals.max() * 0.05)

for ax in axs[1]:
    ax.set_ylim(o_ymin, o_ymax)
    ax.locator_params(axis="y", nbins=6)

for lab, ax in zip(labels, axs[1]):
    ax.text(0.5, 0.99, lab, transform=ax.transAxes,
            ha="center", fontsize="small")

axs[1, 0].set_ylabel("Average residual")
axs[1, 1].set_xlabel("OCNS predictor")

fig.subplots_adjust(left=0.1, right=0.98, bottom=0.115, top=0.95,
                    wspace=0.3, hspace=0.6)

mplutil.subplot_label(axs[0, 0], r"\textbf{A}")
mplutil.subplot_label(axs[1, 0], r"\textbf{B}")

fig.savefig("regde-per6-resid.pdf")
