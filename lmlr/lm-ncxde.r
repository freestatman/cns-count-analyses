#!/usr/bin/env Rscript

library(reshape)
library(parallel)

de <- as.matrix(read.table("../limma-de/ncx-de-p1.dat", sep=" "))
colnames(de) <- NULL

ng.file <- "../data/nearest-gene/kang2011spatio-nearest-counts.csv"
ng <- read.csv(ng.file)
ng <- ng[c("ocns", "hacns", "cacns", "macns")]
ng <- log1p(ng)

stage_logistic <- function(stage){
    dat <- cbind(de=de[,stage], ng)
    glm(de ~ ocns + hacns + cacns + macns,
        data=dat, family=binomial(link="probit"))
}

period.bm <- colSums(de) > 50
periods <- 1:15
periods <- periods[period.bm]

fits <- lapply(periods, stage_logistic)

## stage.pvals <- sapply(fits, function(x) coef(summary(x))[,4])

stage.coefs <- sapply(fits, coef)
rownames(stage.coefs)[1]  <- "intercept"
colnames(stage.coefs)  <- periods
scs <- melt(stage.coefs, varnames=c("param", "period"))
scs$param <- factor(scs$param, levels=unique(scs$param))
scs$period <- factor(scs$period)

stage.bounds <- mclapply(fits, confint, mc.cores=4)

create.bounds.df <- function (stage){
    idx <- which(periods == stage)
    d <- data.frame(period=stage,
                    lb=stage.bounds[[idx]][,1],
                    ub=stage.bounds[[idx]][,2])
    rownames(d)[1] <- "intercept"
    d$param <- rownames(d)
    rownames(d) <- NULL
    return(d)
}

sbs <- do.call(rbind, lapply(periods, create.bounds.df))

scs <- merge(sbs, scs)

write.csv(scs, file="lm-ncxde.csv", row.names=FALSE, quote=FALSE)
