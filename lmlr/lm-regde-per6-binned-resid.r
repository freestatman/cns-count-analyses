#!/usr/bin/env Rscript

library(arm)

per6.idx <- 6

de <- as.matrix(read.table("../limma-de/region-de-p1.dat", sep=" "))[,per6.idx]

ng.u <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng.u <- ng.u[c("ocns", "hacns", "cacns", "macns")]
ng.l <- log1p(ng.u)
ng.s <- sqrt(ng.u)

dat.u <- data.frame(de=de, ng.u)
dat.s <- data.frame(de=de, ng.s)
dat.l <- data.frame(de=de, ng.l)

fit.u <- glm(de ~ ocns + hacns + cacns + macns,
             data=dat.u, family=binomial(link="probit"))
fit.s <- glm(de ~ ocns + hacns + cacns + macns,
             data=dat.s, family=binomial(link="probit"))
fit.l <- glm(de ~ ocns + hacns + cacns + macns,
             data=dat.l, family=binomial(link="probit"))

pred.u <- predict(fit.u, type="response")
resid.u  <- de - pred.u
br.estm.u <- data.frame(binned.resids(pred.u, resid.u)$binned,
                        transform="none")
br.ocns.u <- data.frame(binned.resids(ng.u$ocns, resid.u)$binned,
                        transform="none")

pred.s <- predict(fit.s, type="response")
resid.s  <- de - pred.s
br.estm.s <- data.frame(binned.resids(pred.s, resid.s)$binned,
                        transform="sqrt")
br.ocns.s <- data.frame(binned.resids(ng.s$ocns, resid.s)$binned,
                        transform="sqrt")

pred.l <- predict(fit.l, type="response")
resid.l  <- de - pred.l
br.estm.l <- data.frame(binned.resids(pred.l, resid.l)$binned,
                        transform="log1p")
br.ocns.l <- data.frame(binned.resids(ng.l$ocns, resid.l)$binned,
                        transform="log1p")

br.estm <- rbind(br.estm.u, br.estm.s, br.estm.l)
br.ocns <- rbind(br.ocns.u, br.ocns.s, br.ocns.l)

write.table(br.estm, "regde-per6-resid-vs-estim.dat",
            quote=FALSE, row.names=FALSE)
write.table(br.ocns, "regde-per6-resid-vs-ocns.dat",
            quote=FALSE, row.names=FALSE)
