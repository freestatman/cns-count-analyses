#!/usr/bin/env Rscript

library(reshape)
library(parallel)

period.idx <- 6

de <- as.matrix(read.table("../limma-de/region-de-p1.dat", sep=" "))
colnames(de) <- NULL
de <- de[, period.idx]

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts-score.csv")
ng <- ng[,colnames(ng) != "ensg"]
ng <- log1p(ng)
ragg <- scan("../data/nearest-gene/kang2011spatio-randagg.dat", quiet=TRUE)
ng$ragg <- ragg

acns.cols <- c("hacns", "cacns", "macns")
score.cols <- paste("c", seq(150, 800, 50), sep="")

score_logistic <- function(score.col){
    dat <- cbind(de=de, ocns=ng[[score.col]], ng[acns.cols])
    glm(de ~ ocns + hacns + cacns + macns + ragg,
        data=dat, family=binomial(link="probit"))
}

fits <- lapply(score.cols, score_logistic)

## stage.pvals <- sapply(fits, function(x) coef(summary(x))[,4])

score.coefs <- sapply(fits, coef)
rownames(score.coefs)[1]  <- "intercept"
colnames(score.coefs)  <- score.cols

scs <- melt(score.coefs, varnames=c("param", "ocns.score"))
scs$param <- factor(scs$param, levels=unique(scs$param))
scs$ocns.score <- factor(scs$ocns.score)

bounds <- mclapply(fits, confint, mc.cores=4)

create.bounds.df <- function (col.idx){
    d <- data.frame(ocns.score=score.cols[col.idx],
                    lb=bounds[[col.idx]][,1],
                    ub=bounds[[col.idx]][,2])
    rownames(d)[1] <- "intercept"
    d$param <- rownames(d)
    rownames(d) <- NULL
    return(d)
}

sbs <- do.call(rbind, lapply(1:length(score.cols), create.bounds.df))

scs <- merge(sbs, scs)

write.csv(scs, file="lm-regde-score-ragg.csv", row.names=FALSE, quote=FALSE)
