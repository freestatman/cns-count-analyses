#!/usr/bin/env Rscript

library(reshape)
library(parallel)

de <- as.matrix(read.table("../limma-de/region-de-p1.dat", sep=" "))
colnames(de) <- NULL

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts.csv")
ng <- ng[c("hacns", "cacns", "macns")]
ng <- log1p(ng)

rand <- read.csv(
    "../data/nearest-gene/kang2011spatio-nearest-counts-random-elems.csv")
rand <- rand[,colnames(rand) != "ensg"]
rand <- log1p(rand)
rand.agg <- apply(rand, 1, median)

stage_logistic <- function(stage){
    dat <- cbind(de=de[,stage], ng)
    glm(de ~ hacns + cacns + macns + rand.agg,
        data=dat, family=binomial(link="probit"))
}

periods <- 1:15

fits <- lapply(periods, stage_logistic)

## stage.pvals <- sapply(fits, function(x) coef(summary(x))[,4])

stage.coefs <- sapply(fits, coef)
rownames(stage.coefs)[1]  <- "intercept"
colnames(stage.coefs)  <- periods
scs <- melt(stage.coefs, varnames=c("param", "period"))
scs$param <- factor(scs$param, levels=unique(scs$param))
scs$period <- factor(scs$period)

stage.bounds <- mclapply(fits, function (x) confint(x),
                         mc.cores=4)

create.bounds.df <- function (stage){
    d <- data.frame(period=stage,
                    lb=stage.bounds[[stage]][,1],
                    ub=stage.bounds[[stage]][,2])
    rownames(d)[1] <- "intercept"
    d$param <- rownames(d)
    rownames(d) <- NULL
    return(d)
}

sbs <- do.call(rbind, lapply(periods, create.bounds.df))

scs <- merge(sbs, scs)

write.csv(scs, file="lm-regde-nocns-ragg.csv", row.names=FALSE, quote=FALSE)
