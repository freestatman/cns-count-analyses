#!/usr/bin/env Rscript

library(reshape)
library(parallel)

de <- as.matrix(read.table("../limma-de/region-de-p1.dat", sep=" "))
colnames(de) <- NULL

ng <- read.csv("../data/nearest-gene/kang2011spatio-nearest-counts-lgof.csv")

ng <- ng[c("ocns_nolgof", "lof", "gof")]
ng["ocns_nolgof"] <- log1p(ng["ocns_nolgof"])
ng["lof"] <- (ng["lof"] > 0) + 0
ng["gof"] <- (ng["gof"] > 0) + 0

stage_logistic <- function(stage){
    dat <- cbind(de=de[,stage], ng)
    glm(de ~ ocns_nolgof + lof + gof,
        data=dat, family=binomial(link="probit"))
}

periods <- 1:15

fits <- lapply(periods, stage_logistic)

## stage.pvals <- sapply(fits, function(x) coef(summary(x))[,4])

stage.coefs <- sapply(fits, coef)
rownames(stage.coefs)[1]  <- "intercept"
colnames(stage.coefs)  <- periods
scs <- melt(stage.coefs, varnames=c("param", "period"))
scs$param <- factor(scs$param, levels=unique(scs$param))
scs$period <- factor(scs$period)

stage.bounds <- mclapply(fits, confint, mc.cores=4)

create.bounds.df <- function (stage){
    d <- data.frame(period=stage,
                    lb=stage.bounds[[stage]][,1],
                    ub=stage.bounds[[stage]][,2])
    rownames(d)[1] <- "intercept"
    d$param <- rownames(d)
    rownames(d) <- NULL
    return(d)
}

sbs <- do.call(rbind, lapply(periods, create.bounds.df))

scs <- merge(sbs, scs)

write.csv(scs, file="lm-regde-lgof-any.csv", row.names=FALSE, quote=FALSE)
