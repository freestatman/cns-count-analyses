#!/usr/bin/env Rscript

library(reshape)
library(parallel)

de <- read.csv("../gtex/tissue_nomerge-de-full-up.csv")
cols <- c("pr1.5...pr1.11", "pr2.5...pr2.11", "pr3.5...pr3.11")
de <- as.matrix(de[,cols])
colnames(de) <- NULL

ng <- read.csv("../data/nearest-gene/gtex-nearest-counts.csv")
ng <- ng[c("ocns", "hacns", "cacns", "macns")]
ng <- log1p(ng)

rand <- read.csv(
    "../data/nearest-gene/gtex-nearest-counts-random-elems.csv")
rand <- rand[,colnames(rand) != "ensg"]
rand <- log1p(rand)
rand.agg <- apply(rand, 1, median)

stage_logistic <- function(stage){
    dat <- cbind(de=de[,stage-12], ng)
    glm(de ~ ocns + hacns + cacns + macns + rand.agg,
        data=dat, family=binomial(link="probit"))
}

periods <- 13:15

fits <- lapply(periods, stage_logistic)

## stage.pvals <- sapply(fits, function(x) coef(summary(x))[,4])

stage.coefs <- sapply(fits, coef)
rownames(stage.coefs)[1]  <- "intercept"
colnames(stage.coefs)  <- periods
scs <- melt(stage.coefs, varnames=c("param", "period"))
scs$param <- factor(scs$param, levels=unique(scs$param))
scs$period <- factor(scs$period)

stage.bounds <- mclapply(fits, function (x) confint(x),
                         mc.cores=4)

create.bounds.df <- function (stage){
    d <- data.frame(period=stage,
                    lb=stage.bounds[[stage-12]][,1],
                    ub=stage.bounds[[stage-12]][,2])
    rownames(d)[1] <- "intercept"
    d$param <- rownames(d)
    rownames(d) <- NULL
    return(d)
}

sbs <- do.call(rbind, lapply(periods, create.bounds.df))

scs <- merge(sbs, scs)

write.csv(scs, file="lm-gtex-liver-testes-ragg.csv",
          row.names=FALSE, quote=FALSE)
