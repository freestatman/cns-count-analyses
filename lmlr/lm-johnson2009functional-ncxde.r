#!/usr/bin/env Rscript

library(reshape)

de <- as.matrix(read.table("../johnson2009functional/ncx-de.dat", sep=" "))
colnames(de) <- NULL

ng <- read.csv("../data/nearest-gene/johnson2009functional-nearest-counts.csv")
ng <- ng[c("ocns", "hacns", "cacns", "macns")]
ng <- log1p(ng)

dat <- cbind(de=de, ng)
fit <- glm(de ~ ocns + hacns + cacns + macns,
           data=dat, family=binomial(link="probit"))

cs <- melt(coef(fit))
rownames(cs)[1] <- "intercept"
cs <- cbind(param=rownames(cs), cs)
rownames(cs) <- NULL
cs$param <- factor(cs$param, levels=unique(cs$param))

bs <- data.frame(confint(fit))
rownames(bs)[1] <- "intercept"
colnames(bs) <- c("lb", "ub")
bs$param <- rownames(bs)
rownames(bs) <- NULL

cs <- merge(cs, bs)

write.csv(cs, file="lm-johnson2009functional-ncxde.csv",
          row.names=FALSE, quote=FALSE)
