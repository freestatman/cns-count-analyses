#!/usr/bin/env Rscript

library(ggplot2)

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 2){
    stop("Usage: plot-lm-de.r <coef-file> <outfile>")
} else{
    coef.file <- args[1]
    out.file <- args[2]
}


cs <- read.csv(coef.file)

pdf(out.file, width=7, height=5)
if ("period" %in% names(cs)){
    p <- ggplot(cs) +
        geom_pointrange(aes(period, value, ymin=lb, ymax=ub)) +
        facet_wrap(~ param) +
        geom_hline(yintercept=0)
    print(p)

    p <- ggplot(cs[cs$param != "intercept",]) +
        geom_pointrange(aes(period, value, ymin=lb, ymax=ub)) +
        facet_wrap(~ param) +
        geom_hline(yintercept=0)
    print(p)

} else{
    p <- ggplot(cs) +
        geom_pointrange(aes(param, value, ymin=lb, ymax=ub)) +
        geom_hline(yintercept=0)
    print(p)

    p <- ggplot(cs[cs$param != "intercept",]) +
        geom_pointrange(aes(param, value, ymin=lb, ymax=ub)) +
        geom_hline(yintercept=0)
    print(p)
}
dev.off()
