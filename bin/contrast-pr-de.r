#!/usr/bin/env Rscript

library(limma)

source("../lib/r/utils.r")

args <- commandArgs(trailingOnly = TRUE)
if (length(args) != 2){
    stop("usage: ./contrast-pr-de.r RDATA OUTFILE")
} else{
    rdata.file <- args[1]
    outfile <- args[2]
}

load(rdata.file)

pr.levels <- levels(x$pr)
pr.periods <- unlist(lapply(strsplit(pr.levels, ".", fixed=TRUE),
                            function (x) x[[1]]))

pr.diffs <- tapply(paste("pr", pr.levels, sep=""),
                   list(pr.periods), pair.diffs)
pr.diffs <- as.vector(unlist(pr.diffs))

cm <- makeContrasts(contrasts=pr.diffs, levels=design)

c.fit <- treat(contrasts.fit(fit, contrasts=cm), lfc=1)
pr.de <- decideTests(c.fit, method="global",
                     p.value=0.01, adjust.method="BH")
write.csv(pr.de, file=outfile, row.names=FALSE, quote=FALSE)
